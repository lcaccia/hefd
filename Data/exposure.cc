#include <vector>

// ROOT
#include "TF1.h"
#include "TFile.h"
#include "TMath.h"

// HEALPIX
#include "healpixmap.h"
#include "STClibrary.h"

using namespace std;

// Sommers function * cos(declination)
Double_t fCosVert(Double_t *x, Double_t *par)
{
  //parameters: 0 theta | 1 latitude
  double alpha;
  double xx = x[0] * TMath::DegToRad(); //declination
  double f = ( TMath::Cos(par[0]) - (TMath::Sin(par[1]) * TMath::Sin(xx) ) ) / ( TMath::Cos(par[1]) * TMath::Cos(xx) );
    
  if (f>1)
    alpha = 0.;
  else if (f<-1)
    alpha = TMath::Pi();
  else
    alpha = TMath::ACos(f);

  double omega  = ( TMath::Cos(par[1]) * TMath::Cos(xx) * TMath::Sin(alpha) + alpha * TMath::Sin(par[1]) * TMath::Sin(xx) ) * TMath::Cos(xx);
  return omega;
}

//------------------ exposure function * cos(declination) for inclined events ----------//
Double_t fCosHas(Double_t *x, Double_t *par)
{
  //parameters: 0 latitudine | 1 thetamin | 2 thetamax

  Float_t xx = x[0] * TMath::DegToRad(); //declination
  double ksiM = ( TMath::Cos(par[2]) - TMath::Sin(par[0]) * TMath::Sin(xx) ) / ( TMath::Cos(par[0]) * TMath::Cos(xx) );
  double ksim = ( TMath::Cos(par[1]) - TMath::Sin(par[0]) * TMath::Sin(xx) ) / ( TMath::Cos(par[0]) * TMath::Cos(xx) );
    
  double aM = 0.;
  double am = 0.;
  if (ksiM<-1)
    aM = TMath::Pi();
  else if (ksiM>=-1 && ksiM<=1)
    aM = TMath::ACos(ksiM);
  if (ksim<-1)
    am = TMath::Pi();
  else if (ksim>=-1 && ksim<=1)
    am = TMath::ACos(ksim);
    
  double omega = (TMath::Cos(par[0]) * TMath::Cos(xx) * (TMath::Sin(aM) - TMath::Sin(am)) + (aM-am) * TMath::Sin(par[0]) * TMath::Sin(xx)) * TMath::Cos(xx);
  return omega;
}

//--------------------------------------------------//
// sum of the vertical and inclined exposure        //
Double_t sum(Double_t *x, Double_t *par)
{

  Float_t xx = x[0] * TMath::DegToRad(); //declination
    
  //----------------------------------------------------//
  // vertical part                                      //
  //----------------------------------------------------//
  double alpha;
    
  Double_t f = ( TMath::Cos(par[0]) - (TMath::Sin(par[1]) * TMath::Sin(xx)) ) / ( TMath::Cos(par[1]) * TMath::Cos(xx) );
  if (f>1)
    alpha = 0.;
  else if (f<-1)
    alpha = TMath::Pi();
  else
    alpha = TMath::ACos(f);
    
  double omegav = ( (TMath::Cos(par[1]) * TMath::Cos(xx) * TMath::Sin(alpha) + alpha * TMath::Sin(par[1]) * TMath::Sin(xx)) ) * par[2];
  //----------------------------------------------------//
    
  //----------------------------------------------------//
  // inclined part
  //----------------------------------------------------//
  double ksiM = ( TMath::Cos(par[4]) - TMath::Sin(par[1]) * TMath::Sin(xx) ) / ( TMath::Cos(par[1]) * TMath::Cos(xx) );
  double ksim = ( TMath::Cos(par[3]) - TMath::Sin(par[1]) * TMath::Sin(xx) ) / ( TMath::Cos(par[1]) * TMath::Cos(xx) );
    
  double aM = 0.0, am = 0.0 ;
  
  if (ksiM<-1)
    aM = TMath::Pi();
  else if (ksiM>=-1 && ksiM<=1)
    aM = TMath::ACos(ksiM);
    
  if (ksim<-1)
    am = TMath::Pi();
  else if(ksim>=-1 && ksim<=1)
    am=TMath::ACos(ksim);
    
  double omegah
    = (TMath::Cos(par[1]) * TMath::Cos(xx) * (TMath::Sin(aM) - TMath::Sin(am)) + (aM-am) * TMath::Sin(par[1]) * TMath::Sin(xx)) * par[5];
  //----------------------------------------------------//
    
  return (omegav + omegah) *TMath::Cos(xx);
}

int main()
{
  // number of vertical & inclined events
  double nVert = 2032.;
  double nHor   = 593.;

  // zenith angle range
  double thetaMin = 60.*TMath::DegToRad();
  double thetaMax = 80.*TMath::DegToRad();
    
  // Auger latitude site
  double augerLat = -35.2 * TMath::DegToRad();

  cout << "\n";
  cout << " Calculate differential exposure normalize with the number of events!" << "\n";
  cout << " Vertical events: " << nVert << " - inclined events: " << nHor << endl;
  cout << "\n";

  //---------------------------------------------//
  // EXPOSURE OF THE AUGER HORIZONTAL EVENTS     //
  //---------------------------------------------//
  TF1 fHas("fHas",fCosHas,-90.,90.,3);
  fHas.SetParameters(augerLat,thetaMin,thetaMax);
    
  // calculate the normalization factor
  double intexpohor = fHas.Integral(-90.,90.);
  double normHor = nHor * 1. / (intexpohor*TMath::DegToRad()) * 1. / (2*TMath::Pi());
  
  //---------------------------------------------//
  // EXPOSURE OF THE AUGER VERTICAL   EVENTS     //
  //---------------------------------------------//
  TF1 f("f",fCosVert,-90.,90.,2);
  f.SetParameters(thetaMin,augerLat);
  //normalization factor
  double intexpo = f.Integral(-90.,90.);
  double norm = nVert * 1. / (intexpo*TMath::DegToRad()) * 1. / (2*TMath::Pi());

  //---------------------------------------//
  // FOR THE STRUCTURES ANALYSIS           //
  //---------------------------------------//
  // total exposure (vertical + inclined)
  TF1 fcos("fcos",sum,-90.,90.,6);
  fcos.SetParameters(thetaMin,augerLat,norm,thetaMin,thetaMax,normHor);

  // save declination distribution in a root file //
  string fileOutPut = "Exposure/histo-dec-en-th.root";
  TFile fOutPut(fileOutPut.c_str(),"recreate");
  fcos.Write();
  fOutPut.Close();

  //---------------------------------------//
  // FOR THE LIKELIHOOD ANALYSIS           //
  //---------------------------------------//

  //--------------------------------------------------//
  // create HealpixMap of the sky                     //
  // verticals and horizontals			              //
  //--------------------------------------------------//

  int nside=64;
  THealpixMap covV(nside, 'G');
  THealpixMap covH(nside, 'G');
  THealpixMap covTot(nside, 'G');

  double th, ph, ra, dec; 
  for (unsigned int i = 0; i < covV.NPix(); ++i) {
    covV.GiveLB(i, th, ph);
    gal2radec(th, ph, &ra,  &dec);
    covV[i]=f.Eval(dec)/TMath::Cos(TMath::DegToRad()*dec);
    covH[i]=fHas.Eval(dec)/TMath::Cos(TMath::DegToRad()*dec);
  }

  //Compute the total exposure map
  // comment (Ugo): not sure this exposure is the same as the one in line 154
  covTot = covV/covV.Total()*nVert + covH/covH.Total()*nHor;

  //--------------------------------------------------//
  // save Healpix maps in fits files                  //
  //--------------------------------------------------//
  string fitfile = "exposure_Auger_Total64.fits";
  covTot.WriteFits((char*)fitfile.c_str());

  cout << " ....done!" << "\n";
  cout << "\n";

  return 0;
}
