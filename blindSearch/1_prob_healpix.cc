/**
 \file 1_prob_healpix.cc
 \author Ugo Giaccari
 \date 20 June 2021
 \last modification 1 July 2021
 */

// Toolkit
#include "healpixmap.h"
#include "projmap.h"

// utilities for the blind search analysis
#include "utilities_blindsearch.h"

int main(int argc, char* argv[])
{

  if (argc!=3) {
    cout << " Synopsys :" << argv[0]
      << " Number of isotropic events to simulate"
      << " vertical (0) or inclined probability (> 1)"
    << endl;
    return 0;
  }
  
  // arguments
  int nev = atoi(argv[1]);  // number of isotropic events
  int flag = atoi(argv[2]); // choose vertical or inclined probability

  cout << "\n";
  cout << " ------------------------------------------------------------ " << "\n";
  if(!flag)
    cout << "    calculate isotropic probability for vertical events " << "\n";
  else
    cout << "    calculate isotropic probability for inclined events " << "\n";
  cout << "    number of events over the Auger full sky " << nev << "\n";
  cout << " ------------------------------------------------------------ " << "\n";
  cout << "\n";

  double thetamin = 0.;
  double thetamax = 0.;
  if(!flag) {
    thetamin = 0.;
    thetamax = 60.;
  }
  else {
    thetamin = 60.;
    thetamax = 80.;
  }

  // exposure function 
  TF1 f("f",expocos, -90, 90, 3);
  f.SetParameters(augerLat, thetamin, thetamax);

  // y axis
  vector<double> decrange = decaxis(nSide);
  int ynbin = decrange.size() - 1;

  // xaxis
  double scale[nbin+1];
  angScaleBinnnin(scale);

  TH2F hFinal("hFinal", "hFinal", nbin, scale, ynbin, decrange.data());
  TH2F hSim("hSim", "hSsim", nbin, scale, ynbin, decrange.data());
  
  // Get seed
  unsigned int seed;
  GetSeed(seed);
  TRandom3 myR(seed);

  // events loop
  int iCr = 0;
  while (iCr < nev) {
    double raSim = myR.Rndm() *360.;
    double decSim = myR.Rndm() * 180. - 90.;
    double yv = myR.Rndm() * f.GetMaximum();
    double y = f.Eval(decSim);
    if (yv<=y) {
      ++iCr;
      if (iCr % 100000 == 0)
        cout << " sky " << iCr << endl;

      // loop on sky positions
      for ( int u = 0; u < ynbin; ++u) {
        double dist = angDist(raSim, decSim, 0, decrange[u]);
        hSim.Fill(dist, decrange[u], 1);
      }
    }
  }
  
  // integrate TH2F
  for (int i = 1; i <= ynbin; ++i) {
    double content = 0.;
    for ( int u = 1; u <= nbin; ++u) {
      content = hSim.Integral(1, u, i, i);
      hFinal.SetBinContent(u, i, content / (double) nev );
    }
  }
  
  //--------------------------------------------------//
  // save the probability (TH2) in a root file        //
  //--------------------------------------------------//
  string fileOutPut;
  if (!flag)
    fileOutPut = "Results/pvert.root";
  else
    fileOutPut = "Results/phor.root";
  TFile fOutPut(fileOutPut.c_str(),"recreate");

  string hname;
  if (!flag)
    hname = "hvp";
  else
    hname = "hhp";
  hFinal.SetName(hname.c_str());
  hFinal.SetTitle(hname.c_str());
  hFinal.Write();
  fOutPut.Close();

  cout << "\n";
  cout << " ho passato momenti peggiori.....forse! :| " << "\n";

  return 0;
}
