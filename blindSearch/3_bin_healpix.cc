/**
 \file 3_bin_healpix.cc
 \author Ugo Giaccari
 \date 20 June 2021
 \last modification 1 July 2021
 */

#include "utilities_blindsearch.h"

// Toolkit
#include "healpixmap.h"
#include "projmap.h"

int main(int argc, char* argv[])
{

  cout << "\n";
  cout << "------------------------------ " << "\n";
  cout << " get local p-value " << "\n";
  cout << "------------------------------ " << "\n";
  cout << "\n";

  // get ipix
  int nSide = 64;
  THealpixMap map(nSide,'Q');
  vector<int> ipix = GetIpix(nSide);
    
  // decrange
  vector<double> decrange = decaxis(nSide);

  // xaxis
  double scale[nbin+1];
  angScaleBinnnin(scale);
  
  int nbinen = 49;
  double enmin = 32.;
  double enrange[nbinen];
  for (int z = 0; z <= nbinen; ++z) {
    enrange[z] = enmin + z;
  }

  // get data
  double alphaEv[nEvents];
  double deltaEv[nEvents];
  double enEv[nEvents];
  GetEvents(alphaEv, deltaEv, enEv);

  // Get number of events for each threshold
  double nOut[nEbin];
  GetNETh(enEv, nOut);

  // read exposure function
  string fileData = "Results/th2Data.root";
  TFile f(fileData.c_str(),"READ");

  // read iso probability
  string pv = "Results/pvert.root";
  TFile fv(pv.c_str(),"READ");
  TH2F *hv;
  fv.GetObject("hvp",hv);

  string ph = "Results/phor.root";
  //TFile *fh = TFile::Open(ph.c_str(),"READ");
  TFile fh(ph.c_str(),"READ");
  TH2F *hh;
  fh.GetObject("hhp",hh);

  double minval = 99999.;
  double decmin = 0.;
  double ramin = 0.;
  double nobsmin = 0.;
  double nexpmin = 0.;
  double emin = 0;
  double radiusmin = 0;

  // ipix loop
  for (unsigned int i = 0; i < (ipix.size()-1); ++i) {

    if (i % 10000 == 0)
     cout << " ipix " << i << endl;

    double ra = 0.;
    double dec = 0.;
    map.GiveLB(ipix.at(i), ra, dec);

    TH2F *h2;
    string name = "th2_ipix_"+to_string(i);
    //f->GetObject(name.c_str(),h2);
    f.GetObject(name.c_str(),h2);

    // angular scale loop
    for (int z = 1; z <= nbin; ++z) {

      // get isotropic probabilities for each declination and window size
      int bin = hv->FindBin(scale[z-1], dec);
      //int bin = hv->FindBin(z-1, dec);
      double piv = hv->GetBinContent(bin);
      double pih = hh->GetBinContent(bin);

      // energy loop
      for (int j =0; j < nbinen; ++j) {

        double nex  = GetNexp(piv, pih, nOut[j]);
        double nobs = h2->GetBinContent(z, j+1);
        double prob = binprob(nex, nobs, nOut[j]);

        if (prob<=minval) {
          minval = prob;
          decmin = dec;
          ramin = ra;
          nexpmin = nex;
          nobsmin = nobs;
          emin = enrange[j];
          radiusmin = scale[z];

          /*
          cout << " pv " << piv << " pi " << pih << endl;
          cout << " i " << i << " ipix " << ipix.at(i)
            << " ra " << ra << " dec " << dec
            << " nex " << nex << " nobs " << nobs
            << " emin " << emin << " radius " << radiusmin
            << " local p-value " << minval << endl;
          */

        }
      }
    } // end energy loop
    h2->Reset();
  } // end angular scale loop

  // delete TH2F
  hv->Delete();
  hh->Delete();

  double ntot = nOut[(int)emin-32]; // total number of event above the Eth with the lowest p-value
  double sign = lima(nobsmin, (ntot - nobsmin), (nexpmin / ( ntot - nexpmin) ) );

  cout << "\n";
  cout << " ------------------------------------ " << "\n";
  cout << " local p-value " << "\n";
  cout << " binomial probability " << minval << "\n";
  cout << " ra " << ramin << " dec " << decmin << "\n";
  cout << " nexp " << nexpmin << " nobs " << nobsmin << "\n";
  cout << " emin " << emin << " radius " << radiusmin << "\n";
  cout << " number of events above " << emin << " EeV: " << ntot << "\n";
  cout << " Li&Ma significance " << sign << endl;
  cout << " ------------------------------------ " << "\n";
  cout << "\n";

  //
  ofstream myfile;
  string title = "Results/localPvalue";
  myfile.open(title.c_str());

  myfile << minval
    << " " << ramin << " " << decmin
    << " " << nexpmin << " " << nobsmin
    << " " << emin << " " << radiusmin << endl;

  cout << " ho passato momenti peggiori.....forse! :| " << "\n";

  return 0;

}
