/**
 \file 4_penal_healpix.cc
 \author Ugo Giaccari
 \date 20 June 2021
 \last modification 1 July 2021
 */

#include "utilities_blindsearch.h"

// Toolkit
#include "healpixmap.h"
#include "projmap.h"

int main(int argc, char* argv[])
{
  if (argc!=3) {
    cout << " Synopsys :" << argv[0]
      << " Number of skies "
      << " ............... "
      << endl;
    return 0;
  }

  // arguments
  int nSky = atoi(argv[1]);  // number of isotropic events
  string nFile = argv[2];

  double pvalue = 0.;

  // get loacl p-value for the penalization
  double minval = 0.;
  double ramin = 0.;
  double decmin = 0.;
  double nexpmin = 0.;
  double nobsmin = 0.;
  double emin = 0.;
  double radiusmin = 0.;

  ifstream file;
  string title = "Results/localPvalue";
  file.open(title.c_str());

  while (!file.eof()) {
    file >> minval >> ramin >> decmin >> nexpmin >> nobsmin >> emin >> radiusmin;
    if (!file.good())
      break;
  }

  cout << "\n";
  cout << " Local p-value " << "\n";
  cout << " Binomial probability " << minval
    << " Ra " << ramin << " Dec " << decmin
    << " Nexp " << nexpmin << " Nobs " << nobsmin
    << " Eth/EeV " << emin << " angular scale [deg] " << radiusmin << endl;
  cout << "\n";

  //
  ofstream myfile;
  string fileName = "Results/file_penalization_"+nFile;
  myfile.open(fileName.c_str());

  // get ipix
  THealpixMap map(nSide,'Q');
  vector<int> ipix = GetIpix(nSide);
  int binz = ipix.size() - 1;

  // xaxis
  double scale[nbin];
  angScaleBinnnin(scale);

  // yaxis
  double enrange[nEbin];
  SetEbin(enrange);

  // get data
  double alphaEv[nEvents];
  double deltaEv[nEvents];
  double enEv[nEvents];
  GetEvents(alphaEv, deltaEv, enEv);

  // Get number of events for each energy threshold
  double nOut[nEbin];
  GetNETh(enEv, nOut);

  // read iso
  string pv = "Results/pvert.root";
  TFile fv(pv.c_str(),"READ");
  TH2F *hv;
  fv.GetObject("hvp",hv);

  string ph = "Results/phor.root";
  TFile fh(ph.c_str(),"READ");
  TH2F *hh;
  fh.GetObject("hhp",hh);

  // loop over the number of simulations
  for (int kk = 0; kk < nSky; ++ kk) {

    double minimum = 10e16;

    // simulate one isotropic sky
    double alphaSim[nEvents] = {0.};
    double deltaSim[nEvents] = {0.};
    double enSim[nEvents] = {0.};
    simSky(alphaSim, deltaSim, enSim);

    // pixels loop
    for (int i = 0; i < binz; ++i) {

      TH2F h2("h2","h2",nbin, scale, nEbin, enrange);
      TH2F h2f("h2f","h2f",nbin, scale, nEbin, enrange);

      if (i % 10000 == 0)
        cout << " i " << i << endl;
      double ra = 0.;
      double dec = 0.;
      map.GiveLB(ipix.at(i), ra, dec);

      // events loop
      for (int k = 0; k<nEvents; ++k) {
        double gamma = 0.;
        gamma = angDist(ra, dec, alphaSim[k], deltaSim[k]);
        if (enSim[k] <= 80)
          h2.Fill(gamma, enSim[k]);
        else
          h2.Fill(gamma, 80);
      } // end event loop

      // integrate the th2 histo
      h2f = GetNormIntegral(h2, 1);

      // angular scale loop
      for (int z = 1 ; z <= nbin; ++z) {

        int bin = hv->FindBin(scale[z-1], dec);
        double piv = hv->GetBinContent(bin);
        double pih = hh->GetBinContent(bin);

        // energy loop
        for (int j = 0 ; j < nEbin; ++j) {

          double nex  = GetNexp(piv, pih, nOut[j]);
          double nobs = h2f.GetBinContent(z, j+1);
          double prob = binprob(nex, nobs, nOut[j]);
          if (prob <= minimum) {
            minimum = prob;
            //cout << " ra " << ra << " dec " << dec
              //<< " energy " << j+32 << " scale " << scale[z-1]
              //<< " nex " << nex
              //<< " nobs " << nobs << " p-value " << minimum << endl;
          } // end if
        } // end energy loop
      } // end angular scale loop
      //} // end for
      h2f.Reset();
      h2.Reset();
    } // end
    cout << " sky " << kk << " p-value " << minimum << endl;
    myfile << minimum << endl;

    if (minimum<=minval)
      pvalue++;
  }
  hv->Delete();
  hh->Delete();

  if (pvalue ==0)
    pvalue = 1 / nSky;
  else
    pvalue /= nSky;
  cout << " penalized p-value " << pvalue << endl;

  cout << " ho passato momenti peggiori.....forse! :| " << "\n";
  // rint->Run(kTRUE);
  return 0;

}
