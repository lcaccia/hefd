/**
 \file 2_data_healpix.cc
 \author Ugo Giaccari
 \date 20 June 2021
 \last modification 1 July 2021
 */

#include "utilities_blindsearch.h"

// Toolkit
#include "healpixmap.h"
#include "projmap.h"
// ROOT
#include "TH3F.h"

int main(int argc, char* argv[])
{

  cout << "\n";
  cout << "-----------------------------------------------------------------------------------------" << "\n";
  cout << " scan the data and produce for each healpix pixel a TH2 ROOT histogram " << "\n";
  cout << " showing the number of observed events as a function of the energy and angular scale "<< "\n";
  cout << "-----------------------------------------------------------------------------------------" << "\n";
  cout << "\n";

  // get ipix
  THealpixMap map(nSide,'Q');
  vector<int> ipix = GetIpix(nSide);
  int binz = ipix.size() - 1;

  // xaxis
  double scale[nbin+1];
  angScaleBinnnin(scale);

  // yaxis
  double enrange[nEbin];
  SetEbin(enrange);

  // get data equatorial coordinates and energy
  double alphaEv[nEvents];
  double deltaEv[nEvents];
  double enEv[nEvents];
  GetEvents(alphaEv, deltaEv, enEv);
  
  TH2F h2("h2","h2",nbin, scale, nEbin, enrange);
  TH2F h2f("h2f","h2f",nbin, scale, nEbin, enrange);

  string nameFile = "Results/th2Data.root";
  TFile OutputFile (nameFile.c_str(),"recreate");

  // ipix loop
  for(int i = 0; i < binz; ++i) {

    if (i % 10000 == 0)
      cout << " ipix " << i << endl;
      
    // get ra and get of the pixel (centre)
    double ra = 0.;
    double dec = 0.;
    map.GiveLB(ipix.at(i), ra, dec);

    // events loop
    for (int k = 0; k<nEvents; ++k) {
      double gamma = 0.;
      gamma = angDist(ra, dec, alphaEv[k], deltaEv[k]);
      if (enEv[k] <= 80)
        h2.Fill(gamma, enEv[k]);
      else
        h2.Fill(gamma, 80);
    } // end event loop

    // integrate the th2 histo
    h2f  = GetNormIntegral(h2, 1);

    // save info (TH2F) in a root file
    string name = "th2_ipix_"+to_string(i);
    h2f.SetName(name.c_str());
    h2f.SetTitle(name.c_str());
    h2f.Write();

    h2f.Reset();
    h2.Reset();
  } // end for

  cout << " ho passato momenti peggiori.....forse! :| " << "\n";
  return 0;
}
