/**
 \file 
 \author Ugo Giaccari
 \date 20 June 2021
 \last modification 1 July 2021
 */

#include "utilities_blindsearch.h"

void GetSeed(unsigned int &seed)
{
  //----- seed changed --------//
  gRandom->SetSeed(0);
  struct timeval myTimeVal;
  struct timezone myTimeZone;
  gettimeofday(&myTimeVal, &myTimeZone);
  seed = (unsigned int) (myTimeVal.tv_usec+(myTimeVal.tv_sec % 1000)*1000000);
}

double angDist(double alphaEv, double deltaEv, double alphaObj, double deltaObj)
{
  double deltaEvR = deltaEv * d2r;
  double deltaObjR = deltaObj * d2r;
  double diffAlpha = alphaEv - alphaObj;
  double da = sin (deltaEvR) * sin (deltaObjR );
  double dg = cos (deltaEvR) * cos (deltaObjR) * cos( (diffAlpha) * d2r );
  double argAcos = da + dg;
  return acos (argAcos) * r2d;
}

void SetEbin(double *enBin)
{
  for (int z = 0; z <= nEbin; ++z)
    enBin[z] = minEn + z;
}

double GetNexp(double pv, double pi, int n)
{
  double R = (double) nVert / (double) nHor;
  double p = ( (pv * R) + pi ) * 1. / (1 + R);
  p *= n;
  // cout << " pv " << pv << " pi " << pi << " R " << R << endl;
  return p;
}

void angScaleBinnnin(double *xbins)
{
  for (int i=0; i<=nbin; i++)
    xbins[i] = (i * dx1);
}

int index(double angBin, double enBin)
{
  int index =
    ( (floor(enBin) - 1) * nbin ) + floor(angBin) -1;
  return index;
}

vector<double> decaxis(int nSide)
{
  THealpixMap map(nSide,'Q');

  //! Returns #_npix
  unsigned int npix = map.NPix();
  vector<double> decrange;
  double ndec = 0.;

  for (unsigned int i = (npix-1); i > 0; --i) {
    double ra = 0.;
    double dec = 0.;
    map.GiveLB(i, ra, dec);
    if (dec<=45) {
      if(dec != ndec) {
        ndec = dec;
        decrange.push_back(dec);
      }
    }
  }
  decrange.push_back(45.0);
  return decrange;
}

vector<int> GetIpix(int nSide)
{
  THealpixMap map(nSide,'Q');
  unsigned int npix = map.NPix();
  vector<int> ipix;

  for (unsigned int i = 0; i < npix; ++i) {
    double ra = 0.;
    double dec = 0.;
    map.GiveLB(i, ra, dec);
    if (dec<=45) {
      ipix.push_back(i);
    }
  }
  ipix.push_back(npix + 1);
  return ipix;
}

TH2F GetNormIntegral(TH2F h2, double nsky)
{
  TH2F hFinal(h2);
  for (int i=1; i<=nEbin; ++i) { // energy loop
    for (int j=1; j<=nbin; ++j) { // angular scale loop
      double content = h2.Integral(1,j,i,nEbin);
      hFinal.SetBinContent(j, i, content / double (nsky) );
    } // end angular scale loop
  } // end energy loop
  return hFinal;
}

void SetTCanvas(TCanvas &c)
{
  c.cd();
  c.SetBottomMargin(0.15);
  c.SetLeftMargin(0.15);
  c.SetRightMargin(0.18);
}

void SetTh2(string type, TH2F &h2 , string titleZ)
{
  // X axis
  h2.SetLabelFont(62,"X");
  h2.SetLabelOffset(0.02,"X");
  h2.SetLabelSize(0.05, "X");
  h2.SetTitleFont(62,"X");
  h2.SetTitleOffset(1.4, "X");
  h2.SetTitleSize(0.05,"X");
  h2.GetXaxis()->SetNdivisions(106);
  h2.SetXTitle("#scale[1.5]{#psi} [deg]");
  h2.GetXaxis()->CenterTitle();
    
  // Y axis
  h2.SetLabelFont(62,"Y");
  h2.SetLabelOffset(0.02,"Y");
  h2.SetLabelSize(0.05, "Y");
  h2.SetTitleFont(62,"Y");
  h2.SetTitleOffset(1.25, "Y");
  h2.SetTitleSize(0.05,"Y");
  h2.GetYaxis()->SetNdivisions(308);
  h2.SetYTitle("E_{#scale[1.5]{th}} [EeV]");
  h2.GetYaxis()->CenterTitle();
    
  //Z axis
  h2.SetLabelFont(60,"z");
  h2.SetLabelOffset(0.005,"Z");
  h2.SetLabelSize(0.05, "Z");
  h2.SetTitleFont(62,"Z");
  h2.SetTitleOffset(1.25, "Z");
  h2.SetTitleSize(0.05,"Z");
  h2.SetZTitle(titleZ.c_str());
  h2.GetZaxis()->CenterTitle();
  h2.GetZaxis()->RotateTitle();
   
  // color palette
  gStyle->SetOptStat(0);
  const Int_t NCont = 255;
  gStyle->SetNumberContours(NCont);
  gStyle->SetPalette(53);
  
  // title
  h2.SetTitleFont(82,"T");
  h2.SetTitleSize(0.1,"T");
    
  // set title map
  string titleMap;
  if (!type.compare("gc"))
    titleMap = "Galactic Center";
  else if (!type.compare("gp"))
    titleMap = "Galactic Plane";
  else if (!type.compare("sgp"))
    titleMap = "SuperGalactic Plane";
  else if (!type.compare("ca"))
    titleMap = "Centaurus A";
  h2.SetTitle(titleMap.c_str());
    
  // find the minimum
  int nbinx = 0;
  int nbiny = 0;
  int nbinz = 0;
  h2.GetMinimumBin(nbinx, nbiny, nbinz);
  double x = h2.GetXaxis()->GetBinCenter(nbinx);
  double y = h2.GetYaxis()->GetBinCenter(nbiny);
  
  // plot histo with minimum position (white cross)
  h2.Draw("zcol");
  TMarker *m = new TMarker(x,y,34); //34 full cross
  m->SetMarkerColor(0); //2 red //0 white
  m->SetMarkerSize(1.9);
  m->Draw();
  //delete m;
}

// Get number of events in each differential energy bin
void GetNEbin(double *enEv, double *nOut)
{
  for (int i=0; i<nEbin; ++i) {
    nOut[i] = 0;
    for ( int j =0; j<nEvents; ++j) {
      if (i!=(nEbin-1)) { //exclude the last bin above 80 EeV
        if (enEv[j]>= (minEn+i) && enEv[j]< (minEn+1+i) )
          ++nOut[i];
      }
      else {
        if (enEv[j]>= (minEn+i))
        ++nOut[i];
      }
    }
  }
}

// Get number of events for each threshold
void GetNETh(double *enEv, double *nOut)
{
  for (int i=0; i<nEbin; ++i) { // energy loop
    nOut[i] = 0;
    double eth = i + minEn;
    for (int z=0; z<nEvents; ++z) { // event loop
      if (enEv[z] >= eth)
        ++nOut[i];
    } // end event loop
  } // end energy loop
}

void GetEvents(double *alphaEv, double *deltaEv, double *enEv)
{
  // 5 columns in the event file
  string ID;
  double theta = 0.;
  double en = 0.;
  double RA = 0.;
  double dec = 0.;
    
  //event file
  string pathFile= "../Data/HEFD_v2r0_Id_Th_RA_Dec_E";
  ifstream in;
  in.open(pathFile.c_str());
    
  int count = 0;
  while (!in.eof()) {
    in >> ID >> theta >> RA >> dec >> en;
    if (!in.good()) break;
      alphaEv[count] = RA;
      deltaEv[count] = dec;
    enEv[count] = en;
    ++count;
  } //end while
}

//------------------ directional exposure * cos(declination) ----------//
double expocos(Double_t *x, Double_t *par)
{
  //parameters: 0 latitudine | 1 thetamin | 2 thetamax
  double lat = par[0]*d2r;
  double thetamin = par[1]*d2r;
  double thetamax = par[2]*d2r;
  double dec = x[0]*d2r; // declination
    
  double ksiM = ( cos(thetamax) - sin(lat)*sin(dec) ) / ( cos(lat)*cos(dec) );
  double ksim = ( cos(thetamin) - sin(lat)*sin(dec) ) / ( cos(lat)*cos(dec) );
  double aM = 0.;
  double am = 0.;
  if (ksiM<-1)
    aM = M_PI;
  else if (ksiM>=-1 && ksiM<=1)
    aM = acos(ksiM);
  if (ksim<-1)
    am = M_PI;
  else if (ksim>=-1 && ksim<=1)
    am = acos(ksim);

  double omega;
  omega = ( cos(lat) * cos(dec) * (sin(aM) - sin(am) ) + (aM-am) * sin(lat) * sin(dec) ) * cos(dec);
  return omega;
}

double binprob(double nSim, double nData, double nTot)
{
  double prob = 0.;
  if (nData)
    prob = TMath::BetaIncomplete(nSim / nTot, nData, nTot - nData + 1);
  else
    prob = 1;
  return prob;
}

void simSky(double *alphaSim, double *deltaSim, double *enSim)
{
  // Get data
  double alphaEv[nEvents];
  double deltaEv[nEvents];
  double enEv[nEvents];
  GetEvents(alphaEv, deltaEv, enEv);

  // Get number of events in each energy bin
  double nEnbin[nEbin]; //45 bins
  GetNEbin(enEv, nEnbin);

  // Get seed
  unsigned int seed;
  GetSeed(seed);
  TRandom3 myR(seed);

  // read exposure function
  string fileIn = "../exposure/functions/histo-dec.root";
  TFile *f = TFile::Open(fileIn.c_str(),"READ");
  TF1 *f1;
  f->GetObject("f1",f1);

  int count = 0;
  // energy loop
  for (int j = 0; j<nEbin; ++j) {

    double eSim = double(j + minEn);

    // events loop
    int iCr = 0;
    while (iCr < nEnbin[j]){
      double raSim = myR.Rndm() *360.;
      double decSim = myR.Rndm() * 180. - 90.;
      double y = myR.Rndm() * f1->GetMaximum();
      double yFun = f1->Eval(decSim);
      if (y<=yFun) {
        ++iCr;
        alphaSim[count] = raSim;
        deltaSim[count] = decSim;
        enSim[count] = eSim;
        ++count;
      } //end if
    } //end while
  } // end energy loop
}

// Li and Ma estimator
double lima(double nOn, double nOff, double alpha)
{
    double excess = nOn - (nOff * alpha);
    double n1 = 0.;
    double n2 = 0.;
    double sign = 0.;
    double a = 0.;

    if (excess > 0) {
        n1 = nOn;
        n2 = nOff;
        sign = 1.0;
        a = alpha;
    }
    else {
        n2 = nOn;
        n1 = nOff;
        sign = -1.0;
        a = 1.0 / alpha;
    }
    double nTot = n1 + n2;
    double t1 = n1 * log( ((1+a)/a) * (n1/nTot) );
    double t2 = n2 * log( (1+a) * (n2/nTot) );
    double significance = sqrt(2.) * sqrt( t1 + t2 );

    return sign*significance;
}


