/**
 \file 
 \author Ugo Giaccari
 \date 20 June 2021
 \last modification 1 July 2021
 */

#include <stdlib.h>
#include <stdio.h>
#include <iostream>
#include <fstream>
#include <cstring>
#include <vector>
#include <sstream>
#include <iomanip>
#include <algorithm>
#include <limits>

#include <cmath>
#include <string>
#include <math.h>       // fmod
#include <sstream>
#include <time.h>

//---- ROOT -----//
#include "Riostream.h"
#include "TRandom.h"
#include "TH2F.h"
#include "TFile.h"
#include "TMath.h"
#include "TH1D.h"
#include "TPad.h"
#include "TROOT.h"
#include "TRandom.h"
#include "TRandom3.h"
#include "TF1.h"
#include "TCanvas.h"
#include "TRint.h"
#include "TStyle.h"
#include "TMarker.h"
#include "TSystem.h"

#include <sys/time.h>

#include "STClibrary.h"

// Toolkit
#include "healpixmap.h"
#include "projmap.h"

using namespace std;

//-----------------------------------//
// -------- constants ---------------//
//-----------------------------------//
const double d2r = TMath::Pi() / 180.;
const double r2d = 180 / TMath::Pi();

// Pierre Auger Observatory mean latitude
const double augerLat = -35.2;

// nSide of the Healpix map
const int nSide = 64;

// Number of events
const int nEvents = 2625;  // total number of events
const int nVert = 2032;    // number of vertical  events
const int nHor = 593;      // number of inclined events events

const int nbin = 30;       // number of bins in angular scale
const double dx1 = 1.;     // dTheta = 1.°
const int nEbin = 49;      // number of bins in energy, from 32 to 80 EeV in step of 1 EeV
const double minEn = 32.;  // minimum energy
const double maxEn = 81.;  // maximum energy (in the plot)

//! GetSeed
void GetSeed(unsigned int &seed);

// angular distance btw two points on a sphere
double angDist(double alphaEv, double deltaEv, double alphaObj, double deltaObj);

// print on the screen some info about the analysis
void print(string type, int nsky);

// initialize the angular scale
void angScaleBinnnin(double *xbins);

vector<double> decaxis(int nSide);
vector<int> GetIpix(int nSide);

int index(double angBin, double enBin);

//! Set the object coordinates
void SetObjectCoords(string type, double &alphaObj, double &deltaObj);

// get the events
void GetEvents(double *alphaEv, double *deltaEv, double *enEv);

// integral (E >= Eth , angular scale <=\Phi)
TH2F GetNormIntegral(TH2F h2, double nsky);

// Get number of events in each energy bin
void GetNEbin(double *enEv, double *nOut);

// Get number of events for each threshold
void GetNETh(double *enEv, double *nOut);

// observed number of events
TH2F GetTH2data(string type);

// calculate expected number of events from the isotropy
void evIso(string type, double nsky, TH2F& hMean, TH2F& hProb, TH2F hdata, TH1F *histSim[]);

void SetEbin(double *enBin);

// get of expected events
double GetNexp(double pv, double pi, int n);


//! plotting style for TCanvas and TH2F map
void SetTh2(string type, TH2F &h2 , string titleZ);
void SetTCanvas(TCanvas &c);

string GetNameFile(string type);

// ! function for penalization
double penalization(string type, double nsky);

// geometric exposure in declination
double expocos(Double_t *x, Double_t *par);
double binprob(double nSim, double nData, double nTot);
//double getnexp(double pv, double pi, double fv, double fi, double n);

void simSky(double *alphaSim, double *deltaSim, double *enSim);

// Li and Ma estimator
double lima(double nOn, double nOff, double alpha);

