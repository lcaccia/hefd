///////////////////////////////////////////////////////
//                                                   //
//   Dev. by Jonathan Biteau (biteau@in2p3.fr)       //
//                       last edit: 2021-05-05       //
///////////////////////////////////////////////////////

// C/C++ classics
#include <iostream>
#include <fstream>
#include <vector>

// Toolkit
#include "projmap.h"
#include "healpixmap.h"

// ROOT
#include "TRint.h"
#include "TROOT.h"
#include "TH2D.h"
#include "TMath.h"
#include "TMinuit.h"
#include "TStyle.h"
#include "TLatex.h"
#include "TGraph.h"
#include "TLegend.h"
#include "TSystem.h"

// DATA INPUT
#include "InputData.h"

// UTILITIES CATALOG ANALYSIS
#include "utilities_LikelihoodCatalogs.h"

#ifdef gcc323
char* operator+( streampos&, char* );
#endif

using namespace std;

//Progress bar
static TProgressBar gProgB;

//Global variables needed by Minuit
vector<double> viso_MultiGLOB, vsigma_theta_MultiGLOB;
vector< vector<double> > vtheta1_MultiGLOB, vtheta2_MultiGLOB;

void Usage(string sinput)
{
	cout << endl;
	cout << " Synopsis : " << endl;
	cout << sinput << " -h, --help to obtain this message" << endl;
	cout << sinput << "" << endl << endl;

	cout << " Description :" << endl;
	cout << " Compare the scenarios above the best-fit threshold energies"<<endl
			 << " -- that's all folks! -- "
			 << endl<< endl;

	exit(0);
}

/// Likelihood ////////////////////////////////////////////////////////////////////////////////////////

//Compute the likelihood of a model map for a given event map 
double LogLikelihoodMulti(double theta, double alpha1, double alpha2){
	vector < double > vmodel1 = utils_Interpolate(vtheta1_MultiGLOB, vsigma_theta_MultiGLOB, theta);
	vector < double > vmodel2 = utils_Interpolate(vtheta2_MultiGLOB, vsigma_theta_MultiGLOB, theta);
	double logL=0.;
	for(unsigned int i=0; i<viso_MultiGLOB.size(); i++){
		double nmodel =  alpha1*vmodel1[i] + alpha2*vmodel2[i] + (1.-alpha1-alpha2)*viso_MultiGLOB[i];
		if(nmodel<=0)	return -1.E99;
		else logL+=log(nmodel);
	}
	return logL;
}

void Minuit_MLogL_AllMulti(int &, double *, double &f, double *par, int ){
	f = -LogLikelihoodMulti(par[0], par[1], par[2]);
	return; 
}

//Find best likelihood vs theta
double FindMaxLogL_AllMulti(double& theta, double& dtheta, double& alpha1, double& dalpha1, double& alpha2, double& dalpha2, bool fix_theta=false, bool fix_alpha1=false, bool fix_alpha2=false, bool improve=false){


	//Minuit Initialization
	Int_t npar = 3;
	TMinuit* ptMinuit = new TMinuit(npar);  //initialize TMinuit with a maximum of npar
	ptMinuit->SetPrintLevel(-1);//-1 no output - 1 std output
	double arglist[10];  int ierflg = 0;
	ptMinuit->mnexcm("SET NOW",arglist,1,ierflg);//No warnings
	ptMinuit->SetErrorDef(0.5);//1 for chi2, 0.5 for -logL
	ptMinuit->mnparm(0,"sig_theta",theta,dtheta,3.,40.,ierflg);
	ptMinuit->mnparm(1,"alpha1",alpha1,dalpha1,0.00,1.00,ierflg);
	ptMinuit->mnparm(2,"alpha2",alpha2,dalpha2,0.00,1.00,ierflg);
	if(fix_theta) ptMinuit->FixParameter(0);
	if(fix_alpha1) ptMinuit->FixParameter(1);
	if(fix_alpha2) ptMinuit->FixParameter(2);

	ptMinuit->SetFCN(Minuit_MLogL_AllMulti);
	int nmax_iterations = 100;
	ptMinuit->SetMaxIterations(nmax_iterations);
	arglist[0]=1;//1 std, 2 try to improve (slower)
	ptMinuit->mnexcm("SET STR",arglist,1,ierflg);	
	ptMinuit->mnexcm("SIMPLEX",0,0,ierflg);	
	if(improve){
		arglist[0]=2;//1 std, 2 try to improve (slower)
		ptMinuit->mnexcm("SET STR",arglist,1,ierflg);	
		ptMinuit->mnexcm("MIGRAD",0,0,ierflg);	
	}

	double MLogLmin = 0.;
	double fedm, errdef;
	int npari, nparx, istat;
	ptMinuit->mnstat(MLogLmin,fedm, errdef, npari, nparx, istat);
	ptMinuit->GetParameter(0,theta,dtheta);
	ptMinuit->GetParameter(1,alpha1,dalpha1);
	ptMinuit->GetParameter(2,alpha2,dalpha2);

	return -MLogLmin;
}

//Returns the max TS and best-fit parameters for a given UHECR dataset
double TS_fixed_EMulti(vector< double > vl, vector< double > vb, THealpixMap exposureMap, vector< THealpixMap > vsmoothedMap1, vector< THealpixMap > vsmoothedMap2, double& sig_theta_bf, double& alpha1_bf, double& alpha2_bf, double& logL_0, bool fix_alpha1, bool fix_alpha2, bool fix_theta=false){
	
	unsigned int nevents = vl.size();
	unsigned int nSide = exposureMap.NSide();

	// Load the list of pixels with non-zero content and the event map for final plotting purpose	
	THealpixMap eventMap(nSide,'G');
	vector< unsigned int > vIpix;
	for(unsigned int i=0; i<nevents; i++){
		int pix = exposureMap.Ip(vl[i],vb[i]);
		vIpix.push_back(pix);
		eventMap[pix]+=1;
	}

	//Load the isotropic map
	THealpixMap modelMapIsotropic(nSide,'G');
	double integralExposure = exposureMap.Total();
	modelMapIsotropic=exposureMap*(nevents/integralExposure);
	viso_MultiGLOB.clear();
	viso_MultiGLOB.resize(vIpix.size());
	for(unsigned int i=0; i<nevents; i++) viso_MultiGLOB[i] = modelMapIsotropic[vIpix[i]]; 

	//Load the normalized smoothed maps
	vtheta1_MultiGLOB.clear();
	vtheta1_MultiGLOB.resize(vsigma_theta_MultiGLOB.size());
	for(unsigned int i=0; i<vsmoothedMap1.size(); i++){
		THealpixMap modelMap1(nSide,'G');
		modelMap1 = vsmoothedMap1[i]*exposureMap;
		double integral_sources1 = modelMap1.Total();
		modelMap1=modelMap1*(nevents/integral_sources1);

		//Load the vector of event counts vs iPix
		vtheta1_MultiGLOB[i].clear();
		vtheta1_MultiGLOB[i].resize(vIpix.size());
		for(unsigned int j=0; j<vIpix.size(); j++) vtheta1_MultiGLOB[i][j] = modelMap1[vIpix[j]]; 
	}

	//Load the normalized smoothed maps
	vtheta2_MultiGLOB.clear();
	vtheta2_MultiGLOB.resize(vsigma_theta_MultiGLOB.size());
	for(unsigned int i=0; i<vsmoothedMap2.size(); i++){
		THealpixMap modelMap2(nSide,'G');
		modelMap2 = vsmoothedMap2[i]*exposureMap;
		double integral_sources2 = modelMap2.Total();
		modelMap2=modelMap2*(nevents/integral_sources2);

		//Load the vector of event counts vs iPix
		vtheta2_MultiGLOB[i].clear();
		vtheta2_MultiGLOB[i].resize(vIpix.size());
		for(unsigned int j=0; j<vIpix.size(); j++) vtheta2_MultiGLOB[i][j] = modelMap2[vIpix[j]]; 
	}

	//Likelihood for the data
	double dsig_theta_bf=5.;
	double dalpha1_bf=0.05;
	double dalpha2_bf=0.05;
	double logL = FindMaxLogL_AllMulti(sig_theta_bf, dsig_theta_bf, alpha1_bf, dalpha1_bf, alpha2_bf, dalpha2_bf, fix_theta, fix_alpha1, fix_alpha2, true);

	if(logL<-1.E50) cout<<"!!! Danger: computed chi2 is not right:"<<logL<<endl;
	logL_0 = LogLikelihoodMulti(sig_theta_bf, 0., 0.);
	if(logL_0<-1.E50) cout<<"!!! Danger: reference chi2 is not right:"<<logL_0<<endl;

	//Compute significance	
	double TS_data = 2.*(logL-logL_0);

	return TS_data;
}

//Actual place where all the work is done for a given catalog
void LoadResults(double Eth, int cat2, int compo_model=0, int cat1=0, bool mute = true, bool verbose = false, bool plot=true){

	if(mute) freopen("/dev/null", "w", stderr);

	////////////////////////////////////////////////////////////////////////////
	//                                                                        //
	//                      Model loading                                     //
	//                                                                        //
	////////////////////////////////////////////////////////////////////////////

	//Select catalog and set cosmetics
	stringstream ssEth;
	ssEth<<"#it{E} > "<<Eth<<" EeV";
	
	string sexp1 = InputData_vssources[cat1]+" - "+ssEth.str(); 
	string sexp2 = InputData_vssources[cat2]+" - "+ssEth.str(); 
	string sexp_file = "Results/"+InputData_vsfile[cat2]+"_"+InputData_vsfile[cat1];
	
	//Composition model
	stringstream ssfile_compo1;
	string ssrc_file1 = InputData_vsfile[cat1];
	ssfile_compo1<<"CompoModel/"<<ssrc_file1<<"/"<<ssrc_file1;
	if(InputData_vscenarios[compo_model].length()>1) ssfile_compo1<<"_"<<InputData_vscenarios[compo_model];
	ssfile_compo1<<"_threshold"<<int(Eth);	

	stringstream ssfile_compo2;
	string ssrc_file2 = InputData_vsfile[cat2];
	ssfile_compo2<<"CompoModel/"<<ssrc_file2<<"/"<<ssrc_file2;
	if(InputData_vscenarios[compo_model].length()>1) ssfile_compo2<<"_"<<InputData_vscenarios[compo_model];
	ssfile_compo2<<"_threshold"<<int(Eth);	

	////////////////////////////////////////////////////////////////////////////
	//                                                                        //
	//                      Data/Model loading                                //
	//                                                                        //
	////////////////////////////////////////////////////////////////////////////

	//Note: info on dataset and catalogs stored in InputData.h
	
	//Events///////////////////////////////////////////////////////////////////	
	// Read the event file
	vector< double > vId, vZen, vl, vb, vE;
	LoadAugerData(InputData_seventfile , vId, vZen, vl, vb, vE);
	
	//Trim the data below threshold
	Trim(Eth,vE, vZen, vl,vb);	

	//Exposure/////////////////////////////////////////////////////////////////
	THealpixMap exposureMap((char*)InputData_sexpofitsfile.c_str());
	exposureMap.SetCoordSys('G');
	unsigned int nSide = exposureMap.NSide();

	////////////////////////////////////////////////////////////////////////////
	//                                                                        //
	//     	                 FIT THE DATA ABOVE Eth                           //
	//                                                                        //
	////////////////////////////////////////////////////////////////////////////

	//steps in sigma_theta
	double sig_theta_min = 3., sig_theta_max = 40., dsig_theta = 1.;
	vsigma_theta_MultiGLOB.clear();
	vsigma_theta_MultiGLOB.push_back(sig_theta_min);
	while(vsigma_theta_MultiGLOB.back()<sig_theta_max) vsigma_theta_MultiGLOB.push_back(vsigma_theta_MultiGLOB.back()+dsig_theta);

	//Load the model1
	vector< string > vSrcName1;
	vector< double > vl_model1, vb_model1, vw_model1;
	LoadModel(ssfile_compo1.str(),vSrcName1, vl_model1, vb_model1, vw_model1);
	vector< THealpixMap > vsmoothedMap1;
	for(unsigned int i=0; i<vsigma_theta_MultiGLOB.size(); i++) vsmoothedMap1.push_back(LoadSrcMap(nSide, vsigma_theta_MultiGLOB[i], vl_model1, vb_model1, vw_model1));

	//Load the model2
	vector< string > vSrcName2;
	vector< double > vl_model2, vb_model2, vw_model2;
	LoadModel(ssfile_compo2.str(),vSrcName2, vl_model2, vb_model2, vw_model2);
	vector< THealpixMap > vsmoothedMap2;
	for(unsigned int i=0; i<vsigma_theta_MultiGLOB.size(); i++) vsmoothedMap2.push_back(LoadSrcMap(nSide, vsigma_theta_MultiGLOB[i], vl_model2, vb_model2, vw_model2));


	//Fit the data with 1 fixed to zero
	unsigned int nevents = vl.size();
	double logL_0;
	double sig_theta_bf_1 = 10.;
	double alpha_bf1_1 = 0.00; 
	double alpha_bf2_1 = 0.10;
	bool fix_alpha1 = true;
	bool fix_alpha2 = false;
	double TS_data2 = TS_fixed_EMulti(vl,vb, exposureMap, vsmoothedMap1, vsmoothedMap2, sig_theta_bf_1, alpha_bf1_1, alpha_bf2_1, logL_0, fix_alpha1, fix_alpha2);

	if(verbose){
		cout<<endl<<endl;
		cout<<"---------------------------"<<endl;
		cout<<sexp2<<" - "<<InputData_vscenarios[compo_model]<<endl;
		cout<<"---------------------------"<<endl;
		cout<<"Eth: "<<Eth<<" / nevents = "<<nevents<<endl;
		cout<<"pre-penalization TS: "<<TS_data2<<endl;
		cout<<"for "<<ssrc_file1<<" alpha/theta: "<<alpha_bf1_1<<" "<<sig_theta_bf_1<<std::endl;
		cout<<"for "<<ssrc_file2<<" alpha/theta: "<<alpha_bf2_1<<" "<<sig_theta_bf_1<<std::endl;
	}

	//Fit the data with 2 fixed to zero
	double sig_theta_bf_2 = 10.;
	double alpha_bf1_2 = 0.10; 
	double alpha_bf2_2 = 0.00;
	fix_alpha1 = false;
	fix_alpha2 = true;
	double TS_data1 = TS_fixed_EMulti(vl,vb, exposureMap, vsmoothedMap1, vsmoothedMap2, sig_theta_bf_2, alpha_bf1_2, alpha_bf2_2, logL_0, fix_alpha1, fix_alpha2);

	if(verbose){
		cout<<endl<<endl;
		cout<<"---------------------------"<<endl;
		cout<<sexp1<<" - "<<InputData_vscenarios[compo_model]<<endl;
		cout<<"---------------------------"<<endl;
		cout<<"Eth: "<<Eth<<" / nevents = "<<nevents<<endl;
		cout<<"pre-penalization TS: "<<TS_data1<<endl;
		cout<<"for "<<ssrc_file1<<" alpha/theta: "<<alpha_bf1_2<<" "<<sig_theta_bf_2<<std::endl;
		cout<<"for "<<ssrc_file2<<" alpha/theta: "<<alpha_bf2_2<<" "<<sig_theta_bf_2<<std::endl;
	}
	
	//Fit the data with both free
	double sig_theta_bf = 10.;
	double alpha_bf1 = 0.05;
	double alpha_bf2 = 0.05;
	fix_alpha1 = false;
	fix_alpha2 = false;
	bool fix_theta = true;
	double TS_data = TS_fixed_EMulti(vl,vb, exposureMap, vsmoothedMap1, vsmoothedMap2, sig_theta_bf, alpha_bf1, alpha_bf2, logL_0, fix_alpha1, fix_alpha2,fix_theta);
	fix_theta = false;
	TS_data = TS_fixed_EMulti(vl,vb, exposureMap, vsmoothedMap1, vsmoothedMap2, sig_theta_bf, alpha_bf1, alpha_bf2, logL_0, fix_alpha1, fix_alpha2,fix_theta);

	cout<<endl<<endl;
	cout<<"---------------------------"<<endl;
	cout<<sexp1<<" and "<<sexp2<<" - "<<InputData_vscenarios[compo_model]<<endl;
	cout<<"---------------------------"<<endl;
	cout<<"Eth: "<<Eth<<" / nevents = "<<nevents<<endl;
	cout<<"pre-penalization TS: "<<TS_data<<endl;
	cout<<"for "<<ssrc_file1<<" alpha/theta: "<<alpha_bf1<<" "<<sig_theta_bf<<std::endl;
	cout<<"for "<<ssrc_file2<<" alpha/theta: "<<alpha_bf2<<" "<<sig_theta_bf<<std::endl;
	cout<<"---------------------------"<<endl;
	cout<<ssrc_file1<<" and "<<ssrc_file2<<" vs "<<ssrc_file1<<endl;
	cout<<"Delta TS: "<<TS_data-TS_data1<<endl;
	cout<<"p-val: "<<TMath::Prob(TS_data-TS_data1,1.)<<endl;
	cout<<"sigma: "<<sqrt(2.)*TMath::ErfcInverse(TMath::Prob(TS_data-TS_data1,1.))<<endl;
	cout<<"---------------------------"<<endl;
	cout<<ssrc_file1<<" and "<<ssrc_file2<<" vs "<<ssrc_file2<<endl;
	cout<<"Delta TS: "<<TS_data-TS_data2<<endl;
	cout<<"p-val: "<<TMath::Prob(TS_data-TS_data2,1.)<<endl;
	cout<<"sigma: "<<sqrt(2.)*TMath::ErfcInverse(TMath::Prob(TS_data-TS_data2,1.))<<endl;
	cout<<"---------------------------"<<endl;

	////////////////////////////////////////////////////////////////////////////
	//                                                                        //
	//     	                 LOAD TS VS PARAMS                                //
	//                                                                        //
	////////////////////////////////////////////////////////////////////////////

	if(plot){
		//Contour levels for 1, 2 sigma assuming 3 dof
		int ndof = 3, ncont_level = 2;
		vector< double > vsig, vTS;
		double TS_max = 25, dTS=0.1;
		vTS.push_back(0.);
		while(vTS.back()<=TS_max) vTS.push_back(vTS.back()+dTS);
		for(unsigned int i=0; i<vTS.size(); i++) vsig.push_back(sqrt(2)*TMath::ErfcInverse(TMath::Prob(vTS[i],ndof)));
		TGraph* G_sig_TS = new TGraph(vsig.size(),&vsig[0],&vTS[0]);
		vector< double > vcont_level;
		for(int i=1; i<=ncont_level; i++) vcont_level.push_back(TS_data-G_sig_TS->Eval(i));


		//Load the best fit likelihood maps
		double alpha_max = 0.4;
		int nbins = 50.;
		double dalpha = alpha_max/(nbins-1.);
		double dbin = 0.5*dalpha;

		//FREE THETA///////////////////////////////////////////////////////////////////////////////////////
		//scans vs alpha1, alpha2
		vector< double > valpha1_free_theta, valpha2_free_theta, vTS_free_theta;
		fix_alpha1 = true;
		fix_alpha2 = true;
		fix_theta = false;
		double dalpha2 = 0., dalpha1 = 0.;

		gProgB.Zero();
		gProgB.fBegin = 0;
		gProgB.fEnd = nbins;
		gProgB.InitPercent();

		for(int i=0; i<nbins; i++){//alpha_agn
			double alpha2 = i*dalpha;
			double sig_theta = 10.;
			double dsig_theta = 5.;
			for(int j=0; j<nbins; j++){//alpha_sb
				double alpha1 = j*dalpha;
				double logLij = FindMaxLogL_AllMulti(sig_theta, dsig_theta, alpha1, dalpha1, alpha2, dalpha2, fix_theta, fix_alpha1, fix_alpha2);
				if(logLij>-1.E50){
					valpha1_free_theta.push_back(alpha1);
					valpha2_free_theta.push_back(alpha2);
					vTS_free_theta.push_back(2*(logLij-logL_0));
				}
			}
			gProgB.PrintPercent(i);
		}
		gProgB.EndPercent();

		//TS vs alpha1 and alpha2
		TH2D *h_free_theta = LoadHisto("h_theta", "TS = 2#Delta ln L", (InputData_vsfile[cat1]+" anisotropic Fraction").c_str(), (InputData_vsfile[cat2]+" anisotropic Fraction").c_str(),  nbins,-dbin, alpha_max+dbin, nbins,-dbin, alpha_max+dbin, valpha1_free_theta, valpha2_free_theta, vTS_free_theta);
		h_free_theta->GetXaxis()->SetRangeUser(0.,alpha_max);
		h_free_theta->GetYaxis()->SetRangeUser(0.,alpha_max);
		vector< TGraph* > vG_free_theta = LoadContoursAndBestFit(h_free_theta,vcont_level,alpha_bf1,alpha_bf2);

		TLatex tl2;
			tl2.SetNDC();
			tl2.SetTextAlign(12);
			tl2.SetTextSize(1.4*h_free_theta->GetXaxis()->GetTitleSize());
			tl2.SetTextFont(h_free_theta->GetXaxis()->GetTitleFont());
		std::stringstream sstitle;
		sstitle<<InputData_vsfile[cat1]<<" and "<<InputData_vsfile[cat2]<<" - E > "<<Eth<<" EeV";

		//Plot
		gStyle->SetPalette(53);
		TCanvas *c_free_theta = new TCanvas("c_free_theta","c_free_theta");
			c_free_theta->cd();
			c_free_theta->SetLeftMargin(0.10);
			c_free_theta->SetRightMargin(0.13);
			c_free_theta->SetTopMargin(0.10);
			c_free_theta->SetTickx();
			c_free_theta->SetTicky();
		h_free_theta->SetContour(100.);
		h_free_theta->Draw("colz");
		tl2.DrawLatex(0.10,0.95,sstitle.str().c_str());
		for(unsigned int i=0; i<vG_free_theta.size(); i++) vG_free_theta[i]->Draw("samelp");
		c_free_theta->SaveAs((sexp_file+"_likelihood_map_free_theta.pdf").c_str());
	}

}


/// Main //////////////////////////////////////////////////////////////////////////////////////////////
int main(int argc, char* argv[])
{
	////////////////////////////////////////////////////////////////////////////
	//                                                                        //
	//                            Initialization                              //
	//                                                                        //
	////////////////////////////////////////////////////////////////////////////

	// Command line
	if(argc != 1) Usage(argv[0]);

	// Initialize root
	int fargc = 1;
	TRint *rint = NULL;
	rint = new TRint("Test plot", &fargc, argv);
	gROOT->SetStyle("Plain");
	gStyle->SetTitleFont(30,"TITLE");

	// Check  for two thresholds
	std::vector< double > vEth;
	vEth.push_back(38.);

	//Runs the fits
	for(unsigned int compo_model=0; compo_model<InputData_vscenarios.size(); compo_model++){
		for(unsigned int i=0; i<vEth.size(); i++){
//			for(unsigned int cat2=1; cat2<InputData_vsmodelfile.size(); cat2++){
			for(unsigned int cat2=1; cat2<2; cat2++){
				LoadResults(vEth[i], cat2, compo_model);
			}
		}
	}

	////////////////////////////////////////////////////////////////////////////
	//                                                                        //
	//                         End of the code                                //
	//                                                                        //
	////////////////////////////////////////////////////////////////////////////

	cout << endl <<"------ Program Finished Normally: Good Job! ------" << endl;
	gSystem->Exit(0);
	rint->Run(kTRUE);
}
