//Compute the attenuation factor //
//Based on M. Unger's macro      //
//Last edit: J. Biteau 2017-07-17//

#include <iostream>
#include <sstream>
#include <string>
#include <vector>

#include <TFile.h>
#include <TGraph.h>

using namespace std;

struct Scenario {
  Scenario(const string& name,
           const unsigned int modelIndex,
           const double fP, const double fHe, const double fN,
           const double fSi, const double fFe) :
    fName(name), fModel(modelIndex) {

    fFraction[0] = fP;
    fFraction[1] = fHe;
    fFraction[2] = fN;
    fFraction[3] = fSi;
    fFraction[4] = fFe;

  }

  static const unsigned int gnMass = 5;
  std::string fName;
  unsigned int fModel;
  double fFraction[gnMass];
};


double attFac(
	double distance = - 1,//in Mpc 
	int threshold = -1, //in EeV, >= 20
	int scenario = -1//0,1,2,3
){

  static const unsigned int masses[Scenario::gnMass] = {1, 4, 14, 28, 56};

  vector<Scenario> scenarios;
  scenarios.push_back(Scenario("EPO1st", 0, 0.000, 0.673, 0.281, 0.046, 0));
  scenarios.push_back(Scenario("EPO2nd", 1, 0.000, 0.000, 0.798, 0.202, 0));
  scenarios.push_back(Scenario("Sib1st", 2, 0.702, 0.295, 0.003, 0.000, 0));
  scenarios.push_back(Scenario("QGS1st", 3, 0.000, 0.886, 0.114, 0.000, 0));

  if (distance < 0 || threshold < 0 || scenario < 0 ||
      scenario >= (int)scenarios.size()) {
    cout << "\n usage: attFac(distance, threshold, scenario) \n"
         << "       - distance in Mpc \n"
         << "       - integer threshold [EeV]\n"
         << "       - integer spectral index (1 or 2)\n"
         << "       - scenario id: \n";
    for (unsigned int i = 0; i < scenarios.size(); ++i)
      cout << "          * " << i << " = " << scenarios[i].fName << "\n";
    cout << endl;
    return 0;
  }

  const Scenario& s = scenarios[scenario];

  const string crpFilename("anaCRP3.root");
  TFile* crpFile = TFile::Open(crpFilename.c_str());
  if (!crpFile) {
    cerr << " error opening " << crpFilename << endl;
    return 0;
  }

  double sum = 0;
  for (unsigned int iMass = 0; iMass < Scenario::gnMass; ++iMass) {
    ostringstream gname;
    gname << "atten" << masses[iMass] << "_" << threshold - 20 << "_" << s.fModel
          << "_0";
    TGraph* graph = (TGraph*) crpFile->Get(gname.str().c_str());
    if (!graph) {
      cerr << gname.str() << " does not exist " << endl;
      sum = 0;
      break;
    }
    if (distance < *(graph->GetX() + graph->GetN() - 1)) {
      if (distance <= 0)
        sum += s.fFraction[iMass];
      else
        sum += s.fFraction[iMass] * graph->Eval(distance);
    }
  }
  cout << s.fName << ", D = " << distance << " Mpc, E_thresh = "
       << threshold << " EeV, T = " << sum << endl;
  crpFile->Close();
  return sum;
}
