///////////////////////////////////////////////////////
//                                                   //
//   Dev. by Jonathan Biteau (biteau@in2p3.fr)       //
//                       last edit: 2021-05-05       //
///////////////////////////////////////////////////////

// C/C++ classics
#include <iostream>
#include <fstream>
#include <sstream>
#include <vector>

// Toolkit
#include "healpixmap.h"

// ROOT
#include "TRint.h"
#include "TROOT.h"
#include "TStyle.h"
#include "TGraph.h"
#include "TSystem.h"

// DATA INPUT
#include "InputData.h"

// UTILITIES CATALOG ANALYSIS
#include "utilities_LikelihoodCatalogs.h"

#ifdef gcc323
char* operator+( streampos&, char* );
#endif

using namespace std;

void Usage(string sinput)
{
	cout << endl;
	cout << " Synopsis : " << endl;
	cout << sinput << " -h, --help to obtain this message" << endl;
	cout << sinput << " <cat> <compo_model> <Eth>" << endl << endl;
	cout << " Description :" << endl;
	cout << " Scan as a function of threshold energy: output -> Results/Escan_XXX.pdf and Results/Escan_XXX.root"<<endl
		<< " -- that's all folks! -- "
		<< endl<< endl;

	exit(0);
}

//Actual place where all the work is done for a given catalog
void LoadResultsScan(double Eth = 32, int cat = 0, int compo_model=0){

	////////////////////////////////////////////////////////////////////////////
	//                                                                        //
	//                      Model loading                                     //
	//                                                                        //
	////////////////////////////////////////////////////////////////////////////
	
	//Select catalog and set cosmetics
	string ssrc_file = InputData_vsfile[cat];

	//Composition model
	stringstream ssfile_compo;
	ssfile_compo<<"CompoModel/"<<ssrc_file<<"/"<<ssrc_file;
	if(InputData_vscenarios[compo_model].length()>1) ssfile_compo<<"_"<<InputData_vscenarios[compo_model];
	ssfile_compo<<"_threshold"<<int(Eth);	

	////////////////////////////////////////////////////////////////////////////
	//                                                                        //
	//                      Data Loading                                      //
	//                                                                        //
	////////////////////////////////////////////////////////////////////////////
	
	//Note: info on dataset and catalogs stored in InputData.h
	
	//Events///////////////////////////////////////////////////////////////////	
	vector< double > vId, vZen, vl, vb, vE;
	LoadAugerData(InputData_seventfile , vId, vZen, vl, vb, vE);

	//Trim the data below threshold
	Trim(Eth,vE, vZen, vl,vb);

	//Exposure/////////////////////////////////////////////////////////////////
	THealpixMap exposureMap((char*)InputData_sexpofitsfile.c_str());
	exposureMap.SetCoordSys('G');
	unsigned int nSide = exposureMap.NSide();

	////////////////////////////////////////////////////////////////////////////
	//                                                                        //
	//     	                 FIT THE DATA ABOVE Eth                           //
	//                                                                        //
	////////////////////////////////////////////////////////////////////////////

	//Load the model
	vector< string > vSrcName;
	vector< double > vl_model, vb_model, vw_model;
	LoadModel(ssfile_compo.str(),vSrcName, vl_model, vb_model, vw_model);
	vector< THealpixMap > vsmoothedMap = LoadSmoothedModelMaps(nSide, vl_model, vb_model, vw_model);

	//Fit the data
	double sig_theta_bf, alpha_bf, logL_0;
	double TS_data = TS_fixed_E(vl,vb, exposureMap, vsmoothedMap, sig_theta_bf, alpha_bf, logL_0);

	////////////////////////////////////////////////////////////////////////////
	//                                                                        //
	//     	                 EXPORT RESULTS                                   //
	//                                                                        //
	////////////////////////////////////////////////////////////////////////////

	stringstream ssout;
	ssout<<"Results/scan/"<<ssrc_file<<"_"<<InputData_vscenarios[compo_model];	
	cout<<ssout.str().c_str()<<" "<<Eth<<" "<<TS_data<<"\n";
	
	ofstream file_out(ssout.str().c_str(), ofstream::out | ofstream::app);
	if(Eth==32)	file_out<<"//Eth[TeV] TS\n";
	file_out<<Eth<<" "<<TS_data<<"\n";
	file_out.close();
}


/// Main //////////////////////////////////////////////////////////////////////////////////////////////
int main(int argc, char* argv[])
{

	////////////////////////////////////////////////////////////////////////////
	//                                                                        //
	//                            Initialization                              //
	//                                                                        //
	////////////////////////////////////////////////////////////////////////////
	
	// Command line	
	if(argc != 4) Usage(argv[0]);
	int cat = int(atof(argv[1]));
	int compo_model = int(atof(argv[2]));
	int Eth = int(atof(argv[3]));
	
	// Initialize root
	int fargc = 1;
	TRint *rint = NULL;
	rint = new TRint("Test plot", &fargc, argv);
	gROOT->SetStyle("Plain");
	gStyle->SetTitleFont(30,"TITLE");

	//Runs the fit
	LoadResultsScan(Eth, cat, compo_model);

	////////////////////////////////////////////////////////////////////////////
	//                                                                        //
	//                         End of the code                                //
	//                                                                        //
	////////////////////////////////////////////////////////////////////////////

	cout << endl <<"------ Scan Step Finished Normally: Good Job! ------" << endl;
	gSystem->Exit(0);
	rint->Run(kTRUE);
}
