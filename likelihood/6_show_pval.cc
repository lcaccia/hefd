///////////////////////////////////////////////////////
//                                                   //
//   Dev. by Jonathan Biteau (biteau@in2p3.fr)       //
//                       last edit: 2020-11-05       //
///////////////////////////////////////////////////////

// C/C++ classics
#include <iostream>
#include <iomanip>
#include <fstream>
#include <sstream>
#include <vector>

// ROOT
#include "TRint.h"
#include "TROOT.h"
#include "TSystem.h"
#include "TStyle.h"
#include "TCanvas.h"
#include "TH1D.h"
#include "TF1.h"
#include "TMath.h"
#include "TLegend.h"
#include "TGraphErrors.h"

// DATA INPUT
#include "InputData.h"

#ifdef gcc323
char* operator+( streampos&, char* );
#endif

using namespace std;

void Usage(string sinput)
{
	cout << endl;
	cout << " Synopsis : " << endl;
	cout << sinput << " -h, --help to obtain this message" << endl;
	cout << sinput << " " << endl << endl;

	cout << " Description :" << endl;
	cout << " Simulate isotropic maps starting from <seed>, fits them, and stores the TS values in a file"<<endl
			 << " -- that's all folks! -- "
			 << endl<< endl;

	exit(0);
}

/// Utilities /////////////////////////////////////////////////////////////////////////////////////////
double fun_sigma(double *x, double *par){
	double res=0.;
	double p=TMath::Prob(x[0],2)*2*(par[0]+par[1]*x[0]);
	if(p<0.5) res=sqrt(2.)*TMath::ErfcInverse(p);
	return res;
}


int count_above(TH1D* hTS, int i_first_count_bin){
	int n=0;
	for(int i=i_first_count_bin; i<=hTS->GetNbinsX(); i++) n+=hTS->GetBinContent(i);
	return n;
}

//Downward curved parabolic fit in lin-log
double FitNorm(TGraphErrors *G, double xmin = 0, double xmax = 10){

	TF1* f = new TF1("f","[0]*TMath::Exp([1]*x+[2]*x*x)",xmin, xmax);
	f->SetParameters(1,-0.5,0);
	f->SetParLimits(2,-1,0);//prevents upward going curve
	G->Fit(f,"","",xmin, xmax);
	double p0 = f->GetParameter(0);
	
	delete f;

	return p0;
}

//Load the histo storing the simulated TS
vector< TGraphErrors* > LoadTS(vector< string > vfiles){	
	vector< TGraphErrors* > vG;

	//Histogram storing the number of counts of TS
	double xmin = 2., xmax = 50.;
	double bin_size = 0.5;
	int nbins = (int)(xmax-xmin)/bin_size;
	TH1D* hTS = new TH1D("hTS","",nbins,xmin,xmax);
	TH1D* hTS0 = new TH1D("hTS0","",nbins,xmin,xmax);

	double TS_data=0.;
	//Filling of the histogram might be long
	for(unsigned int i=0; i<vfiles.size(); i++){
		//Load data from file
		ifstream myfile;	
		string line;
		myfile.open(("Simu/"+vfiles[i]).c_str());
		if(myfile.is_open()){
			getline (myfile,line);
			getline (myfile,line);
			stringstream ss0(line);
			ss0>>TS_data;
			getline (myfile,line);
			//loops on the lines
			while(myfile.good()){
				getline (myfile,line);
				stringstream ss(line);
				double buf;
				vector< double > vbuf;
				while(ss>>buf){
					if(buf>-10. && buf<1E4)	vbuf.push_back(buf);//Avoid buggy sims
				}
				if(vbuf.size()==2){
					hTS0->Fill(vbuf[0]);
					hTS->Fill(vbuf[1]);
				}
				else if(line.size()>0) cout<<"Line unread in event file: "<<line<<endl;
			}
			myfile.close();
		}
		else cout<<"Could not open file: "<<vfiles[i]<<endl;
	}
	
	//Graphs storing the p-value vs TS
	int i_first_count_bin=0;//starts counting only above TS>1.5, to account for numerical leakage: algo can get stuck around TS=0.
	int nTS_tot = count_above(hTS, i_first_count_bin);
	int nTS_tot0 = count_above(hTS0, i_first_count_bin);

	vector< double > vTS, vdTS, vpval, vdpval, vpval0, vdpval0;
	for(int i=i_first_count_bin; i<=hTS->GetNbinsX(); i++){
		double TS = hTS->GetBinCenter(i);
		double dTS = 0.5*hTS->GetBinWidth(i);
		int ncounts_above = count_above(hTS, i);
		int ncounts_above0 = count_above(hTS0, i);
		
		if(TS>=0 && ncounts_above>0 && ncounts_above0>0){
			double pval = ncounts_above*1./nTS_tot;
			double pval0 = ncounts_above0*1./nTS_tot0;
			vTS.push_back(TS);
			vdTS.push_back(dTS);
			vpval.push_back(pval);
			vdpval.push_back(pval/sqrt(1.*ncounts_above));
			vpval0.push_back(pval0);
			vdpval0.push_back(pval0/sqrt(1.*ncounts_above0));
		}
	}
	
	//Load the graphs - normalized through a parabolic fit in lin-log
	TGraphErrors *Gpval_no_norm = new TGraphErrors(vTS.size(), &vTS[0], &vpval[0], 0, &vdpval[0]);
	double norm = FitNorm(Gpval_no_norm);
	
	TGraphErrors *Gpval0_no_norm = new TGraphErrors(vTS.size(), &vTS[0], &vpval0[0], 0, &vdpval0[0]);
	double norm0 = FitNorm(Gpval0_no_norm);
	
	for(unsigned int i=0; i<vTS.size(); i++){
		vpval[i]/=norm;
		vdpval[i]/=norm;
		vpval0[i]/=norm0;
		vdpval0[i]/=norm0;
	}
	TGraphErrors *Gpval = new TGraphErrors(vTS.size(), &vTS[0], &vpval[0], &vdTS[0], &vdpval[0]);
	vG.push_back(Gpval);
	TGraphErrors *Gpval0 = new TGraphErrors(vTS.size(), &vTS[0], &vpval0[0], &vdTS[0], &vdpval0[0]);
	vG.push_back(Gpval0);
	
	return vG;
}

//Compute penalty factor = post-scan p-value / expected p-value from chi2 with 2 d.o.f.
vector< TGraphErrors* > LoadPenalty(vector< vector< TGraphErrors* > > vGpval, TF1* f){
	vector< TGraphErrors* > vG;
	
	for(unsigned int j=0; j<vGpval.size(); j++){
		vector< double > vTSpen, vdTSpen, vPen, vdPen;
		for(int i=0; i<vGpval[j][0]->GetN(); i++){
			double TS = vGpval[j][0]->GetX()[i];
			double norm = f->Eval(TS);
			vTSpen.push_back(TS);
			vdTSpen.push_back(vGpval[j][0]->GetErrorX(i));
			vPen.push_back(vGpval[j][0]->GetY()[i]/norm);
			vdPen.push_back(vGpval[j][0]->GetErrorY(i)/norm);		
		}
		TGraphErrors *G = new TGraphErrors(vTSpen.size(), &vTSpen[0], &vPen[0], &vdTSpen[0], &vdPen[0]);
		vG.push_back(G);
	}

	return vG;
}

/// Main //////////////////////////////////////////////////////////////////////////////////////////////
int main(int argc, char* argv[])
{
	////////////////////////////////////////////////////////////////////////////
	//                                                                        //
	//                            Initialization                              //
	//                                                                        //
	////////////////////////////////////////////////////////////////////////////

	// Command line
	if(argc != 1) Usage(argv[0]);

	// Initialize root
	int fargc = 1;
	TRint *rint = NULL;
	rint = new TRint("Test plot", &fargc, argv);
	gROOT->SetStyle("Plain");
	gStyle->SetTitleFont(30,"TITLE");

	////////////////////////////////////////////////////////////////////////////
	//                                                                        //
	//                      Simulation loading                                //
	//                                                                        //
	////////////////////////////////////////////////////////////////////////////
	Color_t col[4] = {kAzure+6, kMagenta+1, kOrange+8, kSpring-8};
	int mstyle[4] = {21, 34, 20, 33};
	int mstyle0[4] = {25, 28, 24, 27};
	double msize[4] = {0.7, 0.8, 0.8, 1.0};
	
	vector< vector< string > > vfiles;
	vfiles.push_back(InputData_vsim_files_SBG);
	vfiles.push_back(InputData_vsim_files_gAGN);
	vfiles.push_back(InputData_vsim_files_xAGN);
	vfiles.push_back(InputData_vsim_files_NIRgal);
	
	vector < string > vexp;
	for(unsigned int i=0; i<vfiles.size(); i++) vexp.push_back(InputData_vssources[i]);
	
	//Histo storing the simulated TS
	vector< vector< TGraphErrors* > > vGpval;
	for(unsigned j=0; j<vfiles.size(); j++){
		vGpval.push_back(LoadTS(vfiles[j]));
		for(unsigned int i=0; i<2; i++){
			vGpval[j][i]->SetMarkerStyle(mstyle[j]*(i==0)+mstyle0[j]*(i==1));
			vGpval[j][i]->SetMarkerSize(msize[j]);
			vGpval[j][i]->SetMarkerColor(col[j]+i);
			vGpval[j][i]->SetLineColor(col[j]+i);
		}
	}

	//Expected function for two degrees of freedom
	double TS_max_plot = 40.;
	TF1 *f = new TF1("f","TMath::Prob(x,2)",0.,TS_max_plot);	//expected distribution 
	f->SetLineWidth(2);

	//Graph with penalty
	vector< TGraphErrors* > vGpenalty = LoadPenalty(vGpval, f);
	for(unsigned j=0; j<vGpenalty.size(); j++){
		vGpenalty[j]->SetMarkerStyle(mstyle[j]);
		vGpenalty[j]->SetMarkerSize(msize[j]);
		vGpenalty[j]->SetMarkerColor(col[j]+1);
		vGpenalty[j]->SetLineColor(col[j]+1);
	}

	////////////////////////////////////////////////////////////////////////////
	//                                                                        //
	//                     Plot p-val vs TS                                   //
	//                                                                        //
	////////////////////////////////////////////////////////////////////////////

	//Plot pval vs TS
	TH1D *hGraph0 = new TH1D("hGraph0","",100,0.,30.);
		hGraph0->SetStats(0);
		hGraph0->SetDirectory(0);
		hGraph0->SetMaximum(1.5);
		hGraph0->SetMinimum(1E-6);
		hGraph0->GetXaxis()->SetTitle("Test Statistics");
		hGraph0->GetYaxis()->SetTitle("p-value");
		hGraph0->GetXaxis()->SetTitleOffset(1.2);
		hGraph0->GetYaxis()->SetTitleOffset(1.3);
		hGraph0->GetXaxis()->CenterTitle();
		hGraph0->GetYaxis()->CenterTitle();

	TCanvas *cgraph0 = new TCanvas("p-val","p-val",1200,900);
		cgraph0->SetLeftMargin(0.10);
		cgraph0->SetTopMargin(0.10);
		cgraph0->SetBottomMargin(0.10);
		cgraph0->SetRightMargin(0.10);
		cgraph0->SetTickx();
		cgraph0->SetTicky();
		cgraph0->Divide(2,2);

	
	for(unsigned j=0; j<vGpval.size(); j++){
		TLegend *leg0 = new TLegend(0.13,0.12,0.3,0.28);
			leg0->SetLineColor(kWhite); 
			leg0->SetFillColor(kWhite);
			leg0->SetMargin(0.3); 
			leg0->SetTextSize(hGraph0->GetXaxis()->GetTitleSize());
			leg0->SetHeader(vexp[j].c_str());
			leg0->AddEntry(vGpval[j][0],"Isotropic sims - E-scan","pl");
			leg0->AddEntry(vGpval[j][1],"Isotropic sims - no E-scan","pl");
			leg0->AddEntry(f,"#chi^{2} with 2 d.o.f.","l");
		cgraph0->cd(j+1);
		cgraph0->cd(j+1)->SetLogy();
		hGraph0->Draw();
		leg0->Draw();

		f->Draw("same l");
		vGpval[j][1]->Draw("same pz");
		vGpval[j][0]->Draw("same pz");
	}
	cgraph0->SaveAs("Results/sim_pvalues.pdf");

	////////////////////////////////////////////////////////////////////////////
	//                                                                        //
	//                     Plot penalty vs TS                                 //
	//                                                                        //
	////////////////////////////////////////////////////////////////////////////

	//Plot pval vs TS
	TH1D *hGraph1 = new TH1D("hGraph1","",100,0.,30.);
		hGraph1->SetStats(0);
		hGraph1->SetDirectory(0);
		hGraph1->SetMaximum(15.);
		hGraph1->SetMinimum(0);
		hGraph1->GetXaxis()->SetTitle("Test Statistics");
		hGraph1->GetYaxis()->SetTitle("Penalty");
		hGraph1->GetXaxis()->SetTitleOffset(1.2);
		hGraph1->GetYaxis()->SetTitleOffset(1.3);
		hGraph1->GetXaxis()->CenterTitle();
		hGraph1->GetYaxis()->CenterTitle();

	TLegend *leg1 = new TLegend(0.43,0.12,0.7,0.28);
		leg1->SetLineColor(kWhite); 
		leg1->SetFillColor(kWhite);
		leg1->SetMargin(0.3); 
		leg1->SetTextSize(hGraph1->GetXaxis()->GetTitleSize());
		for(unsigned int j=0; j<vGpenalty.size(); j++) leg1->AddEntry(vGpenalty[j],vexp[j].c_str());

	TCanvas *cgraph1 = new TCanvas("penalty","penalty");
		cgraph1->SetLeftMargin(0.10);
		cgraph1->SetTopMargin(0.10);
		cgraph1->SetRightMargin(0.10);
		cgraph1->SetTickx();
		cgraph1->SetTicky();
		
		hGraph1->Draw();
		leg1->Draw();
		for(unsigned j=0; j<vGpenalty.size(); j++) vGpenalty[j]->Draw("same pz");
		
	cgraph1->SaveAs("Results/sim_penalty.pdf");
		
	////////////////////////////////////////////////////////////////////////////
	//                                                                        //
	//                         End of the code                                //
	//                                                                        //
	////////////////////////////////////////////////////////////////////////////

	cout << endl <<"------ Program Finished Normally: Good Job! ------" << endl;
//	gSystem->Exit(0);
	rint->Run(kTRUE);
}
