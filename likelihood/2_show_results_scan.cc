///////////////////////////////////////////////////////
//                                                   //
//   Dev. by Jonathan Biteau (biteau@in2p3.fr)       //
//                       last edit: 2020-11-05       //
///////////////////////////////////////////////////////

// C/C++ classics
#include <iostream>
#include <fstream>
#include <sstream>
#include <vector>

// ROOT
#include "TRint.h"
#include "TROOT.h"
#include "TStyle.h"
#include "TMath.h"
#include "TCanvas.h"
#include "TLatex.h"
#include "TGraph.h"
#include "TH1D.h"
#include "TGaxis.h"
#include "TLegend.h"
#include "TSystem.h"

//DATA INPUT
#include "InputData.h"

#ifdef gcc323
char* operator+( streampos&, char* );
#endif

using namespace std;


void Usage(string sinput)
{
	cout << endl;
	cout << " Synopsis : " << endl;
	cout << sinput << " -h, --help to obtain this message" << endl;
	cout << sinput << " " << endl << endl;

	cout << " Description :" << endl;
	cout << " Scan as a function of threshold energy: output -> Results/Escan_XXX.pdf and Results/Escan_XXX.root"<<endl
			 << " -- that's all folks! -- "
			 << endl<< endl;

	exit(0);
}

//Load the results obtained with a given composition model, defined in InputData
void LoadScan_fixed_compo(int compo_model=0){

	////////////////////////////////////////////////////////////////////////////
	//                                                                        //
	//            Load the results for a fixed composition                    //
	//                                                                        //
	////////////////////////////////////////////////////////////////////////////
	
	//Cosmetics
	Color_t col[4] = {kAzure+6, kMagenta+1, kOrange+8, kSpring-8};
	int lstyle[4] = {1, 7, 2, 9};
	int lwidth[4] = {2, 3, 3, 2};
	int mstyle[4] = {21, 25, 20, 27};

	//Loop on the files
	vector< string > vcat;
	vector< TGraph* > vG;
	for(unsigned int j =0; j<InputData_vsfile.size(); j++){
		string filename = "Results/scan/"+InputData_vsfile[j]+"_"+InputData_vscenarios[compo_model];

		stringstream catname;
		catname<<"#color["<<col[j]+1<<"]{"<<InputData_vssources[j]<<"}";
		vcat.push_back(catname.str());

		TGraph* G = new TGraph(filename.c_str());
		G->SetLineStyle(lstyle[j]);
		G->SetMarkerStyle(mstyle[j]);
		G->SetLineColor(col[j]+1);
		G->SetLineWidth(lwidth[j]);
		G->SetMarkerColor(col[j]+1);
		G->SetMarkerSize(0.85);
		vG.push_back(G);
	}
	
	////////////////////////////////////////////////////////////////////////////
	//                                                                        //
	//            Plot the results for a fixed composition                    //
	//                                                                        //
	////////////////////////////////////////////////////////////////////////////

	//Range of the plot
	double TS_max = 30;
	double Emin = 31, Emax = 81;
	TH1D *hGraph = new TH1D("hGraph","",100,Emin, Emax);
		hGraph->SetStats(0);
		hGraph->SetDirectory(0);
		hGraph->SetMinimum(0);
		hGraph->SetMaximum(TS_max);
		hGraph->GetYaxis()->SetTitle("Test statistic, TS = 2#Delta ln L");
		hGraph->GetXaxis()->SetTitle("Threshold energy  [EeV]");
		hGraph->GetXaxis()->SetTitleOffset(1.2);
		hGraph->GetYaxis()->SetTitleOffset(1.3);
		hGraph->GetXaxis()->CenterTitle();
		hGraph->GetYaxis()->CenterTitle();

	TGaxis *G = new TGaxis(81,TS_max,81.001,0,TMath::Prob(TS_max,2.),1.,510,"G-");
		G->SetTitle("Local p-value, #it{P}_{#chi^{2}}(TS,2)");
		G->CenterTitle();
		G->SetTitleOffset(1.6);
		G->SetLabelOffset(0.06);

	TLegend *leg0 = new TLegend(0.53,0.88-0.04*(vG.size()+1),0.83,0.88);
		leg0->SetLineColor(kWhite); 
		leg0->SetFillColor(kWhite);
		leg0->SetMargin(0.2); 
		leg0->SetTextSize(hGraph->GetXaxis()->GetTitleSize()); 
		for(unsigned int i=0; i<vcat.size(); i++) leg0->AddEntry(vG[i],("#bf{"+vcat[i]+"}").c_str(),"lp");
		
	TLatex tl;
		tl.SetNDC();
		tl.SetTextAlign(12);
		tl.SetTextSize(hGraph->GetXaxis()->GetTitleSize());
		tl.SetTextFont(hGraph->GetXaxis()->GetTitleFont());	

	//Graph of the TS
	TCanvas *c_TS_E = new TCanvas(("c"+InputData_vscenarios[compo_model]).c_str(),("c"+InputData_vscenarios[compo_model]).c_str());
		c_TS_E->SetLeftMargin(0.10);
		c_TS_E->SetRightMargin(0.10);
		c_TS_E->SetTopMargin(0.10);
		c_TS_E->SetTickx();
		hGraph->Draw();
		tl.DrawLatex(0.13,0.19,"#bf{Attenuation model:}");
		tl.DrawLatex(0.13,0.15,("#bf{"+InputData_vname_scenarios[compo_model]+"}").c_str());
		G->Draw();
		leg0->Draw();
		
		for(int i=vcat.size()-1; i>-1; i--) vG[i]->Draw("same lp");
		c_TS_E->SaveAs(("Results/Escan_"+InputData_vscenarios[compo_model]+".pdf").c_str());
		c_TS_E->SaveAs(("Results/Escan_"+InputData_vscenarios[compo_model]+".root").c_str());
}

/// Main //////////////////////////////////////////////////////////////////////////////////////////////
int main(int argc, char* argv[])
{
	////////////////////////////////////////////////////////////////////////////
	//                                                                        //
	//                            Initialization                              //
	//                                                                        //
	////////////////////////////////////////////////////////////////////////////

	// Command line
	if(argc != 1) Usage(argv[0]);
	
	// Initialize root
	int fargc = 1;
	TRint *rint = NULL;
	rint = new TRint("Test plot", &fargc, argv);
	gROOT->SetStyle("Plain");
	gStyle->SetTitleFont(30,"TITLE");

	////////////////////////////////////////////////////////////////////////////
	//                                                                        //
	//              Plot the results for a fixed composition                  //
	//                                                                        //
	////////////////////////////////////////////////////////////////////////////

	for(unsigned int i=0; i<InputData_vscenarios.size(); i++) LoadScan_fixed_compo(i);

	////////////////////////////////////////////////////////////////////////////
	//                                                                        //
	//                         End of the code                                //
	//                                                                        //
	////////////////////////////////////////////////////////////////////////////

	cout << endl <<"------ Program Finished Normally: Good Job! ------" << endl;
//	gSystem->Exit(0);
	rint->Run(kTRUE);
}


