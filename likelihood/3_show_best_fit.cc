///////////////////////////////////////////////////////
//                                                   //
//   Dev. by Jonathan Biteau (biteau@in2p3.fr)       //
//                       last edit: 2021-05-05       //
///////////////////////////////////////////////////////

// C/C++ classics
#include <iostream>
#include <fstream>
#include <vector>

// Toolkit
#include "projmap.h"
#include "healpixmap.h"

// ROOT
#include "TRint.h"
#include "TROOT.h"
#include "TH2D.h"
#include "TMath.h"
#include "TStyle.h"
#include "TLatex.h"
#include "TGraph.h"
#include "TLegend.h"
#include "TSystem.h"

// DATA INPUT
#include "InputData.h"

// UTILITIES CATALOG ANALYSIS
#include "utilities_LikelihoodCatalogs.h"

#ifdef gcc323
char* operator+( streampos&, char* );
#endif

using namespace std;


void Usage(string sinput)
{
	cout << endl;
	cout << " Synopsis : " << endl;
	cout << sinput << " -h, --help to obtain this message" << endl;
	cout << sinput << "" << endl << endl;

	cout << " Description :" << endl;
	cout << " Fits the model above the best threshold energy (manual edit)"<<endl
		<< " -- that's all folks! -- "
		<< endl<< endl;

	exit(0);
}

//Actual place where all the work is done for a given catalog
void LoadResultsScan(double Eth = 32, int cat = 0, int compo_model=0){

	////////////////////////////////////////////////////////////////////////////
	//                                                                        //
	//                      Model loading                                     //
	//                                                                        //
	////////////////////////////////////////////////////////////////////////////
	
	//Select catalog and set cosmetics
	stringstream ssEth;
	ssEth<<"#it{E} > "<<Eth<<" EeV";
	
	string sexp = InputData_vssources[cat]+" - "+ssEth.str(); 
	string ssrc_file = InputData_vsfile[cat];
	string sexp_obs = ssEth.str();
	string sexp_file = "Results/auger_"+ssrc_file+"_";	

	bool draw_faint_source = InputData_vdraw_faint_src[cat];
	
	int nxs_repr = 60, nres_repr = 30;
	if(Eth> 50){
		nxs_repr = 40;
		nres_repr = 10;	
	}

	//Composition model
	sexp_file+=InputData_vscenarios[compo_model];
	stringstream ssfile_compo;
	ssfile_compo<<"CompoModel/"<<ssrc_file<<"/"<<ssrc_file;
	if(InputData_vscenarios[compo_model].length()>1) ssfile_compo<<"_"<<InputData_vscenarios[compo_model];
	ssfile_compo<<"_threshold"<<int(Eth);	

	////////////////////////////////////////////////////////////////////////////
	//                                                                        //
	//                      Data Loading                                      //
	//                                                                        //
	////////////////////////////////////////////////////////////////////////////
	
	//Note: info on dataset and catalogs stored in InputData.h
	
	//Events///////////////////////////////////////////////////////////////////	
	// Read the event file
	vector< double > vId, vZen, vl, vb, vE;
	LoadAugerData(InputData_seventfile , vId, vZen, vl, vb, vE);

	//Trim the data below threshold
	Trim(Eth,vE, vZen, vl,vb);

	//Exposure/////////////////////////////////////////////////////////////////
	THealpixMap exposureMap((char*)InputData_sexpofitsfile.c_str());
	exposureMap.SetCoordSys('G');
	unsigned int nSide = exposureMap.NSide();

	
	////////////////////////////////////////////////////////////////////////////
	//                                                                        //
	//     	                 FIT THE DATA ABOVE Eth                           //
	//                                                                        //
	////////////////////////////////////////////////////////////////////////////

	//Load the model
	vector< string > vSrcName;
	vector< double > vl_model, vb_model, vw_model;
	LoadModel(ssfile_compo.str(),vSrcName, vl_model, vb_model, vw_model);
	vector< THealpixMap > vsmoothedMap = LoadSmoothedModelMaps(nSide, vl_model, vb_model, vw_model);

	//Fit the data
	unsigned int nevents = vl.size();
	double sig_theta_bf, alpha_bf, logL_0;

	double TS_data = TS_fixed_E(vl,vb, exposureMap, vsmoothedMap, sig_theta_bf, alpha_bf, logL_0);

	//Print out the signficance
	double pval = TMath::Prob(TS_data,2);
	double sigma = sqrt(2.)*TMath::ErfcInverse(2.*pval);
	cout<<endl<<endl;
	cout<<"---------------------------"<<endl;
	cout<<sexp<<" - "<<InputData_vscenarios[compo_model]<<endl;
	cout<<"---------------------------"<<endl;
	cout<<"Eth: "<<Eth<<" / nevents = "<<nevents<<endl;
	cout<<"pre-penalization TS: "<<TS_data<<endl;
	cout<<"pre-penalization p-value: "<<pval<<endl;
	cout<<"pre-penalization 1-sided sigma: "<<sigma<<endl;

	////////////////////////////////////////////////////////////////////////////
	//                                                                        //
	//     	                 LOAD TS VS PARAMS                                //
	//                                                                        //
	////////////////////////////////////////////////////////////////////////////

	//Load the best fit likelihood maps
	double alpha_max = 0.4, theta_min = 3, theta_max=40;//range of the parameter space to plot
	int nbins = 200.;
	double dalpha = alpha_max/(nbins-1.);
	double dtheta = (theta_max-theta_min)/(nbins-1.);

	//scans vs alpha, theta
	vector< double > valpha_theta, vtheta, vTS_theta;
	for(int i=0; i<nbins; i++){
		double theta = theta_min+i*dtheta;
		for(int j=0; j<nbins; j++){
			double alpha_tmp = j*dalpha;
			double logLij = LogLikelihood(theta, alpha_tmp);
			if(logLij>-1.E50){
				valpha_theta.push_back(alpha_tmp);
				vtheta.push_back(theta);
				vTS_theta.push_back(2*(logLij-logL_0));
			}
		}
	}
	//Contour levels for 1, 2 sigma assuming 2 dof
	int ndof = 2, ncont_level = 2;
	vector< double > vsig, vTS;
	double TS_max = 25, dTS=0.1;
	vTS.push_back(0.);
	while(vTS.back()<=TS_max) vTS.push_back(vTS.back()+dTS);
	for(unsigned int i=0; i<vTS.size(); i++) vsig.push_back(sqrt(2)*TMath::ErfcInverse(TMath::Prob(vTS[i],ndof)));
	TGraph* G_sig_TS = new TGraph(vsig.size(),&vsig[0],&vTS[0]);
	vector< double > vcont_level;
	for(int i=1; i<=ncont_level; i++) vcont_level.push_back(TS_data-G_sig_TS->Eval(i));

	//Plot TS vs ...
	double dbin = 0.5*dalpha;
	double dbin_theta = 0.5*dtheta;
	string sxtitle = "Anisotropic Fraction";

	TH2D *h_theta = LoadHisto("h_theta", "TS = 2#Delta ln L", sxtitle, "Search Radius [ ^{o}^{ } ]",  nbins,-dbin, alpha_max+dbin, nbins,theta_min-dbin_theta, theta_max+dbin_theta, valpha_theta, vtheta, vTS_theta);
	h_theta->GetXaxis()->SetRangeUser(0.,alpha_max);
	h_theta->GetYaxis()->SetRangeUser(theta_min,theta_max);
	vector< TGraph* > vG_theta = LoadContoursAndBestFit(h_theta,vcont_level,alpha_bf,sig_theta_bf);

	TLatex tl;
		tl.SetNDC();
		tl.SetTextAlign(12);
		tl.SetTextSize(1.4*h_theta->GetXaxis()->GetTitleSize());
		tl.SetTextFont(h_theta->GetXaxis()->GetTitleFont());

	//Plot
	gStyle->SetPalette(53);
	TCanvas *cTheta = new TCanvas("cTheta","cTheta");
		cTheta->cd();
		cTheta->SetLeftMargin(0.10);
		cTheta->SetRightMargin(0.13);
		cTheta->SetTopMargin(0.10);
		cTheta->SetTickx();
		cTheta->SetTicky();
	h_theta->SetContour(100.);
	h_theta->Draw("colz");
	tl.DrawLatex(0.10,0.95,sexp.c_str());
	for(unsigned int i=0; i<vG_theta.size(); i++) vG_theta[i]->Draw("samelp");
	cTheta->SaveAs((sexp_file+"_likelihood_map.pdf").c_str());

	////////////////////////////////////////////////////////////////////////////
	//                                                                        //
	//                Filter the excess map with the best-fit                 //
	//                                                                        //
	////////////////////////////////////////////////////////////////////////////

	// Load the list of pixels with non-zero content and the event map for final plotting purpose	
	THealpixMap eventMap(nSide,'G');
	for(unsigned int i=0; i<nevents; i++){
		double l=vl[i], b=vb[i];
		int pix = exposureMap.Ip(l,b);
		eventMap[pix]+=1;
	}

	//Load the isotropic map
	THealpixMap modelMapIsotropic(nSide,'G');
	double integralExposure = exposureMap.Total();
	modelMapIsotropic=exposureMap*(nevents/integralExposure);

	//Best-fit lobe
	vector< vector< double > > vsmoothing = LoadSmoothingVectors(sig_theta_bf);

	// Load an excess point source map
	THealpixMap xsPtMap(nSide,'G');
	double ip = xsPtMap.Ip(0., 0.);
	xsPtMap[ip]+=1.;

	//excess point map
	THealpixMap xsPtModelMap(nSide,'G');
	xsPtModelMap = xsPtMap.Filter(vsmoothing[0], vsmoothing[1]);
	xsPtModelMap = xsPtModelMap.Filter(vsmoothing[0], vsmoothing[1]);
	xsPtModelMap *= (nxs_repr/xsPtModelMap.Total());

	double norm_fact = (nxs_repr/xsPtModelMap.Max());
	xsPtModelMap *= norm_fact;

	//excess model map
	THealpixMap inputMap(nSide,'G');
	for(unsigned int i=0; i<vl_model.size(); i++){
		double l=vl_model[i], b=vb_model[i];
		double ip = inputMap.Ip(l, b);
		inputMap[ip]+=vw_model[i];
	}
	THealpixMap modelMap(nSide,'G');
	modelMap = inputMap.Filter(vsmoothing[0], vsmoothing[1])*exposureMap;
	modelMap *= (nevents/modelMap.Total());
	
	THealpixMap excessModelMap(nSide,'G');
	excessModelMap = norm_fact*alpha_bf*modelMap.Filter(vsmoothing[0], vsmoothing[1]);

	//Compute the effective weight of each source by averaging - used for the size of the markers
	vector< double > vradius;
	for(unsigned int i=0; i<vl_model.size(); i++){
		double area=0.;
		double ip = exposureMap.Ip(vl_model[i], vb_model[i]);
		if(cat<2){//if not too many source, compute the actual integrale
			// Load the model map
			THealpixMap mapTemp(nSide,'G');
			mapTemp[ip]+=vw_model[i];
			mapTemp = mapTemp.Filter(vsmoothing[0], vsmoothing[1])*exposureMap;
			area = mapTemp.Total();
		}
		else{//otherwise approximate with a Dirac
			area = exposureMap[ip]*vw_model[i];
		}
		vradius.push_back(sqrt(area));
	}

	//excess map wrt isotropic bckgd
	THealpixMap excessMap(nSide,'G');
	excessMap = norm_fact*(eventMap-modelMapIsotropic*(1-alpha_bf)).Filter(vsmoothing[0], vsmoothing[1]);

	//residual map
	THealpixMap resMap(nSide,'G');
	resMap = excessMap-excessModelMap;

	//excess beam - title
	stringstream ss_xs_beam_title;
	ss_xs_beam_title<<"N_{evts} = "<<nxs_repr;

	//residual beam - title
	stringstream ss_res_beam_title;
	ss_res_beam_title<<"N_{evts} = "<<nres_repr;


	// Transform the map in a TH2D
	double ndisp_min = min(min(excessMap.Min(),excessModelMap.Min()),resMap.Min());
	double ndisp_max = max(max(excessMap.Max(),excessModelMap.Max()),resMap.Max());

	//Excess map
	TProjMap excessMapProj = LoadProjMapWithInsert("Observed Excess Map - "+sexp_obs, ss_xs_beam_title.str(), ndisp_min, ndisp_max, excessMap, xsPtModelMap, vl_model, vb_model, vradius, vSrcName,draw_faint_source);
	excessMapProj.Save((sexp_file+"_excess_map_obs.pdf").c_str());

	//Excess model map
	TProjMap excessModelMapProj = LoadProjMapWithInsert("Model Excess Map - "+sexp, ss_xs_beam_title.str(), ndisp_min, ndisp_max, excessModelMap, xsPtModelMap, vl_model, vb_model, vradius, vSrcName,draw_faint_source);
	excessModelMapProj.Save((sexp_file+"_excess_map_model.pdf").c_str());

	//Residual map
	TProjMap resMapProj = LoadProjMapWithInsert("Residual Excess Map - "+sexp, ss_res_beam_title.str(), ndisp_min, ndisp_max, resMap, xsPtModelMap*nres_repr/nxs_repr, vl_model, vb_model, vradius, vSrcName,draw_faint_source);
	resMapProj.Save((sexp_file+"_residual_map.pdf").c_str());

	//flux map
	THealpixMap fluxMap = LoadSrcMap(nSide, sig_theta_bf, vl_model, vb_model, vw_model);
	TProjMap fluxMapProj = LoadProjMap("Model Flux Map - "+sexp, fluxMap, vl_model, vb_model, vw_model, vSrcName,draw_faint_source);
	fluxMapProj.Save((sexp_file+"_flux_map.pdf").c_str());
	
}


/// Main //////////////////////////////////////////////////////////////////////////////////////////////
int main(int argc, char* argv[])
{
	////////////////////////////////////////////////////////////////////////////
	//                                                                        //
	//                            Initialization                              //
	//                                                                        //
	////////////////////////////////////////////////////////////////////////////

	// Command line
	if(argc != 1) Usage(argv[0]);

	// Initialize root
	int fargc = 1;
	TRint *rint = NULL;
	rint = new TRint("Test plot", &fargc, argv);
	gROOT->SetStyle("Plain");
	gStyle->SetTitleFont(30,"TITLE");

	//Runs the fits
//	for(unsigned int compo_model=0; compo_model<InputData_vscenarios.size(); compo_model++){
//		for(unsigned int i=0; i<InputData_vsmodelfile.size(); i++){
	for(unsigned int compo_model=0; compo_model<1; compo_model++){
		for(unsigned int i=0; i<1; i++){		
			cout<<"---------------------------"<<endl;
			cout<<InputData_vsmodelfile[i]<<" - "<<InputData_vscenarios[compo_model]<<endl;
			cout<<"---------------------------"<<endl;
			LoadResultsScan(InputData_vBestThresholds[i], i, compo_model);
		}
	}

	////////////////////////////////////////////////////////////////////////////
	//                                                                        //
	//                         End of the code                                //
	//                                                                        //
	////////////////////////////////////////////////////////////////////////////

	cout << endl <<"------ Program Finished Normally: Good Job! ------" << endl;
	gSystem->Exit(0);
	rint->Run(kTRUE);
}
