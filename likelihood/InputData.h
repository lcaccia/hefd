using namespace std;

////////////////////////////////////////////////////////////////////////////
//                                                                        //
//                Pierre Auger Observatory Data                           //
//                                                                        //
////////////////////////////////////////////////////////////////////////////

string InputData_seventfile = "../Data/HEFD_v2r0_Id_Th_RA_Dec_E";

string InputData_sexpofitsfile_vert = "../Data/exposure_Auger_Vertical64.fits";	
string InputData_sexpofitsfile_hor = "../Data/exposure_Auger_Horizontal64.fits";

int InputData_nvert32 = 2032;//# vertical events > 32 EeV
int InputData_nhor32 = 593;//# inclined events > 32 EeV

////////////////////////////////////////////////////////////////////////////
//                                                                        //
//                        Catalogs                                        //
//                                                                        //
////////////////////////////////////////////////////////////////////////////

//Location of the catalogs
vector< string > InputData_vsmodelfile = {"Catalog/Lunardini_SBG.dat","Catalog/Fermi3FHL_AGN.dat","Catalog/SwiftBAT105_AGN.dat","Catalog/2MASS_HyperLEDA.dat"};
vector< string > InputData_vssources = {"Starburst galaxies (radio)", "Jetted AGN (#gamma-rays)", "All AGN (hard X-rays)", "Galaxies > 1 Mpc (IR)"};//Associated display name
vector< string > InputData_vsfile = {"sbg", "fermi_lat_agn", "swift_bat_agn", "2mass_hyperleda"} ;//Associated folder, either: sbg, fermi_lat_agn, swift_bat_agn, 2mass_hyperleda
vector< double > InputData_vBestThresholds = {38, 40, 41, 40};
vector< bool > InputData_vmag = {false, false, false, true};//false if the catalog provides fluxes, true if magnitudes
vector< bool > InputData_vdraw_faint_src = {true, true, false, false};//false if the catalog is too big, otherwise small dots al over the place

////////////////////////////////////////////////////////////////////////////
//                                                                        //
//                        Composition                                     //
//                                                                        //
////////////////////////////////////////////////////////////////////////////

vector< string > InputData_vscenarios = {"EPO1st","EPO2nd","Sib1st",""};
vector< string > InputData_vname_scenarios = {"EPOS-LHC 1st minimum","EPOS-LHC 2nd minimum","Sibyll 1st minimum","no attenuation"};

////////////////////////////////////////////////////////////////////////////
//                                                                        //
//                        Simulations                                     //
//                                                                        //
////////////////////////////////////////////////////////////////////////////

vector< string > InputData_vsim_files_SBG = {
									"sbg_lowres1_rdm_evts_iso_nsimu1000_nseed1.dat",
									"sbg_lowres1_rdm_evts_iso_nsimu1000_nseed2.dat",
									"sbg_lowres1_rdm_evts_iso_nsimu1000_nseed3.dat",
									"sbg_lowres1_rdm_evts_iso_nsimu1000_nseed4.dat",
									"sbg_lowres1_rdm_evts_iso_nsimu1000_nseed5.dat",
									"sbg_lowres1_rdm_evts_iso_nsimu1000_nseed6.dat",
									"sbg_lowres1_rdm_evts_iso_nsimu2000_nseed7.dat",
									"sbg_lowres1_rdm_evts_iso_nsimu2000_nseed8.dat",
									"sbg_lowres1_rdm_evts_iso_nsimu2000_nseed9.dat",
									"sbg_lowres1_rdm_evts_iso_nsimu2000_nseed10.dat",
									"sbg_lowres1_rdm_evts_iso_nsimu2000_nseed11.dat",
									"sbg_lowres1_rdm_evts_iso_nsimu2000_nseed12.dat",
									"sbg_lowres1_rdm_evts_iso_nsimu2000_nseed13.dat",
									"sbg_lowres1_rdm_evts_iso_nsimu2000_nseed14.dat",
									"sbg_lowres1_rdm_evts_iso_nsimu2000_nseed15.dat",
									"sbg_lowres1_rdm_evts_iso_nsimu2000_nseed16.dat",
									"sbg_lowres1_rdm_evts_iso_nsimu2000_nseed17.dat",
									"sbg_lowres1_rdm_evts_iso_nsimu2000_nseed18.dat"
									};
									
vector< string > InputData_vsim_files_gAGN = {
									"fermi_lat_agn_lowres1_rdm_evts_iso_nsimu1000_nseed1.dat",
									"fermi_lat_agn_lowres1_rdm_evts_iso_nsimu1000_nseed2.dat",	
									"fermi_lat_agn_lowres1_rdm_evts_iso_nsimu1000_nseed3.dat",
									"fermi_lat_agn_lowres1_rdm_evts_iso_nsimu1000_nseed4.dat",					
									"fermi_lat_agn_lowres1_rdm_evts_iso_nsimu1000_nseed5.dat",
									"fermi_lat_agn_lowres1_rdm_evts_iso_nsimu1000_nseed6.dat",
									"fermi_lat_agn_lowres1_rdm_evts_iso_nsimu2000_nseed7.dat",
									"fermi_lat_agn_lowres1_rdm_evts_iso_nsimu2000_nseed8.dat",
									"fermi_lat_agn_lowres1_rdm_evts_iso_nsimu2000_nseed9.dat",
									"fermi_lat_agn_lowres1_rdm_evts_iso_nsimu2000_nseed10.dat",
									"fermi_lat_agn_lowres1_rdm_evts_iso_nsimu2000_nseed11.dat",
									"fermi_lat_agn_lowres1_rdm_evts_iso_nsimu2000_nseed12.dat",
									"fermi_lat_agn_lowres1_rdm_evts_iso_nsimu2000_nseed13.dat",
									"fermi_lat_agn_lowres1_rdm_evts_iso_nsimu2000_nseed14.dat",
									"fermi_lat_agn_lowres1_rdm_evts_iso_nsimu2000_nseed15.dat",
									"fermi_lat_agn_lowres1_rdm_evts_iso_nsimu2000_nseed16.dat",
									"fermi_lat_agn_lowres1_rdm_evts_iso_nsimu2000_nseed17.dat",
									"fermi_lat_agn_lowres1_rdm_evts_iso_nsimu2000_nseed18.dat"
									};
									
vector< string > InputData_vsim_files_xAGN = {
									"swift_bat_agn_lowres1_rdm_evts_iso_nsimu1000_nseed1.dat",
									"swift_bat_agn_lowres1_rdm_evts_iso_nsimu1000_nseed2.dat",	
									"swift_bat_agn_lowres1_rdm_evts_iso_nsimu1000_nseed3.dat",
									"swift_bat_agn_lowres1_rdm_evts_iso_nsimu1000_nseed4.dat",
									"swift_bat_agn_lowres1_rdm_evts_iso_nsimu1000_nseed5.dat",
									"swift_bat_agn_lowres1_rdm_evts_iso_nsimu1000_nseed6.dat",
									"swift_bat_agn_lowres1_rdm_evts_iso_nsimu2000_nseed7.dat",
									"swift_bat_agn_lowres1_rdm_evts_iso_nsimu2000_nseed8.dat",
									"swift_bat_agn_lowres1_rdm_evts_iso_nsimu2000_nseed9.dat",
									"swift_bat_agn_lowres1_rdm_evts_iso_nsimu2000_nseed10.dat",
									"swift_bat_agn_lowres1_rdm_evts_iso_nsimu2000_nseed11.dat",
									"swift_bat_agn_lowres1_rdm_evts_iso_nsimu2000_nseed12.dat",
									"swift_bat_agn_lowres1_rdm_evts_iso_nsimu2000_nseed13.dat",
									"swift_bat_agn_lowres1_rdm_evts_iso_nsimu2000_nseed14.dat",
									"swift_bat_agn_lowres1_rdm_evts_iso_nsimu2000_nseed15.dat",
									"swift_bat_agn_lowres1_rdm_evts_iso_nsimu2000_nseed16.dat",
									"swift_bat_agn_lowres1_rdm_evts_iso_nsimu2000_nseed17.dat",
									"swift_bat_agn_lowres1_rdm_evts_iso_nsimu2000_nseed18.dat"
									};	
									
vector< string > InputData_vsim_files_NIRgal = {
									"2mass_hyperleda_lowres1_rdm_evts_iso_nsimu1000_nseed1.dat",
									"2mass_hyperleda_lowres1_rdm_evts_iso_nsimu1000_nseed2.dat",
									"2mass_hyperleda_lowres1_rdm_evts_iso_nsimu1000_nseed3.dat",
									"2mass_hyperleda_lowres1_rdm_evts_iso_nsimu1000_nseed4.dat",
									"2mass_hyperleda_lowres1_rdm_evts_iso_nsimu1000_nseed5.dat",
									"2mass_hyperleda_lowres1_rdm_evts_iso_nsimu1000_nseed6.dat",
									"2mass_hyperleda_lowres1_rdm_evts_iso_nsimu2000_nseed7.dat",
									"2mass_hyperleda_lowres1_rdm_evts_iso_nsimu2000_nseed8.dat",
									"2mass_hyperleda_lowres1_rdm_evts_iso_nsimu2000_nseed9.dat",
									"2mass_hyperleda_lowres1_rdm_evts_iso_nsimu2000_nseed10.dat",
									"2mass_hyperleda_lowres1_rdm_evts_iso_nsimu2000_nseed11.dat",
									"2mass_hyperleda_lowres1_rdm_evts_iso_nsimu2000_nseed12.dat",
									"2mass_hyperleda_lowres1_rdm_evts_iso_nsimu2000_nseed13.dat",
									"2mass_hyperleda_lowres1_rdm_evts_iso_nsimu2000_nseed14.dat",
									"2mass_hyperleda_lowres1_rdm_evts_iso_nsimu2000_nseed15.dat",
									"2mass_hyperleda_lowres1_rdm_evts_iso_nsimu2000_nseed16.dat",
									"2mass_hyperleda_lowres1_rdm_evts_iso_nsimu2000_nseed17.dat",
									"2mass_hyperleda_lowres1_rdm_evts_iso_nsimu2000_nseed18.dat"
									};								
									
