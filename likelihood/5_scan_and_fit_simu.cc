///////////////////////////////////////////////////////
//                                                   //
//   Dev. by Jonathan Biteau (biteau@in2p3.fr)       //
//                       last edit: 2020-11-03       //
///////////////////////////////////////////////////////

// C/C++ classics
#include <iostream>
#include <fstream>
#include <sstream>
#include <vector>
#include <tuple>
#include <chrono>
#include <algorithm> 

// Toolkit
#include "healpixmap.h"

// ROOT
#include "TGraph.h"
#include "TMath.h"
#include "TRandom3.h"

//DATA INPUT
#include "InputData.h"

// UTILITIES CATALOG ANALYSIS
#include "utilities_LikelihoodCatalogs.h"

#ifdef gcc323
char* operator+( streampos&, char* );
#endif

using namespace std;

//Progress bar
static TProgressBar gProgB;

void Usage(string sinput)
{
	cout << endl;
	cout << " Synopsis : " << endl;
	cout << sinput << " -h, --help to obtain this message" << endl;
	cout << sinput << " <# of simulations (advice: <=1E4)> <nseed>" << endl << endl;

	cout << " Description :" << endl;
	cout << " Simulate isotropic events and fits one of the models"<<endl
			 << " -- that's all folks! -- "
			 << endl<< endl;

	exit(0);
}

//Update the model maps (time consuming process)
void UpdateModel(vector< THealpixMap > &vsmoothedMap, int nSide, double Eth, string ssrc_file_ext, bool low_resolution){
	vsmoothedMap.clear();
	
	//Composition model
	stringstream ssfile_compo;
	ssfile_compo<<ssrc_file_ext<<"_threshold"<<int(Eth);	

	//Load the model
	vector< string > vSrcName;
	vector< double > vl_model, vb_model, vw_model;
	LoadModel(ssfile_compo.str(),vSrcName, vl_model, vb_model, vw_model);
			
	vsmoothedMap = LoadSmoothedModelMaps(nSide, vl_model, vb_model, vw_model, low_resolution);
}

//Return max TS for a scan over vthreshold
void LoadTSmax(double &TSmax, double &TS0, string ssrc_file_ext, vector< THealpixMap > vexposureMap, vector< double > vthreshold, vector < double > vE, vector < double > vZen, vector < double > vl, vector < double > vb, bool low_resolution, bool verbose=false, int stepE = 10){

	int nSide = vexposureMap.front().NSide();
	
	if(verbose){
		cout<<"Computing TS in range: "<<vthreshold.front()<<" "<<vthreshold.back()<<" EeV"<<endl;
		gProgB.Zero();
		gProgB.fBegin = 0;
		gProgB.fEnd = vthreshold.size();
		gProgB.InitPercent();
	}
	
	vector< THealpixMap > vsmoothedMap;
	vector< double > vTS_data;
	for(unsigned int k=0; k<vthreshold.size(); k++){
		double Eth = vthreshold[k];
	
		//Trim the data below threshold
		Trim(Eth,vE, vZen, vl,vb);

		//Update the model every 1 EeV or every stepE EeV if low_resolution
		if((not low_resolution) or (k % stepE ==0)) UpdateModel(vsmoothedMap, nSide, Eth, ssrc_file_ext, low_resolution);

		//Fit the data
		double sig_theta_bf, alpha_bf, logL_0;
		vTS_data.push_back(TS_fixed_E(vl,vb, vexposureMap[k], vsmoothedMap, sig_theta_bf, alpha_bf, logL_0));

		//Update progress bar
		if(verbose) gProgB.PrintPercent(k);
	}
	
	TSmax = *max_element(vTS_data.begin(),vTS_data.end());
	TS0 = vTS_data[0];
	if(verbose){
		gProgB.EndPercent();
		cout<<"Max TS = "<<TSmax<<endl;
	}
}

//Remove and count last elements - ASSUMES events are energy ordered!!!!!
unsigned int TrimAbove(vector< double >& vE, double Eth){
	unsigned int count = 0;
	while(vE.back()>=Eth){
		if(vE.back()<vE[vE.size()-2]) cout<<"EVENTS ARE NOT ORDERED IN ENERGY!!!! WRONG SIM"<< endl;
		vE.pop_back();
		count++;
	}
	return count;
}

//Simulate isotropic data	
void SimulateIsotropicData(vector< TRandom3 > &vr0,vector< double >& vEsim, vector< double >& vlsim, vector< double >& vbsim, vector< double > vE, vector< THealpixMap > vexposureMap, vector< double > vthreshold){
		
	//Cleaning
	vEsim.clear();
	vlsim.clear();
	vbsim.clear();
		
	//Random variables for generation on the sphere
	int nmaxsimu=1E6;
	TRandom3 ru(int(vr0[0].Uniform(nmaxsimu))), rv(int(vr0[1].Uniform(nmaxsimu)));//for long, lat, picking
	TRandom3 rp(int(vr0[2].Uniform(nmaxsimu)));//for rejection/acceptance

	//Store data in a tuple (E, l, b)
	vector< tuple<double, double, double> > vdata;

	//Load the simulated evts: same number of events above decreasing Eth
	for(int k=vthreshold.size()-1; k>=0; k--){
		unsigned int nabove = TrimAbove(vE, vthreshold[k]);
		unsigned int count = 0;
		while(count<nabove){
			//Uniform picking on the sphere
			double phi = 2*M_PI*ru.Uniform();// in [0;2pi]
			double theta = TMath::ACos(2.*rv.Uniform()-1.);// in [0;pi]
			double l = (phi-M_PI)*RTOD;// in [-180;180]
			double b = (theta-0.5*M_PI)*RTOD;// in [-90;90]

			//Acceptance/rejection criterion
			long pix = vexposureMap[k].Ip(l,b);
			if(rp.Uniform()<=vexposureMap[k][pix]){
				count++;
				vdata.push_back(make_tuple(vthreshold[k], l, b));
			}
		}
	}
	
	//Sort the simulated events by increasing energy
	sort(vdata.begin(), vdata.end());	
	for(unsigned int i=0; i<vdata.size(); i++){
		vEsim.push_back(get<0>(vdata[i]));
		vlsim.push_back(get<1>(vdata[i]));
		vbsim.push_back(get<2>(vdata[i])); 	
	}
}

//Utilities	
double vector_mean(vector<double> v){
	return accumulate( v.begin(), v.end(), 0.0) / v.size();
}

double average_gradient(vector<double> v){
	vector< double > vdiff;
	for(unsigned int i=0; i< v.size()-1; i++) vdiff.push_back(v[i+1]-v[i]);
	return vector_mean(vdiff);
}


//Actual place where the work is done for a given catalog
void LoadResultsScan(int cat = 0, int compo_model=0, int nsimu=1E1, int nseed=123, bool low_resolution = true){

	////////////////////////////////////////////////////////////////////////////
	//                                                                        //
	//     	                 FIT THE DATA AT ALL Eth                          //
	//                                                                        //
	////////////////////////////////////////////////////////////////////////////
	
	//Events///////////////////////////////////////////////////////////////////	
	vector< double > vId, vZen, vl, vb, vE;
	LoadAugerData(InputData_seventfile , vId, vZen, vl, vb, vE);

	//Exposure/////////////////////////////////////////////////////////////////
	THealpixMap exposureMap_vert((char*)InputData_sexpofitsfile_vert.c_str());
	exposureMap_vert.SetCoordSys('G');
	unsigned int nSide = exposureMap_vert.NSide();

	THealpixMap exposureMap_hor((char*)InputData_sexpofitsfile_hor.c_str());
	exposureMap_hor.SetCoordSys('G');
	if(exposureMap_hor.NSide()!=nSide) cout<<"WRONG EXPOSURE MAPS!!!!!"<<endl;
	
	//Rescale exposures according to nominal ones
	//Note: only the relative normalization matters
	exposureMap_vert *= IntputData_exposure_vert/exposureMap_vert.Total();
	exposureMap_hor *= IntputData_exposure_hor/exposureMap_hor.Total();
	
	//Unfolding factors////////////////////////////////////////////////////////
	vector < double > vthreshold, vthreshold_tmp, vunfolding_vert, vunfolding_hor;
	LoadUnfolding(InputData_sfile_vert_unfolding, vthreshold, vunfolding_vert);
	LoadUnfolding(InputData_sfile_hor_unfolding, vthreshold_tmp, vunfolding_hor);
	
	//Checks that the energy thresholds are the same
	if(vthreshold!=vthreshold_tmp) cout<<"WRONG UNFOLDING FACTORS!!!!!"<<endl;

	//Interpolate unfolding factors
	TGraph *G_vert = new TGraph(vthreshold.size(),&vthreshold[0],&vunfolding_vert[0]);
	TGraph *G_hor = new TGraph(vthreshold.size(),&vthreshold[0],&vunfolding_hor[0]);
	
	//Compute the list of exposure maps normalized to their maximum for acceptance rejection
	vector< THealpixMap > vexposureMap;
	for(unsigned int k=0; k<vthreshold.size(); k++){
		double Eth = vthreshold[k];
		THealpixMap expMap = exposureMap_vert/G_vert->Eval(Eth) + exposureMap_hor/G_hor->Eval(Eth);
		expMap=expMap/expMap.Max();
		vexposureMap.push_back(expMap);
	}
	delete G_vert;
	delete G_hor;

	//Result for the data//////////////////////////////////////////////////////
	stringstream ssrc_file_ext;
	ssrc_file_ext<<"CompoModel/"<<InputData_vsfile[cat]<<"/"<<InputData_vsfile[cat];
	if(InputData_vscenarios[compo_model].length()>1) ssrc_file_ext<<"_"<<InputData_vscenarios[compo_model];

	bool verbose=true;
	double TS32, TS_data;
	LoadTSmax(TS_data, TS32,  ssrc_file_ext.str(), vexposureMap, vthreshold, vE, vZen, vl,vb, low_resolution, verbose);

	//Write results////////////////////////////////////////////////////////////	
	std::stringstream ssfile;
	ssfile<<"Simu/"<<InputData_vsfile[cat]<<"_lowres"<<low_resolution<<"_rdm_evts_iso_nsimu"<<nsimu<<"_nseed"<<nseed<<".dat";
	std::ofstream myfile;
	myfile.open (ssfile.str().c_str());
	myfile<<"//TS_data"<<"\n";
	myfile <<TS_data<<"\n";
	myfile<<"//TS_simu(max)"<<"\n";


	////////////////////////////////////////////////////////////////////////////
	//                                                                        //
	//     	                 FIT THE SIMS AT ALL Eth                          //
	//                                                                        //
	////////////////////////////////////////////////////////////////////////////

	// Progress bar
	cout<<"########### Starting "<<nsimu<<" simulations ###########"<<endl;
	gProgB.Zero();
	gProgB.fBegin = 0;
	gProgB.fEnd = nsimu;
	gProgB.InitPercent();
	
	//Loop on threshold energies
	vector< TRandom3 > vr0;
	for(unsigned int i=0; i<3; i++){
		TRandom3 r0(nseed+i);
		vr0.push_back(r0);
	}


	auto startSimu = chrono::high_resolution_clock::now();
	double TS_sim;
	for(int isim=0; isim<nsimu; isim++){
		vector< double > vEsim, vlsim, vbsim;
		SimulateIsotropicData(vr0, vEsim, vlsim, vbsim, vE, vexposureMap, vthreshold);
		
		LoadTSmax(TS_sim, TS32, ssrc_file_ext.str(), vexposureMap, vthreshold, vEsim, vZen, vlsim,vbsim, low_resolution);
		
		//write simu output if TS32 > minimum value (minimizer stuck for some sims)
		double TS_threshold=1.5;
		if(TS32>TS_threshold) myfile <<TS32<<" "<<TS_sim<<"\n";

		gProgB.PrintPercent(isim);
	}
	myfile.close();
	gProgB.EndPercent();

	auto finishSimu = chrono::high_resolution_clock::now();
	chrono::duration<double> eSimu = finishSimu - startSimu;
	cout << "Total time per entry: " << eSimu.count()/nsimu << " s"<<endl;
}



/// Main //////////////////////////////////////////////////////////////////////////////////////////////
int main(int argc, char* argv[])
{
	////////////////////////////////////////////////////////////////////////////
	//                                                                        //
	//                            Initialization                              //
	//                                                                        //
	////////////////////////////////////////////////////////////////////////////

	// Command line
	if(argc != 3) Usage(argv[0]);
	int nsimu = int(atof(argv[1]));
	int nseed = int(atof(argv[2]));

	//Runs the scans
	vector< double > vmax;
	for(unsigned int cat=0; cat<InputData_vssources.size(); cat++){
		cout<<"########### "<<InputData_vscenarios[0]<<" -  "<<InputData_vssources[cat]<<" ###########"<<endl;
		LoadResultsScan(cat, 0., nsimu, nseed);
	}

	////////////////////////////////////////////////////////////////////////////
	//                                                                        //
	//                         End of the code                                //
	//                                                                        //
	////////////////////////////////////////////////////////////////////////////

	cout << endl <<"------ Program Finished Normally: Good Job! ------" << endl;
}
