#include "utilities_Structures.h"

int main(int argc, char* argv[])
{
  // spot the plots
  // int fargc = 1;
  // TRint rint("Auger analysis", &fargc, argv);
    
  if (argc!=3) {
    cout << " Synopsys :" << argv[0]
         << " <Type of analysis: ca, gc, gp, sgp, ac> "
         << " <number of isotropic skies>"
      << endl;
    return 0;
  }
    
  // arguments
  string type = argv[1];    // choose the analysis, possible options: ca, gc, gp, sgp, ac
  int nsky = atoi(argv[2]); // number of isotropic skies
  if (!checkAnalysis(type))
    return 0;
  print(type, nsky);
  
  // Set angular scale bin
  double xbins[nbin+1];
  GetAngScaleBinning(xbins);

  //-------------------------------------------------------------------------------------//
  // get number of observed events/pairs as a function of energy and angular scale       //
  //-------------------------------------------------------------------------------------//
  TH2F hData = GetTH2data(type);
  TCanvas c1("c1","c1",900,600);
  SetTCanvas(c1);
  if (!type.compare("ac"))
    SetTh2(type, hData, "Observed number of pairs");
  else
    SetTh2(type, hData, "Observed number of events");
  hData.Draw("zcol");

  //-------------------------------------------------------------------------------------//
  // expected number of events as a function of the energy and angular scale             //
  //-------------------------------------------------------------------------------------//
  TH2F hMean("hMean","hMean",nbin,xbins,nEbin,minEn,maxEn);

  TCanvas c2("c2","c2",900,600);
  SetTCanvas(c2);
  if (!type.compare("ac"))
    SetTh2(type, hMean, "Expected number of pairs");
  else
    SetTh2(type, hMean, "Expected number of events");
  hMean.Draw("zcol");

  //-------------------------------------------------------------------------------------//
  // get local p-value                                                                   //
  //-------------------------------------------------------------------------------------//
  // p-value (estimated with simulations)
  TH2F hProb("hProb","hProb",nbin,xbins,nEbin,minEn,maxEn);
  // p-value with binomial probability (only for CenA)
  TH2F hP("hP","hP",nbin,xbins,nEbin,minEn,maxEn);
  
  int nhistoTot = nEbin*nbin;
  TH1F *histSim[nhistoTot];

  if (!type.compare("ac"))
    evIso2pt(type, nsky, hMean, hProb, hData, histSim);
  else
    evIso(type, nsky, hMean, hProb, hData, histSim);

  TCanvas c3("c3","c3",900,600);
  SetTCanvas(c3);
  c3.SetLogz();
    
  // results for not CenA analyses
  if (type.compare("ca")) {
    SetTh2(type, hProb, "#it{p}-value");
    //hProb.Draw("zcol");
  }
  // results for CenA analysis
  else {
    BinProb(hData, hMean, hP);
    //hP.SetMinimum(1e-7);
    SetTh2(type, hP, "#it{p}-value");
    //hP.Draw("zcol");
  }

  //-------------------------------------------------------------------------------------//
  // save results in a root file                                                         //
  //-------------------------------------------------------------------------------------//
  string nameFile = SetNameFile(type);
  TFile OutputFile (nameFile.c_str(),"recreate");
  OutputFile.cd();
  hData.Write();
  hMean.Write();
  for (int h=0; h<nhistoTot; ++h)
    histSim[h]->Write();
  if (type.compare("ca"))
    hProb.Write();
  else
    hP.Write();
  
  cout << " ho passato momenti peggiori.....forse! :| " << "\n";
  
  // rint.Run(kTRUE);
  
  return 0;
}
