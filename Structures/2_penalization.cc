#include "utilities_Structures.h"

int main(int argc, char* argv[])
{
  // spot the plots
  // int fargc = 1;
  // TRint rint("Auger analysis", &fargc, argv);
    
  if (argc!=3) {
    cout << " Synopsys :" << argv[0]
         << " <Type of analysis: ca, gc, gp, sgp, ac> "
         << " <number of isotropic skies>"
    << endl;
    return 0;
  }
    
  //-----------------------------//
  //------ arguments ------------//
  //-----------------------------//
  string type = argv[1];    // choose the analysis, possible options: gc, gp, sgl, cc, ca
  int nsky = atoi(argv[2]); // number of isotropic skies
  if (!checkAnalysis(type))
    return 0;
  print(type, nsky);

  if (!type.compare("ac"))
    penalization2pt(type, nsky);
  else
    penalization(type, nsky);

  cout << " ho passato momenti peggiori.....forse! :| " << "\n";
  
  // rint.Run(kTRUE);
  
  return 0;

}
