#include <stdlib.h>
#include <stdio.h>
#include <iostream>
#include <fstream>
#include <cstring>
#include <vector>
#include <sstream>
#include <iomanip>
#include <algorithm>
#include <limits>

#include <cmath>
#include <string>
#include <math.h>  // fmod
#include <sstream>
#include <time.h>

//---- ROOT -----//
#include "Riostream.h"
#include "TRandom.h"
#include "TH2F.h"
#include "TFile.h"
#include "TMath.h"
#include "TH1D.h"
#include "TPad.h"
#include "TROOT.h"
#include "TRandom.h"
#include "TRandom3.h"
#include "TF1.h"
#include "TCanvas.h"
#include "TRint.h"
#include "TStyle.h"
#include "TMarker.h"

#include <sys/time.h>

#include "STClibrary.h"

using namespace std;

//-----------------------------------//
// -------- constants ---------------//
//-----------------------------------//
const double d2r = TMath::Pi() / 180.;
const double r2d = 180 / TMath::Pi();

// Number of events
const int nEvents = 2625;

// Binning in angular scale
// dTheta = 0.25° (for theta < 5°) | dTheta = 1.° (for theta > 5°)
// (20+25) number of bins (20) up to 5° plus numner of bins (25) up to 30°
const int nbin = 45;
const double dx1 = 0.25;
const double dx2 = 1.;
const int nEbin = 49;      // number of bins in energy, from 32 to 80 EeV in step of 1 EeV
const double minEn = 32.;  // minimum energy
const double maxEn = 81.;  // maximum energy (in the plot)

//! GetSeed
void GetSeed(unsigned int &seed);

//! Get angular distance btw two points on a sphere
double GetAngDist(double alphaEv, double deltaEv, double alphaObj, double deltaObj);

//! print on the screen some info about the analysis 
void print(string type, int nsky);

// set file for storing local p-value results
string SetNameFile(string type);

//! Set angular scale bin
void GetAngScaleBinning(double *xbins);

int index(double angBin, double enBin);

//! Set the object coordinates
void SetObjectCoords(string type, double &alphaObj, double &deltaObj);

// get the events
void GetEvents(string type, double *alphaEv, double *deltaEv, double *enEv);

// integral (E >= Eth , angular scale <=\Phi)
TH2F GetNormIntegral(TH2F h2, double nsky);

// integral (E >= Eth , angular scale <=\Phi)
TH2F GetNormIntegral2pt(TH2F h2, double nsky);

// Get number of events in each energy bin
void GetNEbin(double *enEv, double *nOut);

// Get number of events for each threshold
void GetNETh(double *enEv, double *nOut);

// observed number of events
TH2F GetTH2data(string type);

// calculate expected number of events from the isotropy
void evIso(string type, double nsky, TH2F& hMean, TH2F& hProb, TH2F hdata, TH1F *histSim[]);

// calculate expected number of events from the isotropy
void evIso2pt(string type, double nsky, TH2F& hMean, TH2F& hProb, TH2F hdata, TH1F *histSim[]);

// calculate binomial probability for Centaurus A
void BinProb(TH2F &hData, TH2F &hSim, TH2F &h2);

//! plotting style for TCanvas and TH2F map
void SetTh2(string type, TH2F &h2 , string titleZ);

//! set TCanvas style
void SetTCanvas(TCanvas &c);

// ! Get minimum p-value
void GetMinimum(TH2F &hData, TH2F &hMean, TH2F &hProb);

// ! function for penalization
double penalization(string type, double nsky);
double penalization2pt(string type, double nsky);

// check the allowed analyses
bool checkAnalysis(string type);

