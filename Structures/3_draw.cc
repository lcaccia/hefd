#include "utilities_Structures.h"

#include "TGraph.h"
#include "TMultiGraph.h"

void SetMinimum(string type, TH2F &h2 )
{
  string file;
  if (!type.compare("ac"))
    h2.SetMinimum(1e-7);
  if (!type.compare("gc"))
    h2.SetMinimum(1e-2);
  else if (!type.compare("gp"))
    h2.SetMinimum(1e-2);
  else if (!type.compare("sgp"))
    h2.SetMinimum(1e-3);
  else if (!type.compare("ca"))
    h2.SetMinimum(1e-7);
}


// convert p-value in sigma
double sigma(double pvalue)
{
  double p = 1 - pvalue;
  double argument = (2*p) - 1;
  double significance = sqrt(2)*TMath::ErfInverse( argument );
  return significance;
}

// sigma from pvalue
double pvalue(double significance)
{
  //calculate 2p -1
  double tmp1 = TMath::Erf ( ( 1./sqrt(2) )*significance);
  double pvalue = (1 - tmp1)*0.5;
  return pvalue;
}


int main(int argc, char* argv[])
{
  // spot the plots
  int fargc = 1;
  TRint* rint = new TRint("Auger analysis", &fargc, argv);

  if (argc!=2) {
    cout << " Synopsys :" << argv[0]
         << " <Type of analysis: ca, gc, gp, sgp, ac> "
    << endl;
    return 0;
  }
    
  //-----------------------------//
  //------ arguments ------------//
  //-----------------------------//
  string type = argv[1];    // choose the analysis, possible options: gc, gp, sgl, cc, ca
  if (!checkAnalysis(type))
    return 0;

  // ------------------------------//
  // take the info
  string file = SetNameFile(type);
  cout << "\n";
  cout << " file " << file << endl;

  // take the values
  TFile *f = new TFile(file.c_str());
  file.clear();
  TH2F *hData = (TH2F*)f->Get("hData");
  TH2F *hMean = (TH2F*)f->Get("hMean");
  TH2F *hP;
  if (type.compare("ca"))
    hP = (TH2F*)f->Get("hProb");
  else
    hP = (TH2F*)f->Get("hP");
  GetMinimum(*hData, *hMean, *hP);

  // local p-value
  double min = hP->GetMinimum();
  cout << " Local p-value " << min << endl;

  TCanvas c3("c3","c3",900,600);
  SetTCanvas(c3);
  c3.SetLogz();

  SetMinimum(type, *hP);
  SetTh2(type, *hP, "#it{p}-value");
  //hP->Draw("zcol");

  //
  ifstream myfile;
  string title = "Files/file_"+type;
  myfile.open(title.c_str());

  double sigmatrue = 0.;
  double total = 0.;
  double tmp = 0.;
  vector<double> value;

  while(!myfile.eof()){
    myfile >> tmp;
    if (!myfile.good())
      break;
    ++total;
    value.push_back(tmp);
    if (tmp<=min) {
      ++sigmatrue;
      //cout << " tmp " << tmp << endl;
    }
  }
  myfile.close();

  double global = sigmatrue * 1./total;
  double sigma_ = sigma(global);
  cout << " Global p-value " << global <<  " significance " << sigma_ << endl;
  /*
  //--------------------//
  //    make tgraph     //
  //--------------------//
  int number = value.size();
  const int n = 160;
  double x[n], y[n];

  for (int i = 0; i< n; ++i){

    //double non_sigma = 1.5 + (i*0.1);
    //double non_p = pvalue(non_sigma);
    double non_p =  pow(10, -((i+1) * 0.1) );
    double p = 0;
    for (int j=0; j<number; ++j){
      if(value.at(j)<=non_p) p++;
    }
    p/=(double)number;
    double sig_true = sigma(p);

    x[i]=non_p;

    if(sig_true>0) y[i]=p;
    else y[i]=-1;
  }

  TCanvas c1("c1","c1",900,600);
  SetTCanvas(c1);
  c1.SetLogy();
  c1.SetLogx();
  c1.SetGrid();

  TGraph g(n, x, y);
  g.SetMarkerStyle(20);
  g.SetMarkerSize(1);
  g.SetMarkerColor(kRed);
  g.SetTitle("");
  g.GetXaxis()->SetTitle("Local #it{p}-value");
  g.GetYaxis()->SetTitle("Global #it{p}-value");
  g.GetXaxis()->SetLimits(1e-9,1e-0);
  g.SetMaximum(0.5);
  g.SetMinimum(0);
  g.GetYaxis()->SetTitleSize(0.055);
  g.GetXaxis()->SetTitleSize(0.055);
  g.GetYaxis()->SetTitleOffset(1.4);
  g.GetXaxis()->SetTitleOffset(1.4);
  g.GetYaxis()->SetTitleFont(62);
  g.GetXaxis()->SetTitleFont(62);
  g.GetYaxis()->SetLabelFont(62);
  g.GetXaxis()->SetLabelFont(62);
  g.GetXaxis()->SetNdivisions(110);
  g.GetYaxis()->SetNdivisions(110);
  g.GetYaxis()->CenterTitle();
  g.GetXaxis()->CenterTitle();
  g.GetYaxis()->SetLabelOffset(0.02);
  g.GetYaxis()->SetLabelSize(0.04);
  g.GetXaxis()->SetLabelOffset(0.02);
  g.GetXaxis()->SetLabelSize(0.04);
  g.Draw("apl");

  cout << " ho passato momenti peggiori.....forse! :| " << "\n";
  */
  rint->Run(kTRUE);
  
  return 0;

}
