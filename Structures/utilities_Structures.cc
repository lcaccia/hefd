#include "utilities_Structures.h"

void GetSeed(unsigned int &seed)
{
  //----- seed changed --------//
  gRandom->SetSeed(0);
  struct timeval myTimeVal;
  struct timezone myTimeZone;
  gettimeofday(&myTimeVal, &myTimeZone);
  seed = (unsigned int) (myTimeVal.tv_usec+(myTimeVal.tv_sec % 1000)*1000000);
}

double GetAngDist(double alphaEv, double deltaEv, double alphaObj, double deltaObj)
{
  double deltaEvR = deltaEv*d2r;
  double deltaObjR = deltaObj*d2r;
  double diffAlpha = alphaEv - alphaObj;
  double da = sin (deltaEvR) * sin (deltaObjR );
  double dg = cos (deltaEvR) * cos (deltaObjR) * cos( (diffAlpha) * d2r );
  double argAcos = da + dg;
  return acos (argAcos) * r2d;
}

void print(string type, int nsky)
{
  cout << "\n";
  cout << " ------------------------------------------ \n";
  if (!type.compare("ac"))
    cout << " Auto-correlation analysis " << "\n";
  if (!type.compare("gc"))
    cout << " Analysis aroung the Galactic Center " << "\n";
  else if (!type.compare("gp"))
    cout << " Analysis aroung the Galactic Plane " << "\n";
  else if (!type.compare("sgp"))
    cout << " Analysis aroung the Super Galactic Plane " << "\n";
  else if (!type.compare("ca"))
    cout << " Analysis aroung Centaurus A " << "\n";
  cout << " Number of simulated isotropic skies " << nsky << endl;
  cout << " ------------------------------------------ \n";
  cout << "\n";
}

string SetNameFile(string type)
{
  string file;
  if (!type.compare("ac"))
    file = "Results/AutoCorr.root";
  if (!type.compare("gc"))
    file = "Results/GalCenter.root";
  else if (!type.compare("gp"))
    file = "Results/GalPlane.root";
  else if (!type.compare("sgp"))
    file = "Results/SGalPlane.root";
  else if (!type.compare("ca"))
    file = "Results/CenA.root";
  return file;
}


void GetAngScaleBinning(double *xbins)
{
  // make the binning used in ApJ2015
  // D\Phi = 0.25° (for theta < 5°) | DE = 1.° (for theta > 5°)
  // number of bins = 20 + 25;
  // 20 bins up to 5° // 25 bins from 5° to 30°
  for (int i=0; i<=nbin; i++) {
    if (i<20)
      xbins[i] = i * dx1;
    else
      xbins[i] = 5 + (i-20) * dx2;
  }
}

int index(double angBin, double enBin)
{
  int index =
    ( (floor(enBin) - 1) * nbin ) + floor(angBin) -1;
  return index;
}

void SetObjectCoords(string type, double &alphaObj, double &deltaObj)
{
  if (!type.compare("ca")) { // CentaurusA equatorial coordinates
    alphaObj = 201.4;
    deltaObj = -43.0;
  }
  else { // Gal Center // GalPlan // SGal Plane
    alphaObj = 0.;
    deltaObj = 0.;
  }
}

TH2F GetNormIntegral(TH2F h2, double nsky)
{
  TH2F hFinal(h2);
  for (int i=1; i<=nEbin; ++i) { // energy loop
    for (int j=1; j<=nbin; ++j) { // angular scale loop
      double content = h2.Integral(1,j,i,nEbin);
      hFinal.SetBinContent(j, i, content / double (nsky) );
    } // end angular scale loop
  } // end energy loop
  return hFinal;
}

TH2F GetNormIntegral2pt(TH2F h2, double nsky)
{
  TH2F hFinal(h2);
  // integrate the histo
  for (int i=1; i<=nEbin; ++i) { // energy loop
    for (int j=1; j<=nbin; ++j) { // angular scale loop
      double content = h2.Integral(1,j,i,i);
      h2.SetBinContent(j, i, content * 1. / (double) nsky );
      cout << " En bin " << i << " ang bin " << j << " content " << content * 1. / (double) nsky << endl;
    } // end angular scale loop
  } // end energy loop
  return hFinal;
}

void SetTCanvas(TCanvas &c)
{
  c.cd();
  c.SetBottomMargin(0.15);
  c.SetLeftMargin(0.15);
  c.SetRightMargin(0.18);
}

void SetTh2(string type, TH2F &h2 , string titleZ)
{
  // X axis
  h2.SetLabelFont(62,"X");
  h2.SetLabelOffset(0.02,"X");
  h2.SetLabelSize(0.05, "X");
  h2.SetTitleFont(62,"X");
  h2.SetTitleOffset(1.4, "X");
  h2.SetTitleSize(0.05,"X");
  h2.GetXaxis()->SetNdivisions(106);
  h2.SetXTitle("#scale[1.5]{#psi} [deg]");
  h2.GetXaxis()->CenterTitle();
    
  // Y axis
  h2.SetLabelFont(62,"Y");
  h2.SetLabelOffset(0.02,"Y");
  h2.SetLabelSize(0.05, "Y");
  h2.SetTitleFont(62,"Y");
  h2.SetTitleOffset(1.25, "Y");
  h2.SetTitleSize(0.05,"Y");
  h2.GetYaxis()->SetNdivisions(308);
  h2.SetYTitle("E_{#scale[1.5]{th}} [EeV]");
  h2.GetYaxis()->CenterTitle();
    
  //Z axis
  h2.SetLabelFont(60,"z");
  h2.SetLabelOffset(0.005,"Z");
  h2.SetLabelSize(0.05, "Z");
  h2.SetTitleFont(62,"Z");
  h2.SetTitleOffset(1.25, "Z");
  h2.SetTitleSize(0.05,"Z");
  h2.SetZTitle(titleZ.c_str());
  h2.GetZaxis()->CenterTitle();
  h2.GetZaxis()->RotateTitle();
   
  // color palette
  gStyle->SetOptStat(0);
  const int NCont = 255;
  gStyle->SetNumberContours(NCont);
  gStyle->SetPalette(53);
  
  // title
  h2.SetTitleFont(82,"T");
  h2.SetTitleSize(0.1,"T");
    
  // set title map
  string titleMap;
  if (!type.compare("gc"))
    titleMap = "Galactic Center";
  else if (!type.compare("gp"))
    titleMap = "Galactic Plane";
  else if (!type.compare("sgp"))
    titleMap = "SuperGalactic Plane";
  else if (!type.compare("ca"))
    titleMap = "Centaurus A";
  else if (!type.compare("ac"))
    titleMap = "Auto-correlation";
  h2.SetTitle(titleMap.c_str());
    
  // find the minimum
  int nbinx = 0;
  int nbiny = 0;
  int nbinz = 0;
  h2.GetMinimumBin(nbinx, nbiny, nbinz);
  double x = h2.GetXaxis()->GetBinCenter(nbinx);
  double y = h2.GetYaxis()->GetBinCenter(nbiny);
  
  // plot histo with minimum position (white cross)
  h2.Draw("zcol");
  TMarker *m = new TMarker(x,y,34); // 34 full cross
  m->SetMarkerColor(0); //0 white
  m->SetMarkerSize(1.9);
  m->Draw();
  //delete m;
}

// Get number of events in each differential energy bin
void GetNEbin(double *enEv, double *nOut)
{
  for (int i=0; i<nEbin; ++i) {
    nOut[i] = 0;
    for ( int j =0; j<nEvents; ++j) {
      if (i!=(nEbin-1)) { //exclude the last bin above 80 EeV
        if (enEv[j]>= (minEn+i) && enEv[j]< (minEn+1+i) )
          ++nOut[i];
      }
      else {
        if (enEv[j]>= (minEn+i))
        ++nOut[i];
      }
    }
  }
}

// Get number of events for each threshold
void GetNETh(double *enEv, double *nOut)
{
  for (int i=0; i<nEbin; ++i) { // energy loop
    nOut[i] = 0;
    double eth = i + minEn;
    for (int z=0; z<nEvents; ++z) { // event loop
      if (enEv[z] >= eth)
        ++nOut[i];
    } // end event loop
  } // end energy loop
}

void GetEvents(string type, double *alphaEv, double *deltaEv, double *enEv)
{
  // 5 columns in the event file
  string ID;
  double theta = 0.;
  double en = 0.;
  double RA = 0.;
  double dec = 0.;
    
  //event file
  string pathFile= "../Data/HEFD_v2r0_Id_Th_RA_Dec_E";;
  ifstream in;
  in.open(pathFile .c_str());
    
  int count = 0;
  while (!in.eof()) {
    in >> ID >> theta >> RA >> dec >> en;
    if (!in.good()) break;
    if (!type.compare("ca") || !type.compare("ac")) {
      alphaEv[count] = RA;
      deltaEv[count] = dec;
    } else if (!type.compare("gc") || !type.compare("gp")) {
      double ll = 0.;
      double bb = 0.;
      radec2gal(RA/15.0, dec, &ll, &bb, 2000);
      alphaEv[count] = ll;
      deltaEv[count] = bb;
    } else if (type.compare("sgp") == 0) {
      double ll = 0.;
      double bb = 0.;
      radec2gal(RA/15.0, dec, &ll, &bb,2000);
      double sll = 0.;
      double sbb = 0.;
      gal2Sgal(ll, bb, sll, sbb);
      alphaEv[count] = sll;
      deltaEv[count] = sbb;
    }
    enEv[count] = en;
    ++count;
  } //end while
}

TH2F GetTH2data(string type)
{
  // set object coordinates
  double alphaObj = 0.;
  double deltaObj = 0.;
  SetObjectCoords(type, alphaObj, deltaObj);
    
  // set angular scale bin
  double xbins[nbin+1]; // 45+1 bins
  GetAngScaleBinning(xbins);
    
  // get data coordinates
  double alphaEv[nEvents];
  double deltaEv[nEvents];
  double enEv[nEvents];
  GetEvents(type, alphaEv, deltaEv, enEv);
    
  // TH2 map histo: differential (hDataTmp) and integrated form (hData)
  TH2F hData("hData","hData",nbin,xbins,nEbin,minEn,maxEn);
  TH2F hDataTmp("hDataTmp","hDataTmp",nbin,xbins,nEbin,minEn,maxEn);
    
  // for 2-point correlations
  if (!type.compare("ac")) {
    for (int en = 0; en < nEbin; ++en) {
      for (int zz=0; zz<nEvents; ++zz) {
        for (int zj=zz+1; zj<nEvents; ++zj) {
          double gamma = 0.;
          double th = 32 + en;
          if ( enEv[zz] >= th && enEv[zj] >= th) {
            gamma = GetAngDist(alphaEv[zz], deltaEv[zz], alphaEv[zj], deltaEv[zj]);
            hDataTmp.Fill(gamma, th);
          }
        }
      }
    }
  } else { // for the other analyses
    for (int z=0; z<nEvents; ++z) {
      double gamma = 0.;
      // choose the angular distance: from an object (CenA, GC) or from a plane (GP, SGP)
      if (!type.compare("ca")) {
        gamma = GetAngDist(alphaEv[z], deltaEv[z], alphaObj, deltaObj);
      } else if (!type.compare("gc")) {
        gamma = GetAngDist(alphaEv[z], deltaEv[z], alphaObj, deltaObj);
      } else if (!type.compare("gp")) {
        gamma = abs(deltaEv[z]);
      } else if (!type.compare("sgp")) {
        gamma = abs(deltaEv[z]);
      }
      // fill the histo
      if (enEv[z] <= 80)
        hDataTmp.Fill(gamma, enEv[z]);
      else
        hDataTmp.Fill(gamma, 80);
    }
  }

  // for auto-correlation
  if (!type.compare("ac")) {
    for (int i=1; i<=nEbin; ++i) { // energy loop
      for (int j=1; j<=nbin; ++j) { // angular scale loop
        double content = hDataTmp.Integral(1,j,i,i);
        hData.SetBinContent(j, i, content);
      } // end angular scale loop
    }
  }

  // for the others analyses
  else {
    // final TH2 map: observed events as a function of energy and angular scale
    hData  = GetNormIntegral(hDataTmp, 1);
  }
  hData.SetName("hData");

  return hData;
}

// Calculate expected number of events from the isotropic expectation (hMean)
// and the p-value (hProb)
void evIso(string type, double nsky, TH2F& hMean, TH2F& hProb, TH2F hData, TH1F *histSim[])
{
  // Set object coordinates: CenA, GC, GalPlane, SGalPlane
  double alphaObj = 0.;
  double deltaObj = 0.;
  SetObjectCoords(type, alphaObj, deltaObj);
    
  // Set angular scale bin
  double xbins[nbin+1]; //45+1 bins
  GetAngScaleBinning(xbins);
    
  // TH2 histo(s)
  // temporary histos used during the analysis
  TH2F hSimTmp("hSimTmp","hSimTmp",nbin,xbins,nEbin,minEn,maxEn);
  TH2F hSimTmp2("hSimTmp2","hSimTmp2",nbin,xbins,nEbin,minEn,maxEn);
  TH2F hSim("hSim","hSim",nbin,xbins,nEbin,minEn,maxEn);
    
  int nHisto = nEbin*nbin;
  //TH1F *histSim[nHisto];
  for (int z=0; z<nHisto; ++z) {
    char *histname = new char[10];
    sprintf(histname, "h%d",z);
    histSim[z] = new TH1F(histname,histname,nEvents, 0, nEvents);
    delete [] histname;
  }
    
  // Get data
  double alphaEv[nEvents];
  double deltaEv[nEvents];
  double enEv[nEvents];
  GetEvents(type, alphaEv, deltaEv, enEv);

  // Get number of events in each energy bin
  double nEnbin[nEbin]; //45 bins
  GetNEbin(enEv, nEnbin);
    
  // Get seed
  unsigned int seed;
  GetSeed(seed);
  TRandom3 myR(seed);

  // read exposure function
  string fileIn = "../Data/Exposure/histo-dec-en-th.root";
  TFile *f = TFile::Open(fileIn.c_str(),"READ");
  TF1 *f1;
  f->GetObject("fcos",f1);

  // make simulations!
    
  // loop over isotropic skies
  for (int i = 1; i <= nsky; i++  ) {
    if (i % 1000 ==0)
      cout << " sky " << i << endl;

    // energy loop
    for (int j = 0; j<nEbin; ++j) {
        
      // convert double into string....can be improved
      double eSim = double(j + minEn);
      // stringstream ss;
      // ss << eSim;
      // string str = ss.str();
          
      // events loop
      int iCr = 0;
      while (iCr < nEnbin[j]){
        double raSim = myR.Rndm() *360.;
        double decSim = myR.Rndm() * 180. - 90.;
        double y = myR.Rndm() * f1->GetMaximum();
        double yFun = f1->Eval(decSim);
        if (y<=yFun) {
          ++iCr;

          // calculate the distance (gamma) from the corresponding object or plane
          double gamma = 0.;
          if (!type.compare("ca")) {
            gamma = GetAngDist(raSim, decSim, alphaObj, deltaObj);
          } else if (!type.compare("gc")) {
            double ll = 0.;
            double bb = 0.;
            radec2gal(raSim/15.0, decSim, &ll, &bb,2000);
            gamma = GetAngDist(ll, bb, alphaObj, deltaObj);
          } else if (!type.compare("gp")) {
            double ll = 0.;
            double bb = 0.;
            radec2gal(raSim/15.0, decSim, &ll, &bb,2000);
            gamma = abs(bb);
          } else if (!type.compare("sgp")) {
            double ll(0.),bb(0.);
            radec2gal(raSim/15.0, decSim, &ll, &bb,2000);
            double sll = 0.;
            double sbb = 0.;
            gal2Sgal(ll, bb, sll, sbb);
            gamma = abs(sbb);
          }
          //--------------------//
          // Fill the histo
          hSimTmp.Fill(gamma, eSim);
          hSim.Fill(gamma, eSim);
          //--------------------//
        } //end if
      } //end while
    } // end energy loop

    // for the other analyses
    hSimTmp2  = GetNormIntegral(hSimTmp, 1);

    // compare single sky map with event map (for the p-value calculation)
    for (int k=1; k<=nEbin; ++k) {
      for (int y=1; y<=nbin; ++y) {
        double nData = hData.GetBinContent(y,k);
        double nSim  = hSimTmp2.GetBinContent(y,k);
        int nhisto = index(y,k);
        histSim[nhisto]->Fill(nSim);
        if (nSim>=nData)
          hProb.Fill(xbins[y-1],k+31, 1 / (double)nsky);
      } // end angular scale loop
    } // end energy loop
    hSimTmp.Reset();
    hSimTmp2.Reset();
  } // end events loop
  delete f1;

  // now integrate the TH2 histo
  hMean  = GetNormIntegral(hSim, nsky);
  hMean.SetName("hMean");
    
  // print results only for not CenA analyses
  if (type.compare("ca") != 0)
    GetMinimum(hData, hMean, hProb);
}

// Calculate expected number of events from the isotropic expectation (hMean)
// and the p-value (hProb)
void evIso2pt(string type, double nsky, TH2F& hMean, TH2F& hProb, TH2F hData, TH1F *histSim[])
{
  // Set angular scale bin
  double xbins[nbin+1]; //45+1 bins
  GetAngScaleBinning(xbins);

  // TH2 histo(s)
  // temporary histos used during the analysis
  TH2F hSimTmp("hSimTmp","hSimTmp",nbin,xbins,nEbin,minEn,maxEn);
  TH2F hSimTmp2("hSimTmp2","hSimTmp2",nbin,xbins,nEbin,minEn,maxEn);
  TH2F hSim("hSim","hSim",nbin,xbins,nEbin,minEn,maxEn);

  int nHisto = nEbin*nbin;
  //TH1F *histSim[nHisto];
  for (int z=0; z<nHisto; ++z) {
    char *histname = new char[10];
    sprintf(histname, "h%d",z);
    histSim[z] = new TH1F(histname,histname,nEvents, 0, nEvents);
    delete [] histname;
  }

  // Get data
  double alphaEv[nEvents];
  double deltaEv[nEvents];
  double enEv[nEvents];
  GetEvents(type, alphaEv, deltaEv, enEv);

  // Get number of events in each energy bin
  double nEnbin[nEbin]; //45 bins
  GetNEbin(enEv, nEnbin);

  // Get seed
  unsigned int seed;
  GetSeed(seed);
  TRandom3 myR(seed);

  // read exposure function
  string fileIn = "../Data/Exposure/histo-dec-en-th.root";
  TFile *f = TFile::Open(fileIn.c_str(),"READ");
  TF1 *f1;
  f->GetObject("fcos",f1);

  // make simulations!

  // loop over isotropic skies
  for (int i = 1; i <= nsky; i++  ) {
    if (i % 1 ==0)
      cout << " sky " << i << endl;

    // Get data
    double alphaSim[nEvents] = {0.};
    double deltaSim[nEvents] = {0.};
    double enSim[nEvents] = {0.};

    int count = 0;
    // energy loop
    for (int j = 0; j<nEbin; ++j) {

      double eSim = double(j + minEn);

      // events loop
      int iCr = 0;
      while (iCr < nEnbin[j]){
        double raSim = myR.Rndm() *360.;
        double decSim = myR.Rndm() * 180. - 90.;
        double y = myR.Rndm() * f1->GetMaximum();
        double yFun = f1->Eval(decSim);
        if (y<=yFun) {
          ++iCr;
          alphaSim[count] = raSim;
          deltaSim[count] = decSim;
          enSim[count] = eSim;
          ++count;
        } //end if
      } //end while
    } // end energy loop

    // for 2-point correlations
    for (int en = 0; en < nEbin; ++en) {
      for (int zz=0; zz<nEvents; ++zz) {
        for (int zj=zz+1; zj<nEvents; ++zj){
          double gamma = 0.;
          double th = 32 + en;
          if ( enSim[zz] >= th && enSim[zj] >= th) {
            gamma = GetAngDist(alphaSim[zz], deltaSim[zz], alphaSim[zj], deltaSim[zj]);
            hSimTmp.Fill(gamma, th);
            hSim.Fill(gamma, th);
          }
        }
      }
    }

    // integrate the histo
    for (int i=1; i<=nEbin; ++i) { // energy loop
      for (int j=1; j<=nbin; ++j) { // angular scale loop
        double content = hSimTmp.Integral(1,j,i,i);
        //cout << " En bin " << i << " ang bin " << j << " content " << content << endl;
        hSimTmp2.SetBinContent(j, i, content);
      } // end angular scale loop
    }

    // compare single sky map with event map (for the p-value calculation)
    for (int k=1; k<=nEbin; ++k) {
      for (int y=1; y<=nbin; ++y) {
        double nData = hData.GetBinContent(y,k);
        double nSim  = hSimTmp2.GetBinContent(y,k);
        int nhisto = index(y,k);
        histSim[nhisto]->Fill(nSim);
        if (nSim>=nData)
          hProb.Fill(xbins[y-1],k+31, 1 / (double)nsky);
      } // end angular scale loop
    } // end energy loop
    hSimTmp.Reset();
    hSimTmp2.Reset();
  } // end events loop
  delete f1;

  // now integrate the TH2 histo
  hMean  = GetNormIntegral2pt(hSim, nsky);
  hMean.SetName("hMean");

  // print results only for not CenA analyses
  GetMinimum(hData, hMean, hProb);
}

void GetMinimum(TH2F &hData, TH2F &hMean, TH2F &hProb)
{
  // search for the minimum
  int nbinx = 0;
  int nbiny = 0;
  int nbinz = 0;
  hProb.GetMinimumBin(nbinx, nbiny, nbinz);
  double x = hProb.GetXaxis()->GetBinLowEdge(nbinx + 1);
  double y = hProb.GetYaxis()->GetBinLowEdge(nbiny);
  double nData = hData.GetBinContent(nbinx, nbiny);
  double nExp  = hMean.GetBinContent(nbinx, nbiny);
  double min = hProb.GetMinimum();
    
  // if p-value = 0 simulate more isotropic skies
  //if (min==0)
  //  cout << " More simulations needed!" << endl;
  //else {
    cout << endl;
    cout << " ---- Results ----------------------------------------------------------------- " << endl;
    cout << " Local p-value " << min << endl;
    cout << " Energy threshold / EeV " << y << " Angular scale / deg " << x << endl;
    cout << " Observed number of events " << nData
      << " Expected number of events (on average) " << nExp << endl;
    cout << " ------------------------------------------------------------------------------ " << endl;
    cout << endl;
  //}
}

// calculate binomial probability for Centaurus A
void BinProb(TH2F &hData, TH2F &hSim, TH2F &h2)
{
  // get data
  double alphaEv[nEvents];
  double deltaEv[nEvents];
  double enEv[nEvents];
  string type = "ca";
  GetEvents(type, alphaEv, deltaEv, enEv);
    
  // Get number of events for each threshold
  double nOut[nEbin];
  GetNETh(enEv, nOut);
    
  // binomial probability
  for (int i=1; i<=nEbin; ++i) {
    double nTot = nOut[i-1];
    for (int j=1; j<=nbin; ++j) {
      double prob = 0.;
      double nData = hData.GetBinContent(j,i);
      double nSim = hSim.GetBinContent(j,i);
      if (nData)
        prob = TMath::BetaIncomplete(nSim / nTot, nData, nTot - nData + 1);
      else
        prob = 1.;
      h2.SetBinContent(j,i,prob);
    } // end angular scale loop
  } // end energy loop
    
  // print the results on the screen
  GetMinimum(hData, hSim, h2);
}

// check the allowed analyses
bool checkAnalysis(string type)
{
  if ( (!type.compare("ca") ) && (!type.compare("gc")) &&
    (!type.compare("gp")) && (!type.compare("sgp")) && (!type.compare("ac"))) {
        
    cout << endl;
    cout << " Type of allowed analyses: ca (CenA), gc (Galactic Center), gp (GalPlane), sgp (SGalPlane), ac (Auto-correlation) " << endl;
    cout << endl;
    return false;
  }
  else
    return true;
}

// ! function for penalization
double penalization(string type, double nsky)
{
  double pvalue = 0.;
    
  //
  ofstream myfile;
  string title = "Files/file_"+type;
  myfile.open(title.c_str());
    
  // ------------------------------//
  // take the info
  string file = SetNameFile(type);
  cout << " file " << file << endl;
  // take the values
  TFile *f = new TFile(file.c_str());
  file.clear();
  TH2F *hData = (TH2F*)f->Get("hData");
  TH2F *hMean = (TH2F*)f->Get("hMean");
  TH2F *hP;
  if (type.compare("ca"))
    hP = (TH2F*)f->Get("hProb");
  else
    hP = (TH2F*)f->Get("hP");
  int nHisto = nEbin*nbin;
  TH1F *histSim[nHisto];
  for (int z=0; z<nHisto; ++z) {
    char *histname = new char[10];
    sprintf(histname, "h%d",z);
    // cout << " histname " << histname << endl;
    histSim[z] = (TH1F*)f->Get(histname);
  }
  GetMinimum(*hData, *hMean, *hP);
  double min = hP->GetMinimum();
  cout << " p-value to penalize " << min << endl;
  // ------------------------------//
    
  // make the simulations //
  
  // Set object coordinates: CenA, GC, GalPlane, SGalPlane
  double alphaObj = 0.;
  double deltaObj = 0.;
  SetObjectCoords(type, alphaObj, deltaObj);
    
  // Set angular scale bin
  double xbins[nbin+1]; //45+1 bins
  GetAngScaleBinning(xbins);
    
  // TH2 histo(s)
  // temporary histos used during the analysis
  TH2F hSimTmp("hSimTmp","hSimTmp",nbin,xbins,nEbin,minEn,maxEn);
  TH2F hSimTmp2("hSimTmp2","hSimTmp2",nbin,xbins,nEbin,minEn,maxEn);
  TH2F hSim("hSim","hSim",nbin,xbins,nEbin,minEn,maxEn);
    
  // Get data
  double alphaEv[nEvents];
  double deltaEv[nEvents];
  double enEv[nEvents];
  GetEvents(type, alphaEv, deltaEv, enEv);
    
  // Get number of events in each energy bin
  double nEnbin[nEbin]; //45 bins
  GetNEbin(enEv, nEnbin);
    
  // Get number of events for each threshold
  double nOut[nEbin];
  GetNETh(enEv, nOut);
    
  // Get seed
  unsigned int seed;
  GetSeed(seed);
  TRandom3 myR(seed);

  // read exposure function
  string fileIn = "../Data/Exposure/histo-dec-en-th.root";
  TFile *fexpo = TFile::Open(fileIn.c_str(),"READ");
  TF1 *f1;
  fexpo->GetObject("fcos",f1);

  // loop over isotropic skies
  for (int i = 1; i <= nsky; i++  ) {
    // if (i % 100 ==0) cout << " sky " << i << endl;

    // energy loop
    for (int j = 0; j<nEbin; ++j) {
            
      // convert double into string....can be improved
      double eSim = double(j + minEn);

      // events loop
      int iCr = 0;
      while (iCr < nEnbin[j]){
        double raSim = myR.Rndm()*360;
        double decSim = myR.Rndm()*180. - 90.;

        double y = myR.Rndm()*f1->GetMaximum();
        double yFun = f1->Eval(decSim);
        if (y<=yFun) {
          ++iCr;
          // calculate the distance (gamma) from the corresponding object or plane
          double gamma = 0.;
          if (!type.compare("ca")) {
            gamma = GetAngDist(raSim, decSim, alphaObj, deltaObj);
          } else if (!type.compare("gc")) {
            double ll = 0.;
            double bb = 0.;
            radec2gal(raSim/15.0, decSim, &ll, &bb,2000);
            gamma = GetAngDist(ll, bb, alphaObj, deltaObj);
          } else if (!type.compare("gp")) {
            double ll = 0.;
            double bb = 0.;
            radec2gal(raSim/15.0, decSim, &ll, &bb,2000);
            gamma = abs(bb);
          } else if (!type.compare("sgp")) {
            double ll(0.),bb(0.);
            radec2gal(raSim/15.0, decSim, &ll, &bb,2000);
            double sll = 0.;
            double sbb = 0.;
            gal2Sgal(ll, bb, sll, sbb);
            gamma = abs(sbb);
          }
          //--------------------//
          // Fill the histo
          hSimTmp.Fill(gamma, eSim);
          //--------------------//
        } // if
      } // while
    } // energy loop
    hSimTmp2  = GetNormIntegral(hSimTmp, 1);

    double localMin = 1.;
      
    // compare single sky map with event map
    for (int k=1; k<=nEbin; ++k) {
      double nTot = nOut[k-1];
      for (int y=1; y<=nbin; ++y) {
        double nDataSim  = hSimTmp2.GetBinContent(y,k);
        double prob = 0.;
          
        // Cenaturus A analysis - binomial probability
        if (!type.compare("ca")) {
          double mean = hMean->GetBinContent(y,k);
          if (nDataSim && mean)
            prob = TMath::BetaIncomplete(mean / nTot, nDataSim, nTot - nDataSim + 1);
          else
            prob = 1.;
        }
          
        // others analyses with simulations
        else {
          hP = (TH2F*)f->Get("hP");
          int nhisto = index(y,k);
          double entries = histSim[nhisto]->GetEntries();
          double integral = histSim[nhisto]->Integral(nDataSim,nEvents);
          prob = integral / entries;
          if (prob==0)
            prob = 1. / nsky;
        }
          
        if (prob < localMin)
          localMin = prob;
      } // end angular scale loop
    } // end energy loop
    
    myfile << setprecision(12) << localMin << endl;
      
    if (i % 10000 ==0)
      cout << " sky " << i << " min " << localMin << endl;
      
    if (localMin <= min)
      pvalue++;
    hSimTmp.Reset();
    hSimTmp2.Reset();
  } // end events loop

  delete f1;

  if (pvalue ==0)
    pvalue = 1 / nsky;
  else
    pvalue /= nsky;
  
  cout << " penalized p-value " << pvalue << endl;
      
  return pvalue;
}

// ! function for penalization
double penalization2pt(string type, double nsky)
{
  double pvalue = 0.;

  //
  ofstream myfile;
  string title = "Files/file_"+type;
  myfile.open(title.c_str());

  // ------------------------------//
  // take the info
  string file = SetNameFile(type);
  cout << " file " << file << endl;
  // take the values
  TFile *f = new TFile(file.c_str());
  file.clear();
  TH2F *hData = (TH2F*)f->Get("hData");
  TH2F *hMean = (TH2F*)f->Get("hMean");
  TH2F *hP;
  if (type.compare("ca"))
    hP = (TH2F*)f->Get("hProb");
  else
    hP = (TH2F*)f->Get("hP");
  int nHisto = nEbin*nbin;
  TH1F *histSim[nHisto];
  for (int z=0; z<nHisto; ++z) {
    char *histname = new char[10];
    sprintf(histname, "h%d",z);
    // cout << " histname " << histname << endl;
    histSim[z] = (TH1F*)f->Get(histname);
  }
  GetMinimum(*hData, *hMean, *hP);
  double min = hP->GetMinimum();
  cout << " p-value to penalize " << min << endl;
  // ------------------------------//

  // make the simulations //

  // Set object coordinates: CenA, GC, GalPlane, SGalPlane
  double alphaObj = 0.;
  double deltaObj = 0.;
  SetObjectCoords(type, alphaObj, deltaObj);

  // Set angular scale bin
  double xbins[nbin+1]; //45+1 bins
  GetAngScaleBinning(xbins);

  // TH2 histo(s)
  // temporary histos used during the analysis
  TH2F hSimTmp("hSimTmp","hSimTmp",nbin,xbins,nEbin,minEn,maxEn);
  TH2F hSimTmp2("hSimTmp2","hSimTmp2",nbin,xbins,nEbin,minEn,maxEn);
  TH2F hSim("hSim","hSim",nbin,xbins,nEbin,minEn,maxEn);

  // Get data
  double alphaEv[nEvents];
  double deltaEv[nEvents];
  double enEv[nEvents];
  GetEvents(type, alphaEv, deltaEv, enEv);

  // Get number of events in each energy bin
  double nEnbin[nEbin]; //45 bins
  GetNEbin(enEv, nEnbin);

  // Get number of events for each threshold
  double nOut[nEbin];
  GetNETh(enEv, nOut);

  // Get seed
  unsigned int seed;
  GetSeed(seed);
  TRandom3 myR(seed);

  // read exposure function
  string fileIn = "../Data/Exposure/histo-dec-en-th.root";
  TFile *fexpo = TFile::Open(fileIn.c_str(),"READ");
  TF1 *f1;
  fexpo->GetObject("fcos",f1);

  // loop over isotropic skies
  for (int i = 1; i <= nsky; i++  ) {
    if (i % 1000 ==0) cout << " sky " << i << endl;

    // to store simulations
    double alphaSim[nEvents] = {0.};
    double deltaSim[nEvents] = {0.};
    double enSim[nEvents] = {0.};

    int count = 0;
    // energy loop
    for (int j = 0; j<nEbin; ++j) {

      // convert double into string....can be improved
      double eSim = double(j + minEn);

      // events loop
      int iCr = 0;
      while (iCr < nEnbin[j]){
        double raSim = myR.Rndm()*360;
        double decSim = myR.Rndm()*180. - 90.;

        double y = myR.Rndm()*f1->GetMaximum();
        double yFun = f1->Eval(decSim);
        if (y<=yFun) {
          ++iCr;
          // auto-correlation
          alphaSim[count] = raSim;
          deltaSim[count] = decSim;
          enSim[count] = eSim;
          ++count;
        } // if
      } // while
    } // energy loop

    // 2-point correlations
    for (int en = 0; en < nEbin; ++en) {
      for (int zz=0; zz<nEvents; ++zz) {
        for (int zj=zz+1; zj<nEvents; ++zj){
          double gamma = 0.;
          double th = 32 + en;
          if ( enSim[zz] >= th && enSim[zj] >= th) {
            gamma = GetAngDist(alphaSim[zz], deltaSim[zz], alphaSim[zj], deltaSim[zj]);
            hSimTmp.Fill(gamma, th);
          }
        }
      }
    }

    hSimTmp2  = GetNormIntegral2pt(hSimTmp, 1);

    double localMin = 1.;

    // compare single sky map with event map
    for (int k=1; k<=nEbin; ++k) {
      for (int y=1; y<=nbin; ++y) {
        double nDataSim  = hSimTmp2.GetBinContent(y,k);
        double prob = 0.;
        hP = (TH2F*)f->Get("hP");
        int nhisto = index(y,k);
        double entries = histSim[nhisto]->GetEntries();
        double integral = histSim[nhisto]->Integral(nDataSim,nEvents);
        prob = integral / entries;
        if (prob==0)
          prob = 1. / nsky;
        if (prob < localMin)
          localMin = prob;
      } // end angular scale loop
    } // end energy loop

    myfile << setprecision(12) << localMin << endl;

    if (i % 100 ==0)
      cout << " sky " << i << " min " << localMin << endl;

    if (localMin <= min)
      pvalue++;
        hSimTmp.Reset();
        hSimTmp2.Reset();
  } // end events loop

  delete f1;

  if (pvalue ==0)
    pvalue = 1 / nsky;
  else
    pvalue /= nsky;
  cout << " penalized p-value " << pvalue << endl;
  return pvalue;
}




