#ifndef _TIMEMOD_H_
#define _TIMEMOD_H_

#include <vector>
#include <string>
#include <iostream>
#include <fstream>
#include <cmath>

#include "STClibrary.h"
#include "angdist.h"

using namespace std;

/*! 
  This class is dedicated to the temporal distribution of the events. It is usefull when one wants to take this 
  modulation into account in the computation of the coverage map
*/

//! Temporal distribution of the events.
class TTimeModulation
{
 public:
  //! Constructor.
  TTimeModulation(); 

  //! Set #fAccTimeModel.
  void SetAccTimeModel(string model);

  //! Returns #fAccTimeModel.
  string GetAccTimeModel() const;

  //! Returns #fUTCTime.
  vector<double> GetUTCTime() const;

  //! Returns #fAccTime.
  vector<double> GetAccTime() const;

  /*! 
    This function computes the acceptance using distributions or files. If model = "UTC" then the acceptance is 
    computed using an interpolation of the UTC distribution. If model = "UTC+JD" then the JD variation are also 
    taken into account. If model = "filename" then the acceptance is read from a file with two columns : time 
    (UTC time) & acceptance (any relative value).
  */
  void ComputeAccTime(const vector<double> & UTCTime, const vector<double> & utcTime, double timestep, double chi2lim);

 private:
  //! Acceptance model. Either TIME_FLAT, UTC, UTC+JD or a file.
  string fAccTimeModel;

  //! UTC time regularly gridded.
  vector<double> fUTCTime;

  //! Acceptance. Any relative value corresponding to #fUTCTime.
  vector<double> fAccTime;
};

#endif
