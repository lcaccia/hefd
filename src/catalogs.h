#ifndef _CATALOG_
#define _CATALOG_
#include <math.h>
#include <vector>
#include <string>
#include "healpixmap.h"
#include "maptools.h"
#include "events.h"


using namespace std;

class Catalog{

 
public:
  THealpixMap Iso,Mask;
  THealpixMap FluxSources,FluxSourcesP,FluxSourcesI;
  vector< vector<vector<complex<double> > > > alm_sources,alm_map,alm_mapI,alm_mapP;
  //vector< vector<vector<complex<double> > > > alm_sources, alm_map;
  //vector<vector<complex<double> > >  alm_sources, alm_map;
  
  vector<double> ra,dec,l,b,z,flux,ws,lEvents,bEvents,eEvents,lSimu,bSimu,eSimu;
  vector< vector< double > > w; // sources weights
  vector < vector< unsigned int > > histo_E; // histogram of events sorted by energy
  vector<int> source_type;

  unsigned int nside,nsources,wmode;
  unsigned long lmax,lmaxP,lmaxI;
  int masktype;
  double smooth_angle,norm_mask;
  unsigned int energybin;
  void ComputeAlmSources(void);
  THealpixMap ComputeSmoothMap(double sigma,double fiso,unsigned int ebin,bool withfiso=false);
  THealpixMap ComputeTotalMap(double sigma,double fiso,bool withfiso=false);
  void ReadCat(string filename);
  void WeightSources(unsigned int mode);
  vector<THealpixMap> ComputeMaps(double sigma,double f,bool withfiso=false);
  void CutZ(double zmin,double zmax);
  void CutMask(int masktype);
  void CutFlux(double fluxmin);
  void CutB(double bmin);
  void CutType(int stype);
  void CutVol(double dmax,double fluxmin);
  void GetEvents(const vector<TEvent> &events);
  void SimuEventsMap(vector <THealpixMap> & Maps);
  void SimuEventsIso(void);
  //void SimuEventsIso( vector<TEvent> &events);
  void HistoE_Events(void);
  double LoglikeEventsE(double sigma,double f,bool withfiso=false);
  double LoglikeSimuE(double sigma,double f,bool withfiso=false);

  Catalog(string filename,unsigned int weightmode=0);
  //! Redefine operator = (copy)
  Catalog & operator=(const Catalog &);
 
};

void ComputeLobe(double sigma,unsigned int lmax,vector<double> &thetalobe,vector<double> &lobe);
double LoglikeEvent(double lEvent,double bEvent, THealpixMap & Map);
double LoglikeEvents(vector<double> & lEvents, vector<double> &bEvents,THealpixMap & Map);
double LoglikeEvents_Energy(vector<double> & lEvents, vector<double> & bEvents, vector< vector <unsigned int> > & histo_E, vector<THealpixMap> & Maps);
unsigned int mask(unsigned int type,double ra,double dec,double l,double b);
THealpixMap MapMask(unsigned int nside,unsigned int masktype);
void Compute2fCoefs(THealpixMap & mapcat, THealpixMap & mapdata, double * C, double * Idd);
void gzk_init(void);
double gzk_e(double logE,double z);
unsigned int gzk_ebin(double logE);
void GetEventsFromMap(unsigned int nEvents, vector<double> &lSimu, vector<double> &bSimu, THealpixMap &Map);
//double FindMin(Catalog *CAT,double *sigma,double *fiso);
//double FindMinContours(Catalog *CAT,double *sigma,double *fiso,unsigned int npts,double x[],double y[]);
#endif
