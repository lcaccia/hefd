#include "timemod.h"
#include "userfcn.h"

// ROOT
#include "TH1.h"
#include "TF1.h"
#include "TCanvas.h"

#ifdef gcc323
char* operator+( std::streampos&, char* );
#endif

static char gObjName[1024];
static int gObjNumber=0;
static char * GetObjName() {sprintf(gObjName,"Time%d",gObjNumber++); return gObjName;}



TTimeModulation::TTimeModulation() {fAccTimeModel = "TIME_FLAT";}



void TTimeModulation::SetAccTimeModel(string model) {fAccTimeModel = model;}



string TTimeModulation::GetAccTimeModel() const {return fAccTimeModel;}



vector<double> TTimeModulation::GetUTCTime() const {return fUTCTime;}



vector<double> TTimeModulation::GetAccTime() const {return fAccTime;}



void TTimeModulation::ComputeAccTime(const vector<double> & UTCTime, const vector<double> & utcTime, double timestep, double chi2lim)
{
  // Read a file
  if(fAccTimeModel != "TIME_FLAT" && fAccTimeModel != "UTC" && fAccTimeModel != "UTC+JD")
  {
      cout << "Acceptance file : " << fAccTimeModel << endl;
      fUTCTime.clear();
      fAccTime.clear();
      ifstream lStream(fAccTimeModel.c_str());
      double lTmpUTCTime, lTmpAccTime;
      if(!lStream.is_open())
      {
          cerr << "Program Failed : Error : Cannot access to acceptance file" << endl;
          exit(-1);
      }
      while(lStream >> lTmpUTCTime)
      {
          lStream >> lTmpAccTime;
          fUTCTime.push_back(lTmpUTCTime);
          fAccTime.push_back(lTmpAccTime);
      }
      cout << "Acceptance file loaded" << endl ;
      cout << fUTCTime.size() << " line were read" << endl;
      return;
    }
  
  // UTC time
  double lutcmin = *min_element(utcTime.begin(),utcTime.end());
  double lutcmax = *max_element(utcTime.begin(),utcTime.end());
  unsigned int nelt = (int)floor((lutcmax-lutcmin)/timestep);
  fUTCTime.clear();
  fUTCTime.resize(nelt);
  for(unsigned int i = 0; i < nelt; i++) fUTCTime[i] = lutcmin+(lutcmax-lutcmin)*i*1./(nelt-1);

  // JD
  vector<double> JDTime(utcTime.size());
  double jdmin, jdmax;
  for(unsigned int j = 0; j < utcTime.size(); j++) utcs2jd(utcTime[j], &JDTime[j]);
  jdmax = *max_element(JDTime.begin(), JDTime.end());  
  jdmin = *min_element(JDTime.begin(), JDTime.end());
   
  
  /***********************************\
  |  Plot the UTC & JD distributions  |
  \***********************************/

  TCanvas *cArrivalTime = new TCanvas(GetObjName(), "Arrival Times", 1200, 500);
  cArrivalTime->Divide(2,1);
  
  // UTC
  cArrivalTime->cd(1);
  unsigned int nbinsUTC = 48; 
  TH1F* hUTC = new TH1F(GetObjName(),"UTC Distribution", nbinsUTC, 0, 24.);
  hUTC->SetMinimum(0.);
  GetHisto(cArrivalTime, hUTC, "Hour of the day", "",UTCTime);
  hUTC->Draw("e1p");
  cArrivalTime->Update();
      
  // JD
  cArrivalTime->cd(2);
  unsigned int nbinsjd = (unsigned int)(jdmax-jdmin);
  TH1F* hJD = new TH1F(GetObjName(),"Julian Days Distribution", nbinsjd, jdmin, jdmax);
  hJD->SetMinimum(0.);
  GetHisto(cArrivalTime, hJD, "Julian Days", "",JDTime);
  hJD->Draw("e1p");
  cArrivalTime->Update();
  cArrivalTime->SaveAs("arrivalTime.png");
  
  // Interpolation of the UTC histogram
  vector<double> accUTCtmp(nbinsUTC);
  vector<double> timeUTCtmp(nbinsUTC);
  for(unsigned int i=0; i<nbinsUTC; i++)
  {
      accUTCtmp[i] = hUTC->GetBinContent(i+1);
      timeUTCtmp[i] = hUTC->GetBinCenter(i+1);
  }	  
  // Fill the acceptance vector with UTC variation
  vector<double> accUTC(fUTCTime.size());
  double timeUTC;
  int timeYear, timeMonth, timeDay;
  for(unsigned int k=0; k<fUTCTime.size(); k++)
  {
      utcs2date(fUTCTime[k], &timeYear, &timeMonth, &timeDay, &timeUTC);
      accUTC[k] = linear_interp(timeUTCtmp, accUTCtmp, timeUTC);
  }

  fAccTime.resize(fUTCTime.size());
  if(fAccTimeModel == "UTC")
  {
      cout << "Acceptance computed from the UTC distribution" << endl;
      for(unsigned int j=0; j<fUTCTime.size(); j++) fAccTime[j] = accUTC[j];
  }
  else if(fAccTimeModel == "UTC+JD")
  {
      cout << "Acceptance computed from the JD & UTC distributions" << endl;
      vector<double> accJDtmp(nbinsjd);
      vector<double> timeJDtmp(nbinsjd);
      for(unsigned int i=0; i<nbinsjd; i++)
      {
          accJDtmp[i] = hJD->GetBinContent(i+1);
          timeJDtmp[i] = hJD->GetBinCenter(i+1);
      }
      // Fill the acceptance vector with jd+UTC variation
      double accJD, timeJD;
      for(unsigned int k=0; k<fUTCTime.size(); k++)
      {
          accJD = 0.;
          utcs2jd(fUTCTime[k], &timeJD);
          accJD = linear_interp(timeJDtmp, accJDtmp, timeJD);
          fAccTime[k] = accJD*accUTC[k];
      }
  }
}

