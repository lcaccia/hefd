#include <vector>
#include <iostream>
#include <fstream>
#include <string.h>
#include "catalogs.h"
#include "coverage.h" 
#include "common.h"
#include "simuevents.h"
#include "STClibrary.h"
#include "STCconstants.h"
#include "TMath.h"

// #include "Math/Functor.h"
// #include "Math/Minimizer.h"
// #include "Math/MinimizerOptions.h"
// #include "Minuit2/Minuit2Minimizer.h"

#define zenith_max 80.
//#define zenith_max 60.

using namespace std;
const unsigned int nlogE=16;
const double logEmin=19.6,logEmax=20.4;


unsigned int gzk_ebin(double logE){
  double delta_logE=(logEmax-logEmin)/nlogE;
  if (logE>logEmax || logE<logEmin) {cerr << "energy out of range! " << endl;return 0;}
  
  unsigned int logEbin=(unsigned int)((logE-logEmin)/delta_logE );
  return logEbin;
}

double gzk(unsigned int Eth,double z){
  
  double f0,f1,a,b,c,d,E,dc,wgt;
  a=0.;b=0.;c=0.;d=0.;E=0.;f0=0.;f1=0.;dc=100.;
  if (Eth==60){
    // spectral index=2.7
    //a=1.004;    b=-.0025;    c=2.e-06;    d=-7.e-09;    E=0.;    f0=120.12;    f1=0.0261;    dc=230.;
    // spectral index=2.2
    a=0.998;    b=-.00132;    c=-3.69e-06;    d=1.36e-08;    E=-2.47e-11;    f0=5.651e+07;    f1=0.0735;    dc=275.;
  }
  if (Eth==80){
    // spectral index=2.7
    a=1.;    b=-.0067;    c=2.74e-05;    d=-2.84e-07;    E=6.21e-10;    f0=14731.;    f1=0.102;    dc=120.;
  }
  double dist=3.e+05*z/71.4; 
  if (dist<dc)
    wgt=a+b*dist+c*(2.*dist*dist-1)+d*(4.*dist*dist*dist-3*dist)+E*(8.*dist*dist*dist*dist-8.*dist*dist+1.);
  else
     wgt=f0*exp(-f1*dist);
    return wgt;
}


Catalog & Catalog::operator=(const Catalog& m)
{

  FluxSources=m.FluxSources;
  FluxSourcesP=m.FluxSourcesP;  FluxSourcesI=m.FluxSourcesI;
  Iso=m.Iso; Mask=m.Mask;

  alm_sources=m.alm_sources; alm_map=m.alm_map; alm_mapP=m.alm_mapP; alm_mapI=m.alm_mapI;
  ra=m.ra; dec=m.dec; l=m.l;b=m.b;z=m.z; flux=m.flux;source_type=m.source_type; ws=m.ws; w=m.w; 
  lEvents=m.lEvents; bEvents=m.bEvents; eEvents=m.eEvents;
  lSimu=m.lSimu; bSimu=m.bSimu; eSimu=m.eSimu; histo_E=m.histo_E;
  energybin=m.energybin;
  nside=m.nside; nsources=m.nsources; wmode=m.wmode;
  lmax=m.lmax;  lmaxP=m.lmaxP;  lmaxI=m.lmaxI; smooth_angle=m.smooth_angle; norm_mask=m.norm_mask;

  return *this;
}


Catalog::Catalog(string filename,unsigned int weightmode){
  if (filename!="fit") {
    nside=128; wmode=weightmode; lmax=3*nside-1;smooth_angle=0.;
    energybin=100;
    cerr << "Reading file..." ;
    double dOmega=M_PI/(3.*nside*nside);
    ReadCat(filename); cerr << "  Weighting..." ;
    WeightSources(weightmode); 
    cerr << "  computing alm..." << endl;
    ComputeAlmSources();
    cerr << "  computing coverage..." << endl;

    THealpixMap Cov=GetAnalyticalCoverage(nside,zenith_max,kConstantsTK::AugerSouthLatitude); //Coverage

    Mask=MapMask(nside,masktype);
    norm_mask=Mask.Total()*dOmega;   // Mask
    Iso=Cov*Mask;  //Isotropic map = Iso * Mask
    double norm=Iso.Total()*dOmega;   
    Iso=Iso/norm;
  }
}



void Catalog::ComputeAlmSources(void){
  for (unsigned int i=0;i<nlogE;i++){
    vector<vector<complex<double> > >  alm_s;
    THealpixMap buff=map_sources(nside,l,b,w[i]);
    //cerr << "computing alm..." << endl;
    alm_s=buff.Map2Alm(lmax);
    alm_sources.push_back(alm_s);    alm_map.push_back(alm_s);
    alm_mapI.push_back(alm_s);       alm_mapP.push_back(alm_s);
  }
  //cerr << " of the sources" << endl;
}
 

THealpixMap Catalog::ComputeSmoothMap(double sigma,double f,unsigned int ebin,bool withfiso){
  double dOmega=M_PI/(3.*nside*nside);  
  
  double delta_logE=(logEmax-logEmin)/nlogE;
  double logE=logEmin+(ebin+0.5)*delta_logE;
  double E50=pow(10.,logE-19.)/5.; // Energy / 50 EeV 
  double sigmaP=sigma/E50;  //smoothing angle for a proton
  if (sigmaP<0.5) sigmaP=0.5; // limit of the angular resolution.
  double sigmaI=26.*sigmaP;
  
  if (sigma!=smooth_angle || ebin!=energybin){
   
    if (withfiso==false){ // param with sigma,fP
      lmaxP=lmax;
      lmaxI=lmax;
      vector<double> thetalobeP;
      vector<double> lobeP;
      ComputeLobe(sigmaP,lmax,thetalobeP,lobeP);
      vector<double> blP = legendrelobe(thetalobeP,lobeP,lmax);
      if(blP.size()<lmaxP+1) lmaxP = blP.size()+1;
      for(unsigned long l = 0;l < lmaxP+1; l++)
	{
	  for(unsigned long m = 0; m < l+1; m++) alm_mapP[ebin][l][m] = alm_sources[ebin][l][m]*blP[l];
	}
      
      vector<double> thetalobeI;
      vector<double> lobeI;
      ComputeLobe(sigmaI,lmax,thetalobeI,lobeI);
      vector<double> blI = legendrelobe(thetalobeI,lobeI,lmax);
      if(blI.size()<lmaxI+1) lmaxI = blI.size()+1;
      for(unsigned long l = 0;l < lmaxI+1; l++)
	{
	  for(unsigned long m = 0; m < l+1; m++) alm_mapI[ebin][l][m] = alm_sources[ebin][l][m]*blI[l];
	}
      
      THealpixMap mapfiltP(nside);
      mapfiltP.Alm2Map(alm_mapP[ebin]); 
      
      THealpixMap mapfiltI(nside);
      mapfiltI.Alm2Map(alm_mapI[ebin]); 
      
      
      double norm_sourcesP=0.;
      double norm_sourcesI=0.;
      for (unsigned int i=0;i<mapfiltP.NPix();i++)
	norm_sourcesP+=mapfiltP[i]*Mask[i];
      norm_sourcesP*=dOmega;   
      
      FluxSourcesP=mapfiltP/norm_sourcesP;
      
      for (unsigned int i=0;i<mapfiltI.NPix();i++)
	norm_sourcesI+=mapfiltI[i]*Mask[i];
      norm_sourcesI*=dOmega;   
      
      FluxSourcesI=mapfiltI/norm_sourcesI;
      smooth_angle=sigma;
    }
    else{ // //param with sigma,fiso 
      vector<double> thetalobe;
      vector<double> lobe;
      ComputeLobe(sigmaP,lmax,thetalobe,lobe);
      vector<double> bl = legendrelobe(thetalobe,lobe,lmax);
      if(bl.size()<lmax+1) lmax = bl.size()+1;
      
      for(unsigned long l = 0;l < lmax+1; l++)
	for(unsigned long m = 0; m < l+1; m++) 
	  alm_map[ebin][l][m] = alm_sources[ebin][l][m]*bl[l];
      
      THealpixMap mapfilt(nside);
      mapfilt.Alm2Map(alm_map[ebin]); 
      
      double norm_sources=0.;
      for (unsigned int i=0;i<mapfilt.NPix();i++)
	norm_sources+=mapfilt[i]*Mask[i];
      norm_sources*=dOmega;   
      
      FluxSources=mapfilt/norm_sources;
      smooth_angle=sigma;
    }
    
  }
  
  THealpixMap mapsmoothed(nside);
  
  for (unsigned int i=0;i<mapsmoothed.NPix();i++){
    if (withfiso==false)
      mapsmoothed[i]=Iso[i]*(f*FluxSourcesP[i]+(1.-f)*FluxSourcesI[i]);
    else
      mapsmoothed[i]=Iso[i]*(f/norm_mask+(1.-f)*FluxSources[i]);
  }
  mapsmoothed/=mapsmoothed.Total()*dOmega;
  
  return mapsmoothed;
}

THealpixMap Catalog::ComputeTotalMap(double sigma,double f,bool withfiso){
  //THealpixMap MapTotal;
  vector<THealpixMap> Maps;
  for (unsigned int ebin=0;ebin<nlogE;ebin++){
    if (histo_E[ebin].size()>0){
      THealpixMap mapcat=ComputeSmoothMap(sigma,f,ebin,withfiso);
      Maps.push_back(mapcat);
      //for (unsigned int i=0;i<mapcat.NPix();i++){
      //if (mapcat[i]>0)
      //MapTotal[i]*=pow(mapcat[i],histo_E[ebin].size());
    }
  }
  int n=Maps.size();
  THealpixMap MapTotal=Maps[0];
  for (unsigned int i=1;i<n;i++){
    MapTotal=MapTotal*Maps[i];
  }
  double norm=MapTotal.Total();
  MapTotal=MapTotal/norm;
  return MapTotal;
}

void Catalog::ReadCat(string filename){
  if( !CheckFile(filename)) {
    cerr << "File " << filename <<  " not found" << endl; 
    exit(0);
  }
  ifstream in;  in.open(filename.c_str(),ios::in);
  nsources=0;  double RA,DEC,L,B,Z,F,W; int Type;
  int m;
  char ligne[256];
  in.getline(ligne,256);
  cerr << ligne << endl;
  in >> m;
  masktype=m;
  cerr << "masktype " << masktype << endl;
  //in.getline(ligne,256);

  while(!in.eof()){
    if (!in.good()) continue;
    in >> RA >> DEC >> Z >> F >> W >> Type;
    //cerr << RA << "  " << DEC << "  " << Z << "  " << F <<" " << W << " " << Type << endl;
    
    RA/= kSTC::Hrtod;radec2gal(RA,DEC,&L,&B);
    
    //if ( Z>0.048) continue;     //cut 2MRS
    //if (fabs(B)<= 10. || Z>=0.048 || Z<0.) continue;     //cut 2MRS
    if (Z<=0.) continue; 
    ra.push_back(RA);dec.push_back(DEC);l.push_back(L);b.push_back(B);
    z.push_back(Z);flux.push_back(F);ws.push_back(W); source_type.push_back(Type);
    nsources++;
  }
  in.close();

}

void Catalog::WeightSources(unsigned int mode){
  const unsigned int ndist=30;
  const double dmax=600.;
  double delta_d=dmax/ndist;
  
  double gzkweight[ndist][nlogE];
  
  ifstream in1("GZK_vsE.dat",ios::in);
  double d,g;
  for (unsigned int i=0;i<ndist;i++){
    in1 >> d ;
    for (unsigned int j=0;j<nlogE;j++){
      in1 >> g; gzkweight[i][j]=g; 
    }
  }
  in1.close();
  
  w.resize(nlogE,vector<double>(nsources,0.)); // allocates a nlogE x nsources matrix
  
  for (unsigned int i=0;i<nlogE;i++){ // for each bin of logE, fill the sources weight
    
    for (unsigned int j=0;j<nsources;j++){
      double dist=z[j]*290./0.07;
      int dbin=(unsigned int)(dist/delta_d);
      if (dbin< 0 || dbin >= ndist)
	w[i][j]=0.;
      else{
	if (mode==0)// weight prop to flux x gzk attenuation
	  //w[i][j]=ws[j]*flux[j]*gzkweight[dbin][i]; // [][] -> first==source  second==energy bin
	w[i][j]=1.;  // equal weight...
	else
	  w[i][j]=ws[j]*(gzkweight[dbin][i])/(dist*dist); 
      }
    }
  }
}

void Catalog::CutZ(double zmin,double zmax){
  for (unsigned int i=0;i<nsources;i++)
    if (z[i] < zmin || z[i] > zmax ){
      ra.erase(ra.begin()+i);      dec.erase(dec.begin()+i);      l.erase(l.begin()+i); b.erase(b.begin()+i);
      z.erase(z.begin()+i); flux.erase(flux.begin()+i); ws.erase(ws.begin()+i); source_type.erase(source_type.begin()+i);
      i--;
    }
}

void Catalog::CutType(int stype){
  for (unsigned int i=0;i<nsources;i++)
    if (source_type[i] ==stype ){
      ra.erase(ra.begin()+i);      dec.erase(dec.begin()+i);      l.erase(l.begin()+i); b.erase(b.begin()+i);
      z.erase(z.begin()+i); flux.erase(flux.begin()+i); ws.erase(ws.begin()+i);source_type.erase(source_type.begin()+i); 
      i--;
    }
}

void Catalog::CutMask(int masktype){
  for (unsigned int i=0;i<nsources;i++)
    if (mask(masktype,ra[i],dec[i],l[i],b[i])==0){
      ra.erase(ra.begin()+i);    dec.erase(dec.begin()+i);      l.erase(l.begin()+i); b.erase(b.begin()+i);
      z.erase(z.begin()+i);flux.erase(flux.begin()+i);  ws.erase(ws.begin()+i); source_type.erase(source_type.begin()+i);
      i--;nsources--;
    }
}


void Catalog::CutFlux(double fluxmin){
  for (unsigned int i=0;i<nsources;i++){
    if (flux[i] < fluxmin) 
      {
	ra.erase(ra.begin()+i);dec.erase(dec.begin()+i);l.erase(l.begin()+i); b.erase(b.begin()+i);
	z.erase(z.begin()+i); flux.erase(flux.begin()+i); ws.erase(ws.begin()+i);source_type.erase(source_type.begin()+i);
	i--;
      }
  }
}   

void Catalog::CutB(double bmin){
  for (unsigned int i=0;i<nsources;i++){
    if (fabs(b[i]) < bmin) 
      {
	ra.erase(ra.begin()+i);dec.erase(dec.begin()+i);l.erase(l.begin()+i); b.erase(b.begin()+i);
	z.erase(z.begin()+i); flux.erase(flux.begin()+i); ws.erase(ws.begin()+i);source_type.erase(source_type.begin()+i);
	i--;
      }
  }
}   
void Catalog::CutVol(double dmax,double fluxmin){
  double zmax=dmax*71.4/300000.;
  for (unsigned int i=0;i<nsources;i++){
    if (z[i]*300000./71.4 > dmax || flux[i] < fluxmin || flux[i]*z[i]*z[i]/(zmax*zmax) < fluxmin){
      ra.erase(ra.begin()+i);dec.erase(dec.begin()+i);l.erase(l.begin()+i); b.erase(b.begin()+i);
	z.erase(z.begin()+i); flux.erase(flux.begin()+i); ws.erase(ws.begin()+i);source_type.erase(source_type.begin()+i);
	i--;
    }
  }
}

double Catalog::LoglikeEventsE(double sigma,double f,bool withfiso){ // compute the loglikelihood/evts of the evts with fiso param
  double x=0;
  for (unsigned int ebin=0;ebin<nlogE;ebin++){
    if (histo_E[ebin].size()>0){
      THealpixMap mapcat=ComputeSmoothMap(sigma,f,ebin,withfiso);    
      for (unsigned int j=0;j<histo_E[ebin].size();++j){
	unsigned int i_evt=histo_E[ebin][j];
        x+=LoglikeEvent(lEvents[i_evt],bEvents[i_evt],mapcat);
      }
    }
  } 
  return x/lEvents.size();
}

double Catalog::LoglikeSimuE(double sigma,double f,bool withfiso){ // compute the loglikelihood/evts of the evts with fiso param
  double x=0;
  for (unsigned int ebin=0;ebin<nlogE;ebin++){
    if (histo_E[ebin].size()>0){
      THealpixMap mapcat=ComputeSmoothMap(sigma,f,ebin,withfiso);    
      for (unsigned int j=0;j<histo_E[ebin].size();++j){
	unsigned int i_evt=histo_E[ebin][j];
        x+=LoglikeEvent(lSimu[i_evt],bSimu[i_evt],mapcat);
      }
    }
  } 
  return x/lSimu.size();
}

void Catalog::SimuEventsIso(void){
  unsigned int nEvents=lEvents.size();
  //THealpixMap mapiso(nside);
  //for (unsigned int i=0;i<mapiso.NPix();i++)
  //mapiso[i]=Iso[i]*Mask[i];
  
  gRandom->SetSeed(0);
  unsigned int seed;
  struct timeval myTimeVal;
  struct timezone myTimeZone;
  gettimeofday(&myTimeVal, &myTimeZone);
  seed = (unsigned int) (myTimeVal.tv_usec+(myTimeVal.tv_sec % 1000)*1000000);
  TRandom trandom(seed);
  
  // Pixel
  unsigned int nPix = Iso.NPix();
  double szPix = Iso.GetPixSize()*DTOR;
  vector<double> pixNumber(nPix);
  for(unsigned int i = 0; i < nPix; i++) pixNumber[i] = i;

  // Simulated events
  vector<long> pixNumberDraw(nEvents);
  vector<double> pixNumberDrawTmp = distri(pixNumber,Iso,nEvents);
  for(unsigned int i = 0; i < nEvents; i++) pixNumberDraw[i] = (long)pixNumberDrawTmp[i];
  
  // Get Pixel coordinates
  vector<double> lPix, bPix;
  Iso.GiveLB(pixNumberDraw, lPix, bPix);
  
  // Celestial coordinates of the simulated events
  //vector<double> lSimEvents(nEvents), bSimEvents(nEvents), raSimEvents(nEvents), decSimEvents(nEvents);
  // All events are located in the center of the pixels. We move them randomly inside their pixel

  double phiSimEventsTmp, thetaSimEventsTmp;
  long pixNumberMove;
  for(unsigned int i = 0; i < nEvents; i++) {
    double thetaPix = PiOver2-bPix[i]*DTOR;
    double phiPix = lPix[i]*DTOR;
    double szPixProj = 2*sin(thetaPix)*szPix;
    bool status = false;
    while( !status )
      {
	thetaSimEventsTmp = acos(cos(thetaPix)+(trandom.Rndm()-0.5)*szPixProj);
	phiSimEventsTmp = phiPix+(trandom.Rndm()-0.5)*szPix;
	ang2pix_ring(Iso.NSide(),thetaSimEventsTmp,phiSimEventsTmp,&pixNumberMove);
	if(pixNumberMove == pixNumberDraw[i]) status = true;
      }
    lSimu[i] =phiSimEventsTmp*RTOD ;
    bSimu[i] =  90.-thetaSimEventsTmp*RTOD;
  }
}

void Catalog::SimuEventsMap(vector <THealpixMap> & Maps){
  
  unsigned int index=0;
  
  for (unsigned int ebin=0;ebin<nlogE;ebin++){
    unsigned int nEvents=histo_E[ebin].size();
    if (nEvents>0){
      //THealpixMap Mapcat=ComputeSmoothMap(sigma,f,ebin,withfiso); 
      //unsigned int nEvents=lEvents.size();
      //THealpixMap mapiso(nside);
      //for (unsigned int i=0;i<mapiso.NPix();i++)
      //mapiso[i]=Iso[i]*Mask[i];
      
      gRandom->SetSeed(0);
      unsigned int seed;
      struct timeval myTimeVal;
      struct timezone myTimeZone;
      gettimeofday(&myTimeVal, &myTimeZone);
      seed = (unsigned int) (myTimeVal.tv_usec+(myTimeVal.tv_sec % 1000)*1000000);
      TRandom trandom(seed);
      
      // Pixel
      unsigned int nPix = Maps[ebin].NPix();
      double szPix = Maps[ebin].GetPixSize()*DTOR;
      vector<double> pixNumber(nPix);
      for(unsigned int i = 0; i < nPix; i++) pixNumber[i] = i;
      
  // Simulated events
      vector<long> pixNumberDraw(nEvents);
      vector<double> pixNumberDrawTmp = distri(pixNumber,Maps[ebin],nEvents);
      for(unsigned int i = 0; i < nEvents; i++) pixNumberDraw[i] = (long)pixNumberDrawTmp[i];
      
  // Get Pixel coordinates
      vector<double> lPix, bPix;
      Maps[ebin].GiveLB(pixNumberDraw, lPix, bPix);
      
      // Celestial coordinates of the simulated events
      //vector<double> lSimEvents(nEvents), bSimEvents(nEvents), raSimEvents(nEvents), decSimEvents(nEvents);
      // All events are located in the center of the pixels. We move them randomly inside their pixel
      
      double phiSimEventsTmp, thetaSimEventsTmp;
      long pixNumberMove;
      for(unsigned int i = 0; i < nEvents; i++) {
	double thetaPix = PiOver2-bPix[i]*DTOR;
	double phiPix = lPix[i]*DTOR;
	double szPixProj = 2*sin(thetaPix)*szPix;
	bool status = false;
	while( !status )
	  {
	    thetaSimEventsTmp = acos(cos(thetaPix)+(trandom.Rndm()-0.5)*szPixProj);
	    phiSimEventsTmp = phiPix+(trandom.Rndm()-0.5)*szPix;
	    ang2pix_ring(Maps[ebin].NSide(),thetaSimEventsTmp,phiSimEventsTmp,&pixNumberMove);
	    if(pixNumberMove == pixNumberDraw[i]) status = true;
	  }
	lSimu[index+i] =phiSimEventsTmp*RTOD ;
	bSimu[index+i] =  90.-thetaSimEventsTmp*RTOD;
      }
      index+=nEvents;
    }
  }
}
    
void ComputeLobe(double sigma,unsigned int lmax,vector<double> &thetalobe,vector<double> &lobe){ 
  unsigned int nb_lobe=6*lmax; 
  double thetamax_lobe=10.*sigma; 
  double k=3282.806/(sigma*sigma); // (180./pi)^2 = 3282.806
  double facteur=k/(kSTC::TwoPi*(1.-exp(-2.*k)));
  double val;
  if (thetamax_lobe>180.) thetamax_lobe=180.;
  //cerr << "computing lobe..." ;
  for(unsigned int i=0; i<nb_lobe; i++) {
    thetalobe.push_back(i*thetamax_lobe/(nb_lobe-1));
    //lobe.push_back(exp(-thetalobe[i]*thetalobe[i]*1./(2.*sigma*sigma))); // planar approximation
    val=facteur*exp(k*(cos(thetalobe[i]*kSTC::DTOR) -1.));
    lobe.push_back(val); // Von Mises- Fisher 
  }
  //cerr << " for the smoothing" << endl;
}


double LoglikeEvent(double lEvent, double bEvent, THealpixMap & Map){
  double likelihood=Map.Value(lEvent,bEvent);
  return log(fabs(likelihood));
  //return log(likelihood);
}

double LoglikeEvents(vector<double> & lEvents, vector<double> & bEvents,THealpixMap & Map){
  double loglikelihood=0.; vector<double> likelihood=Map.Values(lEvents,bEvents);
  for (unsigned int i=0;i<lEvents.size();i++){
    if (likelihood[i]!=0.)   // to avoid the pb of ln(0)
      loglikelihood+=log(fabs(likelihood[i]));
    //loglikelihood+=log(likelihood[i]);
  }
  return loglikelihood;
}

double LoglikeEvents_Energy(vector<double> & lEvents, vector<double> & bEvents, vector< vector <unsigned int> > & histo_E, vector<THealpixMap> & Maps){
  double x=0;double l;
 for (unsigned int ebin=0;ebin<nlogE;ebin++){
   if (histo_E[ebin].size()>0){
     for (unsigned int j=0;j<histo_E[ebin].size();++j){
       unsigned int i_evt=histo_E[ebin][j];
       l=LoglikeEvent(lEvents[i_evt],bEvents[i_evt],Maps[ebin]);
       if  (!std::isinf(l) )  x+=l;
      }
   }
 } 
 return x/lEvents.size();  
}

vector<THealpixMap> Catalog::ComputeMaps(double sigma,double f,bool withfiso){
  vector<THealpixMap> Maps;
  for (unsigned int ebin=0;ebin<nlogE;ebin++){
    THealpixMap map=ComputeSmoothMap(sigma,f,ebin,withfiso);
    Maps.push_back(map);
  }
  return Maps;
}

unsigned int mask(unsigned int type,double ra,double dec,double l,double b){
  // type=0 --> no mask
  // type=1 --> |b|<10 deg
  // type=2 --> HIPASS dec > 25 deg
  // type=3 --> HICAT  dec > 2 deg
  // type=4 --> Exclude Cen.A 18 deg
  // type=5 --> |b|< 10 deg for dec < -40 deg
  // type=6 --> JF2012 deflections
  // type=7 --> Exclude CenA and TA hot spot regions
  double lcenA=-50.5*kSTC::DTOR;  double bcenA=19.42*kSTC::DTOR;
  double a=18.*kSTC::DTOR;

double lTAhotspot=177.1466*kSTC::DTOR, bTAhotspot=49.5982*kSTC::DTOR;
double aTA=20.*kSTC::DTOR;

  switch(type){
  case 0: return 1;
  case 1: if (fabs(b)<10.) return 0; else return 1;
  case 2: if (dec > 25.) return 0; else return 1;
  case 3: if (dec > 2.) return 0; else return 1;
  case 4: if (angle(l*kSTC::DTOR,b*kSTC::DTOR,lcenA,bcenA)<a) return 0; else return 1;
  case 5: if (fabs(b)<10. && dec<-40.) return 0; else return 1;
  case 7: if (angle(l*kSTC::DTOR,b*kSTC::DTOR,lcenA,bcenA)<a||angle(l*kSTC::DTOR,b*kSTC::DTOR,lTAhotspot,bTAhotspot)<aTA) return 0; else return 1;
  default: return 1;
  }
}


THealpixMap MapMask(unsigned int nside,unsigned int masktype){
  THealpixMap m(nside);
  
  double ra,dec;
  vector<long> iPix(m.NPix());  vector<double> lPix;   vector<double> bPix;
  for(unsigned int i = 0; i < m.NPix(); i++) iPix[i] = i;
  m.GiveLB(iPix,lPix,bPix);
  
  if (masktype<6) {
    for(unsigned int i = 0; i < m.NPix(); i++){
      gal2radec(lPix[i],bPix[i],&ra,&dec);
      ra*=15.;
      m[i]=mask(masktype,ra,dec,lPix[i],bPix[i]); 
    }
  }
  
  else{
    long nipix;
    int nside_JF2012=8;
    THealpixMap DeflJF2012(nside_JF2012);
    int count=197;
    int pixmask[]={0,10,11,13,21,24,26,38,40,41,42,57,60,82,85,110,111,112,113,116,142,143,144,146,147,148,173,174,177,178,180,206,207,208,211,237,238,239,241,242,269,271,272,276,301,303,304,305,306,307,309,337,340,341,366,367,368,369,370,396,397,398,399,400,403,405,406,407,408,417,423,426,428,429,430,431,432,433,436,450,453,454,463,464,465,468,469,478,481,484,494,496,497,498,500,502,508,516,517,525,527,528,529,530,532,534,536,538,544,545,547,550,551,558,560,561,562,564,565,568,571,576,578,580,581,582,590,591,592,593,595,596,597,598,599,602,606,607,609,610,622,623,627,628,629,631,633,636,637,639,640,652,656,657,661,667,670,671,682,683,684,685,686,687,688,689,690,697,707,709,710,711,715,716,717,719,726,729,731,732,733,735,736,741,742,745,748,749,754,757,758,759,763,764,765,766,767};
    for(unsigned int i = 0; i < DeflJF2012.NPix(); i++)
      DeflJF2012[i]=1;
    for(unsigned int i = 0; i < count ; i++)
      DeflJF2012[pixmask[i]]=0;
      
    for(unsigned int i = 0; i < m.NPix(); i++){
      
      ang2pix_ring(nside_JF2012,(90.-bPix[i])*M_PI/180.,lPix[i]*M_PI/180.,&nipix); 
      m[i]=DeflJF2012[nipix];
    }
   
    
  }

  return m;  
}


double ConcentrationCoef(THealpixMap & mapcat, THealpixMap & mapdata){
  double c=0.; double p=0.,c2=0.,d2=0.;
  
  if (mapcat.NSide() == mapdata.NSide()){
    for (unsigned int i=0;i<mapcat.NPix();i++){
      p+=mapcat[i]*mapdata[i]; c2+=mapcat[i]*mapcat[i]; d2+=mapdata[i]*mapdata[i];
    }
    c=p/sqrt(c2*d2);
  }
  return c;
}

void Catalog::GetEvents(const vector<TEvent> &events){
  DECLARE_VECTOR(double,levts,events,fL);
  DECLARE_VECTOR(double,bevts,events,fB);
  DECLARE_VECTOR(double,eevts,events,fEnergy);
  unsigned int size=levts.size();  cerr << "size= " <<size << endl;
  
  lEvents.resize(size);bEvents.resize(size);eEvents.resize(size);
  lSimu.resize(size);bSimu.resize(size);eSimu.resize(size);
  
  for (unsigned int i=0;i<size;i++){
    lEvents[i]=levts[i];bEvents[i]=bevts[i];eEvents[i]=eevts[i];
    lSimu[i]=levts[i];bSimu[i]=bevts[i];eSimu[i]=eevts[i];
  }
}

void Catalog::HistoE_Events(void){
  histo_E.clear();
  histo_E.resize(nlogE);
  
  for (unsigned int i=0;i<lEvents.size();i++){
    double logE=18.+log10(eEvents[i]);
    unsigned int bin=gzk_ebin(logE);
    histo_E[bin].push_back(i);
  }
  for (unsigned int i=0;i<nlogE;i++)
    cerr << histo_E[i].size() << " " ;
  cerr <<endl;
}
void GetEventsFromMap(unsigned int nEvents, vector<double> &lSimu, vector<double> &bSimu, THealpixMap &Map){
  lSimu.clear();  bSimu.clear();
  gRandom->SetSeed(0);
  unsigned int seed;
  struct timeval myTimeVal;
  struct timezone myTimeZone;
  gettimeofday(&myTimeVal, &myTimeZone);
  seed = (unsigned int) (myTimeVal.tv_usec+(myTimeVal.tv_sec % 1000)*1000000);
  TRandom trandom(seed);

  // Pixel
  unsigned int nPix = Map.NPix();
  double szPix = Map.GetPixSize()*DTOR;
  vector<double> pixNumber(nPix);
  for(unsigned int i = 0; i < nPix; i++) pixNumber[i] = i;

  // Simulated events
  vector<long> pixNumberDraw(nEvents);
  vector<double> pixNumberDrawTmp = distri(pixNumber,Map,nEvents);
  for(unsigned int i = 0; i < nEvents; i++) pixNumberDraw[i] = (long)pixNumberDrawTmp[i];

  // Get Pixel coordinates
  vector<double> lPix, bPix;
  Map.GiveLB(pixNumberDraw, lPix, bPix);

  // Celestial coordinates of the simulated events
  //vector<double> lSimEvents(nEvents), bSimEvents(nEvents), raSimEvents(nEvents), decSimEvents(nEvents);
  // All events are located in the center of the pixels. We move them randomly inside their pixel

  double phiSimEventsTmp, thetaSimEventsTmp;
  long pixNumberMove;
  for(unsigned int i = 0; i < nEvents; i++) {
    double thetaPix = PiOver2-bPix[i]*DTOR;
    double phiPix = lPix[i]*DTOR;
    double szPixProj = 2*sin(thetaPix)*szPix;
    bool status = false;
    while( !status )
      {
    thetaSimEventsTmp = acos(cos(thetaPix)+(trandom.Rndm()-0.5)*szPixProj);
    phiSimEventsTmp = phiPix+(trandom.Rndm()-0.5)*szPix;
 ang2pix_ring(Map.NSide(),thetaSimEventsTmp,phiSimEventsTmp,&pixNumberMove);
    if(pixNumberMove == pixNumberDraw[i]) status = true;
      }
    //lSimu[i] =phiSimEventsTmp*RTOD ;
    // bSimu[i] =  90.-thetaSimEventsTmp*RTOD;
    lSimu.push_back(phiSimEventsTmp*RTOD);
    bSimu.push_back( 90.-thetaSimEventsTmp*RTOD);
  }

}
		     
// double Catalog::LL(const double *xx)
// {
//   const Double_t sigma = xx[0];
//   const Double_t fiso = xx[1];
//   THealpixMap mapswift=ComputeSmoothMap(sigma,fiso,0);
//   double ll=LoglikeEvents(lEvents,bEvents,mapswift) ;  
//   return -ll;
// }

// double FindMin(Catalog *CAT,double *sigma,double *fiso){
//   ROOT::Minuit2::Minuit2Minimizer min ( ROOT::Minuit2::kMigrad );
//   double bestLL=100000.; 
  
//   min.SetMaxFunctionCalls(1000000);
//   min.SetMaxIterations(100000);
//   min.SetTolerance(0.001);
  
//   ROOT::Math::Functor f(CAT,&Catalog::LL,2); 
//   min.SetFunction(f);
//   // Set the free variables to be minimized!
//   min.SetVariable(0,"sigma",2., 0.1);
//   min.SetLimitedVariable(1,"fiso",0.5, 0.1,0.,1.);
//   min.Minimize(); 
  
//   const double *xs = min.X(); double logl=min.MinValue();
//   if (logl<bestLL){bestLL=logl;*sigma=xs[0];*fiso=xs[1];}

//   min.SetVariable(0,"sigma",8., 0.5);
//   min.SetLimitedVariable(1,"fiso",0.5, 0.1,0.,1.);
//   min.Minimize(); 
  
//   const double *xs2 = min.X();  logl=min.MinValue();
//   if (logl<bestLL){bestLL=logl;*sigma=xs2[0];*fiso=xs2[1];}


//   min.SetVariable(0,"sigma",20., 1.);
//   min.SetLimitedVariable(1,"fiso",0.5, 0.1,0.,1.);
//   min.Minimize(); 
  
//   const double *xs3 = min.X();  logl=min.MinValue();
//   if (logl<bestLL){bestLL=logl;*sigma=xs3[0];*fiso=xs3[1];}
  
  
//   return -bestLL;
// }

// double FindMinContours(Catalog *CAT,double *sigma,double *fiso,unsigned int npts,double x[],double y[]){
  
//   ROOT::Minuit2::Minuit2Minimizer min ( ROOT::Minuit2::kMigrad );
//   double bestLL=100000.; 
  
//   min.SetMaxFunctionCalls(1000000);
//   min.SetMaxIterations(100000);
//   min.SetTolerance(0.001);
  
//   ROOT::Math::Functor f(CAT,&Catalog::LL,2); 
//   min.SetFunction(f);
//   // Set the free variables to be minimized!
//   min.SetVariable(0,"sigma",2., 0.1);
//   min.SetLimitedVariable(1,"fiso",0.5, 0.1,0.,1.);
//   min.Minimize(); 
  
//   const double *xs = min.X(); double logl=min.MinValue();
//   if (logl<bestLL){bestLL=logl;*sigma=xs[0];*fiso=xs[1];}

//   min.SetVariable(0,"sigma",8., 0.5);
//   min.SetLimitedVariable(1,"fiso",0.5, 0.1,0.,1.);
//   min.Minimize(); 
  
//   const double *xs2 = min.X();  logl=min.MinValue();
//   if (logl<bestLL){bestLL=logl;*sigma=xs2[0];*fiso=xs2[1];}


//   min.SetVariable(0,"sigma",20., 1.);
//   min.SetLimitedVariable(1,"fiso",0.5, 0.1,0.,1.);
//   min.Minimize(); 
  
//   const double *xs3 = min.X();  logl=min.MinValue();
//   if (logl<bestLL){bestLL=logl;*sigma=xs3[0];*fiso=xs3[1];}
  
//   min.Contour(1,0,&npts,x,y);
  
//   return -bestLL;
// }
