#include <iostream>
#include <fstream>
#include <vector>
#include <iomanip>
#include <sys/stat.h>

// Files
#include "maptools.h"
#include "common.h"

// ROOT
#include "TRint.h"
#include "TROOT.h"
#include "TStyle.h"

#ifdef gcc323
char* operator+( std::streampos&, char* );
#endif

using namespace std;


void Usage(string myname)
{
  cout << endl;
  cout << " Synopsis : " << endl;
  cout << myname << " -h, --help to obtain this message" << endl;
  cout << myname << " <lobe file>" << endl << endl;

  cout << " Description :" << endl;
  cout << "The events are simulated following an acceptance that vary with time. We compute the coverage map "
       << "using the UTC and the JD distributions of the simulated events in order to absorb the acceptance "
       << "modulation. Finally, the Li & Ma map and the power spectrum are computed. The <lobe file> must "
       << "contain two colums: theta (in deg.) and the lobe value normalized to one at maximum. You can easily "
       << "produce one of this file using the compute_lobe executable." << endl;

  exit(0);
}



int main(int argc, char* argv[])
{
  ////////////////////////////////////////////////////////////////////////////
  //                                                                        //
  //                        To start (initialization)                       //
  //                                                                        //
  //////////////////////////////////////////////////////////////////////////// 
  
  // Command line
  if(argc != 2) Usage(argv[0]);
  string lobeFile = argv[1];
  if( !CheckFile(lobeFile) ) {cerr << "File: " << lobeFile << " not found." << endl; exit(0);}

  int fargc = 1;
  string extension;
  TRint *rint = NULL;
  rint = new TRint("Test", &fargc, argv);
  extension = ".png";
  gROOT->SetStyle("Plain");
  gStyle->SetTitleFont(30,"TITLE");


  // Read the lobe file
  ifstream ifsLobe(lobeFile.c_str());
  vector<double> thetaLobe, lobe;
  while( !ifsLobe.eof() )
    {
      float th, lo;
      ifsLobe >> th >> lo;
      thetaLobe.push_back(th);
      lobe.push_back(lo);
    }

  ////////////////////////////////////////////////////////////////////////////
  //                                                                        //
  //             Simulation of my acceptance law & the events               //
  //                                                                        //
  ////////////////////////////////////////////////////////////////////////////

  unsigned int nSide = 64;
  THealpixMap map(nSide);
  // Isotropic full sky
  map = map+1;
 
  // Simulation of the events with an acceptance law
  double thetaMin = 0.;
  double thetaMax = 60.;
  unsigned int nVal = 10000;
  vector<double> thVal(nVal);
  vector<double> pthVal(nVal);
  for(unsigned int i=0; i<nVal; i++)
    {
      thVal[i] = i*180./(nVal-1);
      pthVal[i] = sin(thVal[i]*M_PI/180)*cos(thVal[i]*M_PI/180);
      if (thVal[i]>thetaMax) pthVal[i] = 0;
    }

	std::cout<<"Hello"<<std::endl;

	return 1;
}
